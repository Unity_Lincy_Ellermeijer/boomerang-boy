﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomPropertyDrawer(typeof(Range))]
public class RangeDrawer : PropertyDrawer
{
    SerializedProperty min;
    SerializedProperty max;

    const float offset = 28;
    const float width = 133;

    public override void OnGUI(Rect pos, SerializedProperty prop, GUIContent label)
    {
        EditorGUI.BeginProperty(pos, label, prop);

        //draw label
        pos = EditorGUI.PrefixLabel(pos, GUIUtility.GetControlID(FocusType.Passive), label);

        //disable indented child fields
        int indent = EditorGUI.indentLevel;
        EditorGUI.indentLevel = 0;

        //find properties of range
        min = prop.FindPropertyRelative("min");
        max = prop.FindPropertyRelative("max");

        //label min / max positions
        var posMinLabel = new Rect(pos.x, pos.y, pos.width / 2, pos.height);
        var posMaxLabel = new Rect(pos.x + pos.width/2, pos.y, pos.width / 2, pos.height);

        //draw fields 
        var minRect = new Rect(posMinLabel.x + offset - 2, posMinLabel.y, width, posMinLabel.height);
        EditorGUI.PrefixLabel(posMinLabel, new GUIContent("Min"));
        min.floatValue = EditorGUI.FloatField(minRect, min.floatValue);

        var maxRect = new Rect(posMaxLabel.x + offset, posMaxLabel.y, width, posMaxLabel.height);
        EditorGUI.PrefixLabel(posMaxLabel, new GUIContent("Max"));
        max.floatValue = EditorGUI.FloatField(maxRect, max.floatValue);

        //set indent back
        EditorGUI.indentLevel = indent;

        EditorGUI.EndProperty();

    }
}
