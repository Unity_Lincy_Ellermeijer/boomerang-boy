﻿using UnityEngine;
using System.Collections;

//This class changes the sprite of an explanation sign, giving the player visual feedback when he's close enough to read it
//It has a ref to a talkscript, which enables talking
public class ExplanationSign : MonoBehaviour
{
    public bool canBeRead; //if player is close enough
    public Sprite[] sprites; 
    ChangePlayerInRange range;
    SpriteRenderer sprRender;
    TalkScript talkScr; //ref to talkscript, needed to talk

    void Start()
    {
        range = GetComponentInChildren<ChangePlayerInRange>();
        sprRender = GetComponent<SpriteRenderer>();
        talkScr = GetComponent<TalkScript>();
    }

    void Update()
    { 
        if (range.playerInZone)
        {
            sprRender.sprite = sprites[1];
        }
        else
        {
            sprRender.sprite = sprites[0];
        }
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            canBeRead = true;
        }
    }
}

