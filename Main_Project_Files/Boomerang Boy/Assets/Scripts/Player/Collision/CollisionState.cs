﻿using UnityEngine;
using System.Collections;

public class CollisionState : MonoBehaviour {

	public LayerMask collisionLayer;
	public LayerMask secondCollisionLayer;
	public bool standing;
	public bool leftOnWall;
    public bool rightOnWall;
    public bool onLadder;
    Vector2 bottomPosition = Vector2.zero;
	Vector2 rightPosition = Vector2.zero;
    Vector2 leftPosition = Vector2.zero;
	public float collisionRadius;
	public Color debugCollisionColor = Color.red;
	Animator animator;


    Renderer rend;

	void Start () {
        rend = GetComponent<Renderer>();
		animator = GetComponent<Animator>();
    }

	void FixedUpdate(){

        Vector2 pos;
        pos.x = transform.position.x;
        pos.y = transform.position.y - rend.bounds.extents.y + (collisionRadius/2);
        bottomPosition = pos;

		if (Physics2D.OverlapCircle (pos, collisionRadius, collisionLayer) || Physics2D.OverlapCircle (pos, collisionRadius, secondCollisionLayer)) {
			standing = true;
		} else {
			standing = false;
		}
		   

        Vector2 rightWallHit;
        rightWallHit.x = transform.position.x + rend.bounds.extents.x - (collisionRadius * 0.95f);
        rightWallHit.y = transform.position.y;
        rightPosition = rightWallHit;

        Vector2 leftWallHit;
        leftWallHit.x = transform.position.x - rend.bounds.extents.x + (collisionRadius * 0.95f);
        leftWallHit.y = transform.position.y;
        leftPosition = leftWallHit;

        leftOnWall = Physics2D.OverlapCircle(leftWallHit, collisionRadius, collisionLayer);  
        rightOnWall = Physics2D.OverlapCircle(rightWallHit, collisionRadius, collisionLayer);
		animator.SetBool("Jump",!standing);       

    }

	void OnDrawGizmos(){
		Gizmos.color = debugCollisionColor;

        Vector2[] positions = new Vector2[] { leftPosition, bottomPosition, rightPosition };

        foreach (Vector2 position in positions) {
			Gizmos.DrawWireSphere (position, collisionRadius);
		}
	}
}
