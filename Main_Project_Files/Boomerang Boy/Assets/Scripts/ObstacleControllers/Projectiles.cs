﻿using UnityEngine;
using System.Collections;

public class Projectiles : ObstacleController
{
    public override void Start()
    {
        base.Start();
    }

    public override void OnCollisionEnter2D(Collision2D other)
    {
        base.OnCollisionEnter2D(other);

        if (other.gameObject.tag == "Player" || other.gameObject.tag == "Collision")
        {
            Destroy(gameObject);
        }
    }
}
