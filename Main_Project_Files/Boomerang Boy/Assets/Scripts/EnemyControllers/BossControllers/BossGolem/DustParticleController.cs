﻿using UnityEngine;
using System.Collections;

public class DustParticleController : MonoBehaviour
{
    public float rotateSpeed = 300;
    public float angle;
    public float moveSpeed = 1.5f;
    SpriteRenderer renderer;
	void Start () 
    {
        renderer = GetComponent<SpriteRenderer>();

	}
	
	void Update () 
    {
        transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z + rotateSpeed * Time.deltaTime);

        Vector2 direction = new Vector2((float)Mathf.Cos(angle * Mathf.Deg2Rad), (float)Mathf.Sin(angle * Mathf.Deg2Rad));
        direction.Normalize();

        Vector2 pos = transform.position;
        pos += direction * moveSpeed * Time.deltaTime;
        transform.position = pos;

        renderer.material.color = new Color(renderer.material.color.r, renderer.material.color.g, renderer.material.color.b, renderer.material.color.a - Time.deltaTime);
        if (renderer.material.color.a < 0.0f)
        {
            Destroy(this.gameObject);
        }
	}
}
