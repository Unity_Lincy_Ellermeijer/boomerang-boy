﻿using UnityEngine;
using System.Collections;

public class BirdBossManager : BossRoomManager
{
    public GameObject boomerangDispenser; //drops the boomerang needed in battle
    public GameObject rightEndRoom, leftEndRoom; //end of room to keep the player in

    public override void Start()
    {
        base.Start();

        boundary = GameObject.FindGameObjectWithTag("BirdBossBoundary").GetComponent<BoxCollider2D>();
        min = boundary.bounds.min;
        max = boundary.bounds.max;
    }

    public override void Update()
    {
        base.Update();

        //boss is dead 
        if (!LevelManager.inBirdBattle && BattleOn)
        {
            Exit();
        }
    }

    //Make boss ready to battle
    public override void SetUpBoss()
    {
        base.SetUpBoss();
        rightEndRoom.GetComponent<BoxCollider2D>().isTrigger = false;
        leftEndRoom.GetComponent<BoxCollider2D>().isTrigger = false;

        LevelManager.inBirdBattle = true;
        boomerangDispenser.SetActive(true);
        SpawnBossHpBar((int)spawnedBoss.GetComponent<BirdBossController>().mySettings.startHealth);
    }

    //Method to exit boss battle if boss died
    public override void Exit()
    {
        base.Exit();
        LevelManager.birdBattleFinished = true;

        rightEndRoom.GetComponent<BoxCollider2D>().isTrigger = true;
        leftEndRoom.GetComponent<BoxCollider2D>().isTrigger = true;
    }

    //Method to exit boss battle if player died
    public override void Reset()
    {
        if (spawnedBoss != null)
        {
            spawnedBoss.GetComponent<BirdBossController>().ClearBattle();
        }

        rightEndRoom.GetComponent<BoxCollider2D>().isTrigger = true;
        leftEndRoom.GetComponent<BoxCollider2D>().isTrigger = true;
        base.Reset();
    }
}
