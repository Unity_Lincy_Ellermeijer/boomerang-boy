﻿using UnityEngine;
using System.Collections;

//The boss enters this state when his health is belong a threshold
//In this state the boss roams around like in RoamingB but is faster
public class StateBeserk : State<GorillaBossController>
{
    float movementScalar;
    float waitTime; //time to wait after jump

    public override void Enter(GorillaBossController agent)
    {
        agent.Anim.SetBool("Roam", true);

        agent.ChangePath("GroundPlatformPath", 1);

        agent.ThisDetermineNextPoint = agent.DetermineNextPointWithInterval; //set delegate for determining point of path
    }

    public override void Execute(GorillaBossController agent)
    {
        if (agent.Dead) 
            agent.GetFSM.ChangeState(agent.GetFSM.PossibleStates["Dying"]);

        MoveToPoint(agent);
    }

    public override void Exit(GorillaBossController agent)
    {
    }

    //This method contains all movement logic for this state
    void MoveToPoint(GorillaBossController agent)
    {
        //we need to determine a new point on the path
        if (agent.GetDistToPoint < agent.GetMinDistToPoint)
        {
            if (agent.GetPointSelection % 2 == 0) //we're on a platform
                waitTime = Time.time + 0.2f;

            agent.ThisDetermineNextPoint();

            if (agent.GetCurrentPoint.transform.position.y > agent.transform.position.y) //target is above us
            {
                agent.Jump(agent.GetCurrentPoint.transform.position);
            }
        }

        //we're at an edge of an platform
        if (agent.AtEdge && !agent.JumpingDown && agent.JumpCooldown > 0)
        {
            agent.JumpOff();
        }

        //set the movementscalar based on actions
        if (waitTime > Time.time)
            movementScalar = 0;
        else if (agent.IsGrounded || agent.GetCurrentPoint.transform.position.y < agent.transform.position.y)
            movementScalar = 1.2f;
        else if (agent.AtEdge)
            movementScalar = 0.3f;
        else
            movementScalar = 0.6f;

        agent.SetMovementSpeed((agent.GetCurrentPoint.transform.position - agent.transform.position).normalized * movementScalar);
    }
}