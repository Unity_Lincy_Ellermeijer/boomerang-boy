﻿using UnityEngine;
using System.Collections;

//Add this class to particles that need to be destoryed if they played once
public class DestroyFinishedParticle : MonoBehaviour {

    private ParticleSystem thisParticleSystem;

	void Start ()
    {
        thisParticleSystem = GetComponent<ParticleSystem>();
	}
	
	void Update ()
    {
        if (thisParticleSystem.isPlaying)
            return;

        Destroy(gameObject);
	}
}
