﻿using UnityEngine;
using System.Collections;

public class LongJump : PlayerController {

	public AudioClip jumpSound;
	AudioSource audio;

    public float minJumpHeight;
    public float jumpTime;

    float jumpForce;
    float timer;
    bool hasJumped;
    bool isJumping;

    public void Update(){
        /*
        if (Input.GetKey(KeyCode.Space))
        {
            timer += Time.deltaTime;


            if (collisionState.standing && timer < 0.1f)
            {
               rb.velocity = new Vector2(rb.velocity.x, minJumpHeight);
               hasJumped = true;
            }

            if(timer < 0.2f && hasJumped == true)
            {
                rb.AddForce(new Vector2(0f, jumpForce));
            }
        }
        else
        {
            timer = 0;
            hasJumped = false;
        }
        */

        if (Input.GetKeyDown(KeyCode.Space) && collisionState.standing)
        {
            isJumping = true;
			GetComponent<AudioSource>().PlayOneShot(jumpSound, 0.7F);
        }


        if (isJumping)
        {
            timer += Time.deltaTime;

            if (!hasJumped)
            {
                rb.velocity = new Vector2(rb.velocity.x, minJumpHeight);
                hasJumped = true;
            }

            if (Input.GetKey(KeyCode.Space) && !(timer >= jumpTime))
            {
                rb.velocity = new Vector2(rb.velocity.x, minJumpHeight);
            }
            else
            {
                isJumping = false;
                hasJumped = false;
                timer = 0;
            }
        }
	}

}

