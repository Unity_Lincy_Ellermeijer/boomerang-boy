﻿using UnityEngine;
using System.Collections;

public class FadeOutBeforeDestory : MonoBehaviour {

    public float waitForFadeTime;
    float timer;

    SpriteRenderer spriteRender;

	void Start () 
    {
        timer = Time.fixedTime + waitForFadeTime;

        spriteRender = GetComponent<SpriteRenderer>();
	}
	
	void Update () 
    {
	    if(timer < Time.fixedTime)
        {
            spriteRender.color -= new Color(0, 0, 0, 0.1f);
        }
	}
}
