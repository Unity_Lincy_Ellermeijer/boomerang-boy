﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class PlayerHealth : MonoBehaviour {

	public AudioClip dieSound;
	public AudioClip hitSound;
	public AudioClip pickupSound;
	AudioSource audio;

    public int startHearts;
    public int currentHearts;

    public Texture heartImg;

    public ParticleSystem hurtParticle;
    public bool isDmged = false;
    float dmgTimer;

    public float heartSize = 1;

    LevelManager levelmanager;
    Animator anim;

	void Start()
    {
        AddHearts(startHearts);
  
        Reset();

        levelmanager = FindObjectOfType<LevelManager>();
        anim = GetComponent<Animator>();
	}


    void Update()
    {
        if(isDmged)
        {
            dmgTimer -= Time.deltaTime;

            if (dmgTimer < 0)
                isDmged = false;
        }
    }
    
    public void AddHearts(int n)
    {
        currentHearts += n;
    }

	public void Reset()
	{
		currentHearts = startHearts;
	}


	void OnGUI()
	{
        if (Application.loadedLevelName == "PrologueLevel")
            return;

		for (int i = 0; i < currentHearts; i++) 
		{
			GUI.DrawTexture(new Rect(10 + (i*1.25f) * heartImg.width * heartSize, Screen.height-heartImg.height*heartSize-10, heartImg.width* heartSize, heartImg.height*heartSize), heartImg);
		}
	}


    void RemoveHearts(int n)
    {
        currentHearts -= n;
        if (currentHearts <= 0)
            Die();
    }


    public void TakeDamage(int damage)
    {
        if (!isDmged)
        {
			GetComponent<AudioSource>().PlayOneShot(hitSound, 0.7F);
			RemoveHearts(damage);
            Instantiate(hurtParticle, transform.position, transform.rotation);
            isDmged = true;
            dmgTimer = 1.5f;
            anim.SetTrigger("isHurt");
        }
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "HeartPickup")
        {
            AddHearts(1);
			GetComponent<AudioSource>().PlayOneShot(pickupSound, 0.7F);
        }
    }

    public void Die()
    {
        levelmanager.RespawnPlayer();
		GetComponent<AudioSource>().PlayOneShot(dieSound, 0.7F);
    }

}
