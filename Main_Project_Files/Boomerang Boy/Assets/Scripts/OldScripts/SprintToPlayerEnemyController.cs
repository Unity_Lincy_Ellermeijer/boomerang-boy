﻿using UnityEngine;
using System.Collections;

public class SprintToPlayerEnemyController : FollowPlayerEnemyController
{
    public float sprintSpeed;

    public override void Start ()
    {
        base.Start();	
	}

    public override void Update ()
    {
        base.Update();

        if (playerInRange)
        {
            rb.velocity = (new Vector3(player.transform.position.x, transform.position.y, 0f) - transform.position).normalized * sprintSpeed;
            anim.SetBool("Sprinting", true);

            if ((player.transform.position.x < transform.position.x && isFacingRight) || (player.transform.position.x > transform.position.x && !isFacingRight))
                Flip();
        }
        else
        {
            if ((currentPoint.transform.position.x < transform.position.x && isFacingRight) || (currentPoint.transform.position.x > transform.position.x && !isFacingRight))
                Flip();

            anim.SetBool("Sprinting", false);
        }
    }
}
