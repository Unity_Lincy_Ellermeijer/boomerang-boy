﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Player controller and behavior
/// </summary>
public class Boomerang : PlayerController
{
	public AudioClip throwSound;
	public AudioClip catchSound;
	AudioSource audio;

    public bool canBeDealtDamageByBoomerang = false;
	Animator animator;

    public float boomerangCatchDistance = 3.4f;
    public GameObject boomerang;
    BoomerangScript boomerangScript;
    CameraController cam;

    public float minShootDistance;
    public float knockback;
    public float knockbackLength;
    public float knockbackCount;
    public bool knockFromRight;
    public bool knockFromDown;

    void Awake()
    {
        boomerang = GameObject.FindGameObjectWithTag("StartBoomerang");
        if (boomerang != null)
        {
            boomerangScript = boomerang.GetComponent<BoomerangScript>();
        }
        cam = Camera.main.GetComponent<CameraController>();
		animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (cam.IsResetting)
        {
            return;
        }
        float distance = (transform.position.z - Camera.main.transform.position.z) * 0.8f;
        Vector3 lookPosition = new Vector3(Input.mousePosition.x, Input.mousePosition.y, distance);
        lookPosition = Camera.main.ScreenToWorldPoint(lookPosition);
        if (Input.GetMouseButtonDown(0))
        {
            BoomerangScript nearbyBoomerang = CheckForNearbyBoomerang();
            if (nearbyBoomerang == null)
            {
                if (boomerangScript != null)
                {
                    if (!boomerangScript.isFired && !boomerangScript.IsDropped)
                    {
                        Vector2 shootPos = new Vector2(lookPosition.x, lookPosition.y);
                        Vector2 diff = shootPos - new Vector2(transform.position.x, transform.position.y);
                        if (diff.magnitude < minShootDistance)
                        {
                            diff.Normalize();
                            diff *= minShootDistance;
                            shootPos = new Vector2(transform.position.x, transform.position.y) + diff;
                        }
                        ShootBoomerang(shootPos);
						GetComponent<AudioSource>().PlayOneShot(throwSound, 0.7F);
                    }
                    else
                    {
                        float distanceToBoomerang = Vector2.Distance(transform.position, boomerang.transform.position);
                        if (distanceToBoomerang < boomerangCatchDistance && !boomerangScript.isJustRecentlyFired)
                        {
                            boomerangScript.PickUpBoomerang();
                        }
                    }
                }
            }
            else
            {

                ////old boomerang
                //if (boomerangScript != null)
                //{
                //    boomerangScript.IsDropped = true;
                //    boomerangScript.isCatched = false;
                //    boomerangScript.isBeingUsed = false;
                //}
			   if (boomerangScript == null || ((boomerangScript.isFired || boomerangScript.isDropped) && !boomerangScript.isCatched) )
			   {
					//new boomerang
					boomerangScript = nearbyBoomerang;
					boomerang = boomerangScript.gameObject;
					boomerang.transform.parent = null;
					boomerangScript.PickUpBoomerang();
					boomerangScript.isBeingUsed = true;
					nearbyBoomerang = null;
					GetComponent<AudioSource>().PlayOneShot(catchSound, 1F);
               }
                
            }


        }
        if (boomerangScript != null)
        {
            animator.SetBool("inSky", boomerangScript.isFired);
            animator.SetBool("inHands", boomerangScript.isCatched);
            animator.SetBool("onGround", boomerangScript.isDropped);
        }
        else
        {
            animator.SetBool("inSky", true);
            animator.SetBool("inHands", false);
            animator.SetBool("onGround", true);
        }
    }

    /// <summary>
    /// call this if you want to steal the boomerang from player
    /// </summary>
    /// <param name="thiefObj">the object that steals the boomerang</param>
    /// <param name="offset">the offset you want to display it based on the thiefobj</param>
    public void Steal(GameObject thiefObj, Vector3 offset)
    {
        if (boomerangScript != null)
        {
            boomerangScript.isFired = true;
            boomerangScript.isDropped = true;    
            boomerangScript.isCatched = false;
            boomerangScript.isBeingStolen = true;
            boomerangScript.GetComponent<SpriteRenderer>().enabled = true;      
            boomerang.transform.position = thiefObj.transform.position + offset;
            boomerang.transform.parent = thiefObj.transform;
            boomerang = null;
            boomerangScript = null;
        }
    }


    BoomerangScript CheckForNearbyBoomerang()
    {
       
        GameObject[] allBoomerangs = GameObject.FindGameObjectsWithTag("Boomerang");
        float shortestDistance = float.MaxValue;
        BoomerangScript closestBoomerang = null;
        for (int i = 0; i < allBoomerangs.Length; i++)
        {
            GameObject currBoomerang = allBoomerangs[i];
            BoomerangScript currBoomerangScript = currBoomerang.GetComponent<BoomerangScript>();
            if (!currBoomerangScript.isBeingStolen && !currBoomerangScript.isCatched && !currBoomerangScript.isGrowing)
            {
                float thisBoomerangDistance = Vector3.Distance(currBoomerang.transform.position, transform.position);
                if (thisBoomerangDistance < (boomerangCatchDistance*0.5f) && thisBoomerangDistance < shortestDistance)
                {
                    shortestDistance = thisBoomerangDistance;
                    closestBoomerang = currBoomerangScript;
                }
            }
        }
        return closestBoomerang;
    }

    void ShootBoomerang(Vector3 pos)
    {
        boomerang.GetComponent<BoomerangScript>().Shoot(pos);
    }
   
}