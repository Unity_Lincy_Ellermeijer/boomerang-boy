﻿using UnityEngine;
using System.Collections;

public class FlyingEnemyController : EnemyController
{
	public Transform[] patrolPoints;

	private int currentPoint;

	private PlayerController thePlayer;

	public bool originalHeightTop;
	public bool originalHeightBottom;

	public float amplitudeY; // sinus beweging
	public float omegaY; // sinus beweging
	private float index; // sinus beweging

	public float playerRange;

	public LayerMask playerLayer;

	public bool playerInRange;


    public override void Start() 
	{
        base.Start();
        thePlayer = GameObject.Find("Player").GetComponent<PlayerController>();
		transform.position = patrolPoints [0].position;
		currentPoint = 0;

	}
	
	public override void Update ()
    {
        base.Update();

		// voor sinus beweging Y-as
		index += Time.deltaTime;
		float y = (((amplitudeY*Mathf.Sin (omegaY*index))));


		playerInRange = Physics2D.OverlapCircle (transform.position, playerRange, playerLayer);
		originalHeightTop = (transform.position.y >= patrolPoints [currentPoint].position.y + omegaY );
		originalHeightBottom = (transform.position.y <= patrolPoints [currentPoint].position.y - omegaY );

		if (transform.position.x == patrolPoints [currentPoint].position.x) 
		{
			currentPoint++;
            Flip();
		}

		if (currentPoint >= patrolPoints.Length) 
		{
			currentPoint = 0;
		}


		if (!playerInRange) 
		{
			transform.position = Vector3.MoveTowards (transform.position + new Vector3(0,y,0), new Vector3(patrolPoints [currentPoint].position.x,transform.position.y ,0), moveSpeed * Time.deltaTime);

			if(!originalHeightTop || !originalHeightBottom)
			{
				transform.position = Vector3.MoveTowards (transform.position , new Vector3(transform.position.x,patrolPoints [currentPoint].position.y ,0), moveSpeed * Time.deltaTime);
			}


		}
		else
		{
			transform.position = Vector3.MoveTowards (transform.position + new Vector3(0,y,0), thePlayer.transform.position, 2* moveSpeed * Time.deltaTime);
		}


	}

}
