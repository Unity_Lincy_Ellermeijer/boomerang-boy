﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

//This class must be put on every object that's able to talk
public class TalkScript : MonoBehaviour
{
    //the text to say
    public string text = "this sentence";
    public string[] conversation = { "whole speech" };

    //player information
    private GameObject player;
    PlayerController playerController;
    public float playerDist;

    //checks for wheter we're talking or not and how far in the conversation we are
    public bool isTalking;
    public bool isDoneTalking;
    public int index;

    //refs to canvas elements
    public GameObject TextWindow;
    public GameObject talkBackgr;
    public Text talkBar;

    public bool canTalkAutomaticly; //some objects are able to talk as soon as player enters some zone

    void Awake()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerController = player.GetComponent<PlayerController>();
        TextWindow = GameObject.FindGameObjectWithTag("ConversationCanvas");
        talkBackgr = GameObject.FindGameObjectWithTag("TalkBackgr");
        talkBar = talkBackgr.GetComponentInChildren<Text>();
        isTalking = false;
        index = 0;
        talkBar.text = conversation[index];
    }

    //handle input if player ios in range of talking object
    void HandleInput()
    {
        if (Input.GetKeyDown(KeyCode.W)) //check for input 'w' 
        {
            if (!isTalking) //only true if cannot talk automatically, active talking 
            {
                talkBar.text = conversation[index];
                isTalking = true;
                playerController.SetTalking(true);
                talkBackgr.gameObject.SetActive(true);
            }
            if (conversation.Length >= (index + 1)) //go to next text
            {
                talkBar.text = conversation[index];
                index++;
            }
            else //end talk
            {
                talkBar.text = "";
                talkBackgr.gameObject.SetActive(false);
                index = 0;
                isDoneTalking = true;
                playerController.SetTalking(false);
                isTalking = false;
            }
        }
    }

    void Update()
    {
        playerDist = (transform.position - player.transform.position).magnitude;
        if (playerDist <= 2.0f) //player is close enough to talk
        {
            if(!canTalkAutomaticly)
                HandleInput(); //player must activate talk himself
            else
            {
                if (!isTalking) //talking will be automatically started
                {
                    isTalking = true;
                    playerController.SetTalking(true);
                    talkBackgr.gameObject.SetActive(true);
                    talkBar.text = conversation[index];
                }
                HandleInput();
            }
        }
        else
        {
            if (!playerController.isTalking && talkBar!= null)
            {
                talkBar.text = "";
                isTalking = false;
                talkBackgr.gameObject.SetActive(false);
                index = 0;
            }
        }
    }
}