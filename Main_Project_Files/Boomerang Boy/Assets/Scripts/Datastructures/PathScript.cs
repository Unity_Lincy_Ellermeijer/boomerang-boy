﻿using UnityEngine;
using System.Collections;

//Simple script to draw lines between points of a path to easily adjust it in the editor
public class PathScript : MonoBehaviour
{
    Transform[] pathPoints; //all points
    public Color rayColor = Color.red; 

    public Transform[] PathPoints { get { return pathPoints; } }

    void Awake()
    {
        pathPoints = GetComponentsInChildren<Transform>();
    }

    void OnDrawGizmosSelected()
    {
        Gizmos.color = rayColor;

        //loop through array of points and draw a line from prev to this
        for (int i = 0; i < pathPoints.Length; i++)
        {
            Vector2 pos = pathPoints[i].position;
            if (i > 1)
            {
                var prev = pathPoints[i - 1].position;
                Gizmos.DrawLine(prev, pos);
            }
        }
    }
}
