﻿using UnityEngine;
using System.Collections;

//This class is the super class for every boss room in the game
public class BossRoomManager : MonoBehaviour
{
    //variables for boss hp bar 
    Texture bossBarTex;
    Texture bossBarFill;
    int currentHp;
    int maxHp;
    bool drawHpBar;
    Vector2 hpBarDrawPos;
    float currentBarWidth;
    float targetBarWidth;

    //player references
    protected PlayerController player;
    PlayerHealth playerHp;

    //variables to handle camera before, while and after boss battle
    Camera camera;
    CameraController cameraController;
    public Vector3 cameraPos;
    public float orthograpicSize; //camera size in battle
    float _orthograpicSize; //variable to store original size

    //boss variables
    public GameObject boss; //prefab
    protected GameObject spawnedBoss; //ref to boss
    public Transform bossTransform; //spawn pos

    //check variables
    float timer;
    public bool BattleOn { get; set; }
    public bool BattleFinished { get; set; }
    protected bool activatedPlayerMovement;
    public ChangePlayerInRange enteredRoom;
    protected BoxCollider2D boundary;
    protected Vector3 min, max;

    //evil monkey!
    public GameObject evilMonkey;
    public Transform spawnMonkey;

    public virtual void Start()
    {
        bossBarTex = Resources.Load<Texture>("Art/Sprites/UI/BossHealthBar");
        bossBarFill = Resources.Load<Texture>("Art/Sprites/UI/fill");
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        playerHp = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerHealth>();
        camera = FindObjectOfType<Camera>();
        cameraController = camera.GetComponent<CameraController>();
    }

    void OnGUI()
    {
        if (drawHpBar)
        {
            GUI.color = Color.red;
            GUI.DrawTexture(new Rect(hpBarDrawPos.x, hpBarDrawPos.y, currentBarWidth, bossBarTex.height), bossBarFill);
            GUI.color = Color.white;
            GUI.DrawTexture(new Rect(hpBarDrawPos.x, hpBarDrawPos.y, bossBarTex.width, bossBarTex.height), bossBarTex);
        }
    }

    public virtual void Update()
    {
        //active this by calling SpawnBossHpBar()
        if (drawHpBar)
        {
            //move inside screen
            if (hpBarDrawPos.y > Screen.height - bossBarTex.height)
            {
                hpBarDrawPos.y -= Time.deltaTime * 300f;
            }

            //decrease or increase bar width (fuck lerp im lazy)
            if (targetBarWidth < currentBarWidth)
            {
                currentBarWidth -= Time.deltaTime * 400f;
                if (currentBarWidth < targetBarWidth)
                {
                    currentBarWidth = targetBarWidth;
                }
            }
            else if (targetBarWidth > currentBarWidth)
            {
                currentBarWidth += Time.deltaTime * 400f;
                if (currentBarWidth > targetBarWidth)
                {
                    currentBarWidth = targetBarWidth;
                }
            }
        }

        //player just entered room
        if (enteredRoom.playerInZone && !BattleOn && !BattleFinished)
        {
            StartMonkey();
            SetUpCamera();
            player.ToggleMovement(false);
            SetUpBoss();
        }

        //intro with evil monkey
        if (BattleOn && !BattleFinished && !activatedPlayerMovement)
        {
            UpdateMonkey();

            if (evilMonkey.transform.position.x > max.x)
            {
                ActivatePlayerMovement();
                evilMonkey.gameObject.SetActive(false);
            }
        }

        //player dead
        if (playerHp.currentHearts <= 0)
        {
            Reset();
        }

        // IN CHILD: if Levelmanager.inTHISBOSSbattle = false & battleOn = true, call exit();
    }

    //call if battle starts 
    public void SetUpCamera()
    {
        cameraController.IsFollowing = false;
        
        _orthograpicSize = camera.orthographicSize;
        camera.orthographicSize = orthograpicSize;

        camera.transform.position = cameraPos;
    }

    //call if battle ends
    public void SetBackCamera()
    {
        cameraController.IsFollowing = true;

        camera.orthographicSize = _orthograpicSize;
    }

    public void SpawnBossHpBar(int _maxHp)
    {
        //spawn the bar
        drawHpBar = true;
        maxHp = _maxHp;
        currentHp = maxHp;
        currentBarWidth = 0;
        targetBarWidth = bossBarTex.width;
        hpBarDrawPos = new Vector2(Screen.width / 2f - bossBarTex.width / 2f, Screen.height);
    }

    public void RemoveBossHpBar()
    {
        //remove the bar
        drawHpBar = false;
    }

    public void UpdateBossHpBar(int hp)
    {
        //update bar health, because the bosses are not of the same baseclass its hard to "get" the hp variable 
        //so use this when hp get decreased
        currentHp = hp;
        if (currentHp < 0)
            currentHp = 0;
        targetBarWidth = bossBarTex.width / maxHp * currentHp;
    }

    //Call this methode as soon as 'intro evilmonkey' is over to let the boss room know the battle can begin!
    public virtual void ActivatePlayerMovement()
    {
        player.ToggleMovement(true);
        activatedPlayerMovement = true;
    }

    //Call to spawn the boss
    public virtual void SetUpBoss()
    {
        BattleOn = true; //battle is very on right now
        spawnedBoss = Instantiate(boss, new Vector3(bossTransform.position.x, bossTransform.position.y, bossTransform.position.z), Quaternion.identity) as GameObject; //need a ref to boss later

        //Set LevelManager ready for boss battle
        LevelManager.ResurrectAllEnemies();
        LevelManager.ChangeMusicToBoss(true); 
        //IN CHILD: set bool LevelManager.inTHISBOSSBattle to true; 
    }

    //Call if boss died
    public virtual void Exit()
    {
        BattleFinished = true;
        BattleOn = false;

        SetBackCamera();
        RemoveBossHpBar();
        LevelManager.ChangeMusicToBoss(false);
        //IN CHILD: end the boss battle properly by spawning, despawning and opening a way out
    }

    //Call if player died
    public virtual void Reset()
    {
        activatedPlayerMovement = false;
        BattleOn = false;
        SetBackCamera();
        Destroy(spawnedBoss);
        RemoveBossHpBar();
        LevelManager.ChangeMusicToBoss(false);
    }

    //Call once when player just entered the room
    public virtual void StartMonkey()
    {
        evilMonkey.gameObject.SetActive(true);
        evilMonkey.transform.position = spawnMonkey.transform.position;
    }

    //Call till monkey is out of screen to make him move out of the screen
    public virtual void UpdateMonkey()
    {
        evilMonkey.transform.position += new Vector3(3 * Time.deltaTime, 0, 0);
    }
}
