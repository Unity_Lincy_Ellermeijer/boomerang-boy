﻿using UnityEngine;
using System.Collections;

public abstract class PlayerController : MonoBehaviour {

	public MonoBehaviour[] Scripts;
    
	protected Rigidbody2D rb;
	protected CollisionState collisionState;
    protected static bool isFacingRight = true;

    public bool isTalking = false;

    protected virtual void Start(){
		rb = GetComponent<Rigidbody2D> ();
		collisionState = GetComponent<CollisionState> ();
	}

    protected virtual void ToggleScripts(bool value){
		foreach (MonoBehaviour script in Scripts) {
			script.enabled = value;
		}
	}

    public void ToggleMovement(bool value)
    {
        if(value == false)
        {
            GetComponent<Animator>().SetFloat("Speed", 0);
        }
        GetComponent<Walk>().enabled = value;
        GetComponent<LongJump>().enabled = value;
        GetComponent<Boomerang>().enabled = value;
    }

    public void SetTalking(bool value)
    {
        if (value != false)
        {
            GetComponent<Animator>().SetFloat("Speed", 0);
        }
        GetComponent<Walk>().enabled = !value;
        GetComponent<LongJump>().enabled = !value;
        GetComponent<Boomerang>().enabled = !value;

        isTalking = value;
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.transform.tag == "MovingPlatform")
            transform.parent = other.transform;
    }

    void OnCollisionExit2D(Collision2D other)
    {
        if (other.transform.tag == "MovingPlatform")
            transform.parent = null;
    }
}
