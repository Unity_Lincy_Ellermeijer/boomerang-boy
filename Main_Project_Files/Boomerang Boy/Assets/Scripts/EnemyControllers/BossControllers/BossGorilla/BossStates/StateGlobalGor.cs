﻿using UnityEngine;
using System.Collections;

//This class is the global state
//Everything in this state will always be executed
public class StateGlobalGor : State<GorillaBossController>
{
    //float dmgtesttimer;

    public override void Enter(GorillaBossController agent)
    {
        //dmgtesttimer = Time.fixedTime + 3f;
    }

    public override void Execute(GorillaBossController agent)
    {
        if (agent.Dead)
            return;

        //aaaaaall the flip conditions
        if ((((agent.GetVelocity.x < 0 && !agent.IsFacingLeft) || (agent.GetVelocity.x > 0 && agent.IsFacingLeft)) && (agent.GetFSM.CurrentState == agent.GetFSM.PossibleStates["RoamingA"] || agent.GetFSM.CurrentState == agent.GetFSM.PossibleStates["RoamingB"] || agent.GetFSM.CurrentState == agent.GetFSM.PossibleStates["Beserk"] || agent.GetFSM.CurrentState == agent.GetFSM.PossibleStates["ClimbingUp"] || agent.GetFSM.CurrentState == agent.GetFSM.PossibleStates["ClimbingDown"])) || (((agent.Player.transform.position.x < agent.transform.position.x && !agent.IsFacingLeft) || (agent.Player.transform.position.x > agent.transform.position.x && agent.IsFacingLeft)) && (agent.GetFSM.CurrentState == agent.GetFSM.PossibleStates["RockAttack"] || agent.GetFSM.CurrentState == agent.GetFSM.PossibleStates["MonkeyAttack"])))
            agent.Flip();
            
        if (!agent.AtEdge && agent.JumpingDown)
            agent.JumpingDown = false;

        if (agent.Health <= agent.mySettings.healthAmountStartBeserk && (agent.GetFSM.CurrentState == agent.GetFSM.PossibleStates["RoamingB"] || agent.GetFSM.CurrentState == agent.GetFSM.PossibleStates["RoamingA"]))
            agent.GetFSM.ChangeState(agent.GetFSM.PossibleStates["Beserk"]);

        //if (dmgtesttimer < Time.fixedTime && !agent.Dead)
        //{
        //agent.TakeDamage(1);
        //dmgtesttimer = Time.fixedTime + 3f;
        //}
    }

    public override void Exit(GorillaBossController agent)
    {
    }
}
