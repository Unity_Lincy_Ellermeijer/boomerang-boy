﻿using UnityEngine;
using System.Collections;

//This class shouldn't only be healthpickup, but every kind of pick up
//It gets destroyed on collision with the player
public class HealthPickup : MonoBehaviour {

    GameObject player;

    void Start ()
    {
        player = GameObject.FindGameObjectWithTag("Player");
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Player")
        {
            Destroy(gameObject); 
        }
    }
}
