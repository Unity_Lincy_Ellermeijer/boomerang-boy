﻿using UnityEngine;
using System.Collections;

public class BoomerangDispenser : MonoBehaviour 
{
    public GameObject boomerangToDispense;
    BoomerangScript currentBoomerang;
    GameObject newBoomerang;

    float startTime;
    public float respawnDelay = 4;
    float respawnTimer;
    public float growDuration = 5f;
    public bool onlyDispenseOnce = false;
	void Start ()
    {
        GetComponent<SpriteRenderer>().enabled = false;
        SpawnBoomerang();
	}
	
	void Update () 
    {
        if (currentBoomerang != null)
        {
            if (currentBoomerang.isGrowing)
            {
                float currentTime = (Time.time - startTime) / growDuration;
                float newScale = Mathf.Lerp(0, 4, currentTime);
                currentBoomerang.transform.localScale = new Vector3(newScale, newScale, newScale);
                if (currentTime >= 1f)
                {
                    currentBoomerang.isGrowing = false;
                }
            }
            if (currentBoomerang.isCatched)
            {
                currentBoomerang = null;
            }
        }
        else
        {
            if (!onlyDispenseOnce)
            {
                respawnTimer += Time.deltaTime;
                if (respawnTimer >= respawnDelay)
                {
                    respawnTimer = 0;
                    SpawnBoomerang();
                }
            }
            
        }
	}

    void SpawnBoomerang()
    {
        if (boomerangToDispense != null)
        {
            newBoomerang = Instantiate(boomerangToDispense, transform.position, Quaternion.identity) as GameObject;
            newBoomerang.transform.localScale = Vector3.zero;
            newBoomerang.transform.parent = this.transform;
            BoomerangScript boomerangScript = newBoomerang.GetComponent<BoomerangScript>();
            boomerangScript.isDropped = true;
            boomerangScript.isGrowing = true;
            //newBoomerang.transform.FindChild("catchCircle").GetComponent<SpriteRenderer>().enabled = false;
            currentBoomerang = boomerangScript;
            startTime = Time.time;
        }
    }

    public void DestroyCurrentBoomerang()
    {
        Destroy(newBoomerang);
        currentBoomerang = null;
    }
}
