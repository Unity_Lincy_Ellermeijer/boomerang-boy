﻿using UnityEngine;
using System.Collections;

public class JumpingEnemyController : EnemyController
{

    public float jumpSpeed = 5f;
    public bool isJumping = false;

    public Transform groundCheck;
    public Transform lowWallCheck;
    public Transform highWallCheck;
    public Transform edgeCheck;
    public bool isGrounded;
    public bool hittingLowWall;
    public bool hittingWall;
    public bool atEdge;

    public override void Start()
    {
        base.Start();
    }

    public override void Update()
    {
        base.Update();

        isGrounded = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));
        hittingLowWall = Physics2D.Linecast(transform.position, lowWallCheck.position, 1 << LayerMask.NameToLayer("Ground"));
        hittingWall = Physics2D.Linecast(transform.position, highWallCheck.position, 1 << LayerMask.NameToLayer("Ground"));
        atEdge = Physics2D.Linecast(transform.position, edgeCheck.position, 1 << LayerMask.NameToLayer("Ground"));

        if ((hittingWall || !atEdge) && isGrounded)
            Flip();

        if (!hittingWall && hittingLowWall && !isJumping)
            Jump();

        if (isGrounded && isJumping)
            isJumping = false;
    }

    void FixedUpdate()
    {
        if (!isJumping)
        {
            if (isFacingRight)
                rb.velocity = new Vector2(moveSpeed, rb.velocity.y);
            else
                rb.velocity = new Vector2(-moveSpeed, rb.velocity.y);
        }

        if (isJumping)
        {
            if (isFacingRight)
                rb.velocity = new Vector2(moveSpeed/2f, rb.velocity.y);
            else
                rb.velocity = new Vector2(-moveSpeed/2f, rb.velocity.y);
        }
    }

    public void Jump()
    {
        rb.velocity = new Vector2(rb.velocity.x, jumpSpeed);
        isJumping = true;
    }
}
