﻿using UnityEngine;
using System.Collections;

public class EventManager : MonoBehaviour {

    public GameObject event1, event2, event3;
    public ChangePlayerInRange golemTrigger, birdTrigger;
    EventScript script1, script2, script3;

    bool started;

	void Start () {
        script1 = event1.GetComponent<EventScript>();
        script2 = event2.GetComponent<EventScript>();
        script3 = event3.GetComponent<EventScript>();
    }
	
	// Update is called once per frame
	void Update () {
        if (!started)
        {
            script1.isTriggered = true;
            started = true;
        }

        if (golemTrigger.playerInZone && LevelManager.golemBattleFinished)
        {
            script2.isTriggered = true;
        }
        
        if(birdTrigger.playerInZone && LevelManager.birdBattleFinished)
        {
            script3.isTriggered = true;
        }
	}
}
