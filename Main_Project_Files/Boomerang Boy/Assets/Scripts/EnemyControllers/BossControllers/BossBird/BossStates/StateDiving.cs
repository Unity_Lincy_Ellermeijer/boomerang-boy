﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//This state is an attack state
//It makes the bird fly at the bottom of the screen, hiting the player and dropping eggs
public class StateDiving : State<BirdBossController>
{
    bool screenEntered; //attack has started
    float timerCheck; 
    float timerStart;

    int randomEggAmount; //random amount of eggs to drop
    List<Vector2> randomEggPosses;
    float checkForDropRadius = 0.5f;

    public override void Enter(BirdBossController agent)
    {
        agent.Anim.SetBool("Diving", true);

        screenEntered = false;
        timerCheck = Time.time + 2;
        timerStart = Time.fixedTime + 1;

        SetDropEggInformation(agent, agent.mySettings.eggsToSpawn); 
        
        SetStartAttackPos(agent, agent.DetermineSideRoom());
        agent.transform.localScale *= agent.GrowScalar; //make bird bigger to make him look close by

        agent.Invulnerable = true;
        agent.SetMovementSpeed(Vector2.zero);
    }

    public override void Execute(BirdBossController agent)
    {
        if (((agent.transform.position.x < agent.GetMinXPos) || (agent.transform.position.x > agent.GetMaxXPos)) && screenEntered) //out of screen and attack was already started
            agent.GetFSM.ChangeState(agent.GetFSM.PossibleStates["Flying"]);

        if (timerCheck < Time.time) //are we in screen yet? done by timer because of the high speed 
            screenEntered = true;

        if(timerStart < Time.fixedTime) //start to move after waiting just a little bit
            MoveToPoint(agent);

        if (randomEggPosses != null) //as long as there are still eggposses to drop an egg at...
        {
            //...check for every pos if the bird is there and drop if so
            for (int i = 0; i < randomEggPosses.Count; i++)
            {
                if ((agent.transform.position.x >= randomEggPosses[i].x - checkForDropRadius) && (agent.transform.position.x <= randomEggPosses[i].x + checkForDropRadius))
                {
                    DropEgg(agent, randomEggPosses[i]);
                    randomEggPosses.Remove(randomEggPosses[i]);
                }
            }
        }
    }

    public override void Exit(BirdBossController agent)
    {
        agent.transform.localScale /= agent.GrowScalar; //set bird size back
        ClearEggDropInformation();

        agent.Anim.SetBool("Diving", false);
        agent.Invulnerable = false;
    }

    void MoveToPoint(BirdBossController agent)
    {
        agent.SetMovementSpeed(((agent.GetCurrentPoint.transform.position - agent.transform.position).normalized) * agent.DiveScalar);
    }

    //Determine what the start pos is based on which side of the room the bird is
    void SetStartAttackPos(BirdBossController agent, bool right)
    {
        if (right)
        {
            agent.transform.position = agent.RightPointUnderRoom.position;
            agent.ChangePath("DiveAttackPath", 2);
        }

        else
        {
            agent.transform.position = agent.LeftPointUnderRoom.position;
            agent.ChangePath("DiveAttackPath", 1);
        }
    }

    //Call to set random amount of eggs with all a random position
    void SetDropEggInformation(BirdBossController agent, Range range)
    {
        randomEggPosses = new List<Vector2>();
        randomEggAmount = (int)range.GetRandomInRange();

        bool[] isTaken = new bool[agent.AttackPoints.Count];

        for (int i = 0; i < randomEggAmount; i++)
        {
            var selected = Random.Range(0, agent.AttackPoints.Count);
            var randomSpawnVector = agent.AttackPoints[selected].position;

            if (isTaken[selected])
            {
                while(isTaken[selected])
                {
                    selected = Random.Range(0, agent.AttackPoints.Count);
                    randomSpawnVector = agent.AttackPoints[selected].position;
                }
            }

            randomEggPosses.Add(randomSpawnVector);
            isTaken[selected] = true;
        }        
    }

    //Call to drop an egg
    void DropEgg(BirdBossController agent, Vector2 dropSpawn)
    {
        GameObject tempEgg = new GameObject();
        tempEgg.transform.position = dropSpawn;
        tempEgg.transform.rotation = Quaternion.identity;
        var eggScript = tempEgg.AddComponent<EggScript>();

        eggScript.Create(agent.MyElement == BirdBossController.Element.fire ? 1 : 0, agent);
    }

    void ClearEggDropInformation()
    {
        randomEggAmount = 0;
        randomEggPosses = null;
    }
}
