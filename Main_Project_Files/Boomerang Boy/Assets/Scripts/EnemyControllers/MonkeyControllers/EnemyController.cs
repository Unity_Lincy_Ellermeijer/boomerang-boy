﻿using UnityEngine;
using System.Collections;

//This class is the super class for all enemies 
public class EnemyController : MonoBehaviour
{
    //health variables    
    public float startHealth;
    float health;
    float Health
    {
        get { return health; }
        set 
        { 
            health = value;
            if (health <= 0)
            {
                Die();
            }
        }
    }
    public ParticleSystem hurtParticle;

    //movement variables
    public float moveSpeed;
    public bool isFacingRight = true;

    //amount of damage 
    public int damage;

    //player references
    [HideInInspector]
    public GameObject player;
    [HideInInspector]
    public PlayerHealth playerHealth;
    [HideInInspector]
    public PlayerController playerController;
    [HideInInspector]
    public Knockback knockback;

    [HideInInspector] public BoxCollider2D collider;
    [HideInInspector] public Rigidbody2D rb;
    [HideInInspector] public Animator anim;

	SoundManager sound; //ref to sound manager 

    public virtual void Start ()
    {
        health = startHealth;
        collider = GetComponent<BoxCollider2D>();
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        player = GameObject.FindGameObjectWithTag("Player");
        playerHealth = FindObjectOfType<PlayerHealth>();
        playerController = FindObjectOfType<PlayerController>();
        knockback = FindObjectOfType<Knockback>();
		sound = GameObject.Find("Main Camera").GetComponent<SoundManager>();
    }
	
    public virtual void Update()
    {
       
    }

    //Called by boomerang on hit 
    public virtual void TakeDamage(float damage)
    {
        Health -= damage;
        Instantiate(hurtParticle, transform.position, transform.rotation);
		sound.PlayHitSound ();
    }

    //Methode overloading for enemies with element
    public virtual void TakeDamage(float damage, int element)
    {
        Health -= damage;
        Instantiate(hurtParticle, transform.position, transform.rotation);
    }

    //Call to flip enemy visually 
    public virtual void Flip()
    {
        isFacingRight = !isFacingRight;

        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    //Call if hp<=0, dont destory enemy to enable respawning
    public virtual void Die()
    {
        health = 0;
        gameObject.SetActive(false);
		sound.PlayDieSound();
    }

    //Zombie enemies! No, jk.. 
    public virtual void Resurrect()
    {
        gameObject.SetActive(true);
        health = startHealth;
    }

    public virtual void OnCollisionEnter2D(Collision2D other)
    {
        //Dont collide with other enemies to avoid nasty complications
        if (other.gameObject.tag == "Enemy")
        {
            Physics2D.IgnoreCollision(other.collider, collider);
        }

        //Damage the player on hit
        if (other.gameObject.tag == "Player")
        {
            if (damage <= 0)
                return;

            if (playerHealth.currentHearts > 0)
            {
                playerHealth.TakeDamage(damage);

                knockback.KnockBack(this.gameObject);

                Flip();
            }
        }
    }
}
