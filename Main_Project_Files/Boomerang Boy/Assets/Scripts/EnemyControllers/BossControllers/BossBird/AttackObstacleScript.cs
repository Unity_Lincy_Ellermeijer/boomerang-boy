﻿using UnityEngine;
using System.Collections;

//This class is the super class for all obstacles that bosses can throw/drop
public class AttackObstacleScript <T> : MonoBehaviour 
{
    Rigidbody2D rb;
    protected Rigidbody2D Rb { get { return rb; } set { rb = value; } }

    SpriteRenderer spriteRender;
    protected SpriteRenderer SpriteRender { get { return spriteRender; } set { spriteRender = value; } }
    protected Sprite thisSprite;
    protected Vector3 particleOffset;

    Collider2D collider;
    protected Collider2D Collider { get { return collider; } set { collider = value; } }

    GameObject player;
    PlayerHealth playerHealth;
    Knockback knockback;

    int damage = 1;

    float randomDropChance = 0.1f;
    GameObject healthDrop;

    protected float MoveSpeed { get; set; }

    SoundManager sound;

    //Call to create 
    public virtual void Create(T agent)
    {
        BaseCreate();

        //IN CHILD: set sprite

        //IN CHILD: add collision2d component, set size
    }

    //Overloaded for flames, need a target and mid point room
    public virtual void Create(Vector2 tempTarget, Vector2 mid, T agent)
    {
        BaseCreate();

        //IN CHILD: set sprite

        //IN CHILD: add collision2d component, set size
    }

    //Overloaded for rocks, need different sprites and a target
    public virtual void Create(Vector2 tempTarget, Sprite tempSprite, GorillaBossController agent)
    {
        BaseCreate();
        spriteRender.sprite = tempSprite;

        //IN CHILD: add collision2d component, set size
    }

    //basic things that need to be created for every obstacle
    public void BaseCreate()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        playerHealth = player.GetComponent<PlayerHealth>();
        knockback = player.GetComponent<Knockback>();

        rb = gameObject.AddComponent<Rigidbody2D>();
        spriteRender = gameObject.AddComponent<SpriteRenderer>();

        gameObject.layer = 11;
        spriteRender.sortingLayerName = "Obstacles";

        healthDrop = Resources.Load<GameObject>("Prefabs/Objects/HeartPickup");
        sound = GameObject.Find("Main Camera").GetComponent<SoundManager>();
    }

    public virtual void FixedUpdate()
    {
	
	}

    public virtual void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            if (playerHealth.currentHearts > 0)
            {
                playerHealth.TakeDamage(damage);

                knockback.KnockBack(this.gameObject);
            }
        }

        if (other.tag == "Collision")
        {
            if (Random.value < randomDropChance) //random chance to drop a health pickup
            {
                Instantiate(healthDrop, transform.position, Quaternion.identity);
            }
            sound.PlayHitSound();
            SpawnParticles();
            Destroy(gameObject);
        }
    }

    //spawn particles on hit collision
    public virtual void SpawnParticles()
    {
        var randomAmount = Random.Range(2, 4);

        for (int i = 0; i < randomAmount; i++)
        {
            GameObject particle = new GameObject();
            particle.transform.position = transform.position - particleOffset;
            particle.transform.rotation = Quaternion.identity;
            var particleScript = particle.AddComponent<ParticleScript>();

            particleScript.Create(thisSprite);
        }
    }
}
