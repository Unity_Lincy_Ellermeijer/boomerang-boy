﻿using UnityEngine;
using System.Collections;

public class Door1 : MonoBehaviour {

    public Button1 button;

    void Start()
    {
        gameObject.active = true;
    }

    void Update()
    {
        if (button.triggered)
        {
            gameObject.active = false;
        }
    }
}
