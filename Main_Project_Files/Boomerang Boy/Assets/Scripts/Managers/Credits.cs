﻿using UnityEngine;
using System.Collections;

public class Credits : MonoBehaviour {

    public GameObject camera;
    public float speed;

	// Update is called once per frame
	void Update ()
    {
        camera.transform.Translate(Vector3.down * Time.deltaTime * speed);

        StartCoroutine(waitFor());
	}

    IEnumerator waitFor()
    {
        yield return new WaitForSeconds(24);

        Debug.Log("time has passed");
        Application.LoadLevel("start");
    }
}
