﻿using UnityEngine;
using System.Collections;

//This state makes the boss roam on the ground
public class StateRoamingA : State<GorillaBossController>
{
    public override void Enter(GorillaBossController agent)
    {
        agent.Anim.SetBool("Roam", true);

        agent.ChangePath("GroundPath", 1);

        agent.ThisDetermineNextPoint = agent.DetermineNextPointIncreasing; //set delegate
    }

    public override void Execute(GorillaBossController agent)
    {
        //if our health is belong a threshold
        //or if we passed mid x times
        //or if we're exhausted
        if (agent.Health < agent.mySettings.healthAmountStartBeserk || (agent.GetCountMid % agent.RandomCountMidDiv == 0 && agent.GetCountMid != 0) || agent.Exhausted > agent.mySettings.maxExhausted)
            agent.GetFSM.ChangeState(agent.GetFSM.PossibleStates["Rest"]);

        MoveToPoint(agent);

        agent.Exhausted += Time.fixedDeltaTime;
    }

    public override void Exit(GorillaBossController agent)
    {
        agent.Anim.SetBool("Roam", false);
    }

    void MoveToPoint(GorillaBossController agent)
    {
        //we need to determine the next point of the path
        if (agent.GetDistToPoint < agent.GetMinDistToPoint)
            agent.ThisDetermineNextPoint();

        agent.SetMovementSpeed((agent.GetCurrentPoint.transform.position - agent.transform.position).normalized);
    }
}