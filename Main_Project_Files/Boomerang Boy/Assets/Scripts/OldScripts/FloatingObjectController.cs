﻿using UnityEngine;
using System.Collections;

public class FloatingObjectController : ObstacleController {

    [HideInInspector] public Rigidbody2D rb;
    [HideInInspector] public Renderer rend;
    public float speedInWater;

    Transform waterTrans;
    Vector2[] Path;
    Vector2 leftPos;
    Vector2 rightPos;
    float lerpPercentage;
    float relativePos;
    float distance;
    int dir;
    bool isHit;
    bool hasPath;
    bool pathInit;

    public override void Start()
    {
        base.Start();

        rb = GetComponent<Rigidbody2D>();
        rend = GetComponent<Renderer>();

        isHit = false;
        hasPath = false;
        dir = 1;
    }
    public override void Update()
    {
        if(!isHit)
        {
            rb.Sleep();
        } else
        {
            rb.WakeUp();
        }

        if (hasPath && !pathInit)
        {
            leftPos = Path[0];
            rightPos = Path[1];

            leftPos.x += rend.bounds.extents.x;
            rightPos.x -= rend.bounds.extents.x;

            distance = rightPos.x - leftPos.x;
            relativePos = rightPos.x - transform.position.x;
            lerpPercentage = relativePos / distance;
            pathInit = true;
        }

        if(hasPath && pathInit)
        { 
            if(lerpPercentage >= 1)
            {
                dir = -1;
            } 
            if(lerpPercentage <= 0)
            {
                dir = 1;
            }

            lerpPercentage += speedInWater * dir;

            rb.MovePosition(Vector2.Lerp(leftPos, rightPos, lerpPercentage));
        }
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if(other.gameObject.tag == "Boomerang")
        {
            isHit = true;
        }
        if (other.gameObject.tag == "Water")
        {
            Path = MakePath(other.gameObject);
            hasPath = true;
        }
    }

    public Vector2[] MakePath(GameObject water)
    {
        Renderer rend = water.GetComponent<Renderer>();
        Transform trans = water.GetComponent<Transform>();

        Vector2 leftPos = new Vector3(trans.position.x - rend.bounds.extents.x, trans.position.y + rend.bounds.extents.y);
        Vector2 rightPos = new Vector3(trans.position.x + rend.bounds.extents.x, trans.position.y + rend.bounds.extents.y);

        Vector2[] pathPoints = { leftPos, rightPos };

        return pathPoints;
    }

}
