﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StateMachine<T>  
{
    T owner; //owner state machine 

    //state properties
    public State<T> CurrentState { get; set; }
    public State<T> GlobalState { get; set; }
    public State<T> PreviousState { get; set; }
    public Dictionary<string, State<T>> PossibleStates { get; set; }

    //constuctor
    public StateMachine(T owner)
    {
        PossibleStates = new Dictionary<string, State<T>>();
        this.owner = owner;
        CurrentState = null;
        PreviousState = null;
        GlobalState = null;
    }

    //always update current and global state
    public void Update()
    {
        if (GlobalState != null)
            GlobalState.Execute(owner);

        if (CurrentState != null)
            CurrentState.Execute(owner);
    }

    //call on state transition
    public void ChangeState(State<T> newState)
    {
        PreviousState = CurrentState;
        CurrentState.Exit(owner);
        CurrentState = newState;
        CurrentState.Enter(owner);
    }

    public void RevertToPreviousState()
    {
        ChangeState(PreviousState);
    }

    //check certain state
    public bool IsInState(State<T> controlState)
    {
        if (controlState == CurrentState)
            return true;

        else
            return false;
    }
}
