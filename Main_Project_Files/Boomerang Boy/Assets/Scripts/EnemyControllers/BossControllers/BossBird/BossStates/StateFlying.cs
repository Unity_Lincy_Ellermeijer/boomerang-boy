﻿using UnityEngine;
using System.Collections;

//Bird boss starts in this state
//Flying between to points 
public class StateFlying : State<BirdBossController>
{
    int randomDivider = 3; //check for times passed mid to go to 'Climbing' -> 'Dive'
    float timer; //check for going to 'Centering' -> 'MassiveXAttack'

    public override void Enter(BirdBossController agent) 
    {
        timer = agent.MassAttackTimer;

        SetStartFlyingPos(agent);
        agent.ChangePath("FlyingPath", agent.DetermineSideRoom() ? 1 : 2); //set path 
        randomDivider += (int)agent.mySettings.dividerCountMid.GetRandomInRange(); //change check for times passed mid every time we enter this state
    }

    public override void Execute(BirdBossController agent) 
    {
        //because this is the first state of the boss, enter() will not be called the first time
        //this check is just to 
        if (timer == 0)
        {
            agent.MassAttackTimer = Time.fixedTime + agent.StartMassAttackTimer;
            timer = agent.MassAttackTimer;
        }

        if (agent.Dead) //hp<0 but not yet destroyed
            agent.GetFSM.ChangeState(agent.GetFSM.PossibleStates["Centering"]);

        if (agent.GetCountMid % randomDivider == 0 && agent.GetCountMid != 0)
            agent.GetFSM.ChangeState(agent.GetFSM.PossibleStates["Climbing"]);

        if (timer < Time.fixedTime)
            agent.GetFSM.ChangeState(agent.GetFSM.PossibleStates["Centering"]);
        
        MoveToPoint(agent); //move bird to current point of path
    }

    public override void Exit(BirdBossController agent) 
    {
        //nothing to set on exit
    }

    //Move bird to his current target in the path
    void MoveToPoint(BirdBossController agent)
    {
        if (agent.GetDistToPoint < agent.GetMinDistToPoint)
        {
            agent.DetermineNextPoint();
        }

        agent.SetMovementSpeed((agent.GetCurrentPoint.transform.position - agent.transform.position).normalized);
    }

    //set bird left or right of the room to start flying
    void SetStartFlyingPos(BirdBossController agent)
    {
        if (agent.DetermineSideRoom())
            agent.transform.position = new Vector2(agent.RightPointUnderRoom.position.x, agent.MidPointRoom.position.y);

        else
            agent.transform.position = new Vector2(agent.LeftPointUnderRoom.position.x, agent.MidPointRoom.position.y);
    }

}
