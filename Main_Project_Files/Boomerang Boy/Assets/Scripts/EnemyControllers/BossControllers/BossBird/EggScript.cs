﻿using UnityEngine;
using System.Collections;

//This class creates an egg, dropped by birdboss in diving state, that will spawn a minion bird 
public class EggScript : MonoBehaviour {

    enum Element
    {
        fire,
        ice
    };

    Element element; //element of egg, corresponds to element of bird when he dropped it 

    //variables to hatch
    float startHatchTime;
    float hatchTime;

    Animator anim;
    SpriteRenderer spriteRender;

    BirdBossController thisAgent;

    //Call to create an egg of certain element
    public void Create(int tempElement, BirdBossController agent) 
    {
        thisAgent = agent;

        startHatchTime = thisAgent.mySettings.startHatchTime;
        hatchTime = thisAgent.mySettings.hatchTime;

        Rigidbody2D rb = gameObject.AddComponent<Rigidbody2D>();
        spriteRender = gameObject.AddComponent<SpriteRenderer>();
        anim = gameObject.AddComponent<Animator>();
        BoxCollider2D coll = gameObject.AddComponent<BoxCollider2D>();
        coll.size = new Vector2(0.4f, 0.4f);

        Sprite tempSprite = Resources.Load<Sprite>("Art/Sprites/Enemies/Boss/Bird/blueEgg_0");
        AnimatorOverrideController animAOC = Resources.Load<AnimatorOverrideController>("Art/AnimatorControllers/Enemy/Birdboss/EggDropBlueAOC");

        if (tempElement == 0)
        {
            animAOC = Resources.Load<AnimatorOverrideController>("Art/AnimatorControllers/Enemy/Birdboss/EggDropBlueAOC");
            element = Element.ice;
            tempSprite = Resources.Load<Sprite>("Art/Sprites/Enemies/Boss/Bird/blueEgg");
        }
        else if (tempElement == 1)
        {
            animAOC = Resources.Load<AnimatorOverrideController>("Art/AnimatorControllers/Enemy/Birdboss/EggDropRedAOC");
            element = Element.fire;
            tempSprite = Resources.Load<Sprite>("Art/Sprites/Enemies/Boss/Bird/redEgg");
        }
        
        spriteRender.sprite = tempSprite;
        spriteRender.sortingLayerName = "Obstacles";
        anim.runtimeAnimatorController = animAOC;

        gameObject.layer = 11;

        StartCoroutine("WaitForHatch");
    }

    //Hatch if x seconds has passed 
    IEnumerator WaitForHatch()
    {
        yield return new WaitForSeconds(startHatchTime);

        anim.SetBool("Hatch", true);

        yield return new WaitForSeconds(hatchTime);

        SpawnMinion();
    }

    //Spawn a bird after hatchtime 
    void SpawnMinion()
    {
        StopCoroutine("WaitForHatch");

        GameObject tempMinion = new GameObject();
        tempMinion.transform.position = transform.position;
        tempMinion.transform.rotation = Quaternion.identity;
        var minionScript = tempMinion.AddComponent<BirdMinionController>();

        minionScript.Create(element == Element.fire ? 1 : 0, thisAgent);

        Destroy(this.gameObject);
    }
}
