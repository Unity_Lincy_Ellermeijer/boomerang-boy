﻿using UnityEngine;
using System.Collections;

public class WaterEnemyController : EnemyController {

    Renderer parentRend;
    Transform parentTrans;
    WaterController water;
    [HideInInspector] public Renderer rend;

    public LayerMask waterLayer;

    bool pointInit;
    Vector2 point;
    float lerpPercentage;
    Vector2 oldPos;

    public override void Start()
    {
        base.Start();

        rend = GetComponent<Renderer>();

        rb.gravityScale = 0;
        pointInit = false;
        lerpPercentage = 0;

        RaycastHit2D hit = Physics2D.Raycast(transform.position + Vector3.forward * 0.3f, Vector3.forward, 1f, waterLayer);

        parentRend = hit.collider.GetComponent<Renderer>();
        parentTrans = hit.collider.GetComponent<Transform>();
        water = hit.collider.GetComponent<WaterController>();
    }

    public override void Update()
    {
        base.Update();

        

        if (water.playerIsIn)
        {
            AttackPlayer();
        }
        else
        {
            followRandomPath();
        }
    }

    public void followRandomPath()
    {
        if (!pointInit)
        {
            point = initPoint();
            oldPos = transform.position;
            lerpPercentage = 0;
            pointInit = true;
        }

        lerpPercentage += moveSpeed * Time.deltaTime;
        
        rb.MovePosition(Vector2.Lerp(oldPos, point, lerpPercentage));

        if (lerpPercentage >= 1)
        {
            pointInit = false;
        }

       
    }

    public void AttackPlayer()
    {
        if (pointInit)
        {
            lerpPercentage = 0;
            oldPos = transform.position;
            pointInit = false;
        }

        lerpPercentage += moveSpeed * Time.deltaTime * 2;

        rb.MovePosition(Vector2.Lerp(oldPos, player.transform.position, lerpPercentage));
    }

    public Vector2 initPoint()
    {
        Vector2 point = new Vector2(Random.Range(parentTrans.position.x - parentRend.bounds.extents.x + rend.bounds.extents.x, parentTrans.position.x + parentRend.bounds.extents.x - rend.bounds.extents.x), Random.Range(parentTrans.position.y, parentTrans.position.y - parentRend.bounds.extents.y + rend.bounds.extents.y));

        return point;
    }
}
