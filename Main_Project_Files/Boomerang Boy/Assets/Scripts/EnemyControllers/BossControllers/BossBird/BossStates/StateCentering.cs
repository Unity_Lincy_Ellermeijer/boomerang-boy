﻿using UnityEngine;
using System.Collections;

//This state is used to bring the bird to the center of the room 
//Here the bird either does his massive attack or he dies
public class StateCentering : State<BirdBossController>
{
    float lookOffset = -90f;
    int dir = 1; //1 = from right; -1 = from left;

    public override void Enter(BirdBossController agent)
    {
        agent.ChangePath("CenteringPoint", 1); //set path
        agent.Invulnerable = true; //not able to hurt the bird, because he has a shield or he is dead

        if (agent.transform.position.x < agent.MidPointRoom.position.x) //set dir based on side of the room
            dir = -1;
        else
            dir = 1;
    }

    public override void Execute(BirdBossController agent)
    {
        if ((agent.transform.position - agent.GetCurrentPoint.position).magnitude < agent.GetMinDistToPoint) //if bird reached mid 
        {
            if (!agent.Dead)
            {
                if (agent.MyElement == BirdBossController.Element.fire)
                {
                    agent.GetFSM.ChangeState(agent.GetFSM.PossibleStates["MassFireAttack"]);
                }
                else
                {
                    agent.GetFSM.ChangeState(agent.GetFSM.PossibleStates["MassIceAttack"]);
                }
            }
            else
            {
                agent.GetFSM.ChangeState(agent.GetFSM.PossibleStates["Dying"]);
            }
            
        }
        else //make bird go to mid
        {
            FacePoint(agent);
            MoveToPoint(agent, agent.ClimbScalar);
        }
    }

    public override void Exit(BirdBossController agent)
    {
        agent.SetMovementSpeed(Vector2.zero);
        agent.ResetRot();
        agent.Invulnerable = false;
    }

    void FacePoint(BirdBossController agent)
    {
        var lookPos = agent.GetCurrentPoint.transform.position - agent.transform.position;
        var angle = Mathf.Atan2(lookPos.x, lookPos.y) * Mathf.Rad2Deg;
        agent.transform.eulerAngles = new Vector3(0f, 0f, -angle + lookOffset * dir);
    }

    void MoveToPoint(BirdBossController agent, float scalar)
    {
        if (agent.GetDistToPoint < agent.GetMinDistToPoint)
            agent.DetermineNextPoint();

        agent.SetMovementSpeed(((agent.GetCurrentPoint.transform.position - agent.transform.position).normalized) * scalar);
    }
}
