﻿using UnityEngine;
using System.Collections;

//This class derives directly from the super class and has the ability to turn around when detecting walls and edges 
public class WallEdgeEnemyContoller : EnemyController
{
    public Transform wallCheck, edgeCheck; //points for checks 
    bool hittingWall, hittingCollision, atEdge; //bool checks

    public override void Start()
    {
        base.Start();
        //get wall and edge check hard coded, because we know these are the only childs 
        Transform[] checks = GetComponentsInChildren<Transform>();
        wallCheck = checks[1];
        edgeCheck = checks[2];
    }

    public override void Update()
    {
        base.Update();

        //do all checks
        hittingWall = (Physics2D.Linecast(transform.position, wallCheck.position, 1 << LayerMask.NameToLayer("Ground")) || Physics2D.Linecast(transform.position, wallCheck.position, 1 << LayerMask.NameToLayer("jumpThroughGround")));
        hittingCollision = Physics2D.Linecast(transform.position, wallCheck.position, 1 << LayerMask.NameToLayer("Collision"));
        atEdge = (Physics2D.Linecast(transform.position, edgeCheck.position, 1 << LayerMask.NameToLayer("Ground")) || Physics2D.Linecast(transform.position, edgeCheck.position, 1 << LayerMask.NameToLayer("jumpThroughGround")));

        //if needed, flip around
        if (hittingWall || hittingCollision || !atEdge)
            Flip();
    }

    public virtual void FixedUpdate()
    {
        //set movement according to orientation
        if (isFacingRight)
            rb.velocity = new Vector2(moveSpeed, rb.velocity.y);
        else
            rb.velocity = new Vector2(-moveSpeed, rb.velocity.y);
    }
}
