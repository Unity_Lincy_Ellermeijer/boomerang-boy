﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class LevelManager : MonoBehaviour {

    public static GameObject currentCheckpoint;
    public GameObject respawnPartical;
    public GameObject deathPartical;

    private GameObject player;
    public Camera camera;

    bool doRespawn;
    public float respawnDelay;
    float respawnTimer;

    public static int totalStars;
    public static int totalStarsLvl1 = 5;
    public static int currentStars;

    public static bool inGorBattle;
    public static bool gorBattleFinished;
    public static bool inBirdBattle;
    public static bool birdBattleFinished;
    public static bool inGolemBattle;
    public static bool golemBattleFinished;
    public static bool gameFinished;

    public static List<BoomerangDispenser> boomerangDispensersInLevel;
    public static List<EnemyController> enemiesInLevel;

    public static AudioSource audio;
    public static AudioClip[] allSounds; //0 = game; 1 = boss;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");
        totalStars = totalStarsLvl1;

        boomerangDispensersInLevel = new List<BoomerangDispenser>();
        var tempBoomDispens = GameObject.FindGameObjectsWithTag("NormalBoomerangDispenser");
        foreach (GameObject g in tempBoomDispens)
        {
            boomerangDispensersInLevel.Add(g.GetComponent<BoomerangDispenser>());
        }

        enemiesInLevel = new List<EnemyController>();
        var tempEnemies = GameObject.FindGameObjectsWithTag("Enemy");
        var tempElemEnemies = GameObject.FindGameObjectsWithTag("ElementalEnemy");
        foreach (GameObject e in tempEnemies)
        {
            enemiesInLevel.Add(e.GetComponent<EnemyController>());
        }
        foreach (GameObject e in tempElemEnemies)
        {
            enemiesInLevel.Add(e.GetComponent<EnemyController>());
        }

        audio = GetComponent<AudioSource>();
        allSounds = new AudioClip[2];
        allSounds[0] = Resources.Load<AudioClip>("Audio/thuumdong");
        allSounds[1] = Resources.Load<AudioClip>("Audio/Bosses/EpicBossBattleMusic");
        ChangeMusicToBoss(false);
    }

    public static void SetLevelReady()
    {
        inGorBattle = false;
        gorBattleFinished = false;
        inBirdBattle = false;
        birdBattleFinished = false;
        inGolemBattle = false;
        golemBattleFinished = false;
        gameFinished = false;

        foreach (BoomerangDispenser b in boomerangDispensersInLevel)
        {
            ChangeBoomerangInDispensers("WoodenBoomerang");
        }

        foreach (EnemyController e in enemiesInLevel)
        {
            e.gameObject.SetActive(true);
        }
    }

    public void RespawnPlayer()
    {
        doRespawn = true;

        respawnTimer = 0;
        player.GetComponent<PlayerController>().enabled = false;
        player.GetComponent<SpriteRenderer>().enabled = false;
        player.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        //camera.transform.position = new Vector3(player.transform.position.x, player.transform.position.y, -10);
        camera.GetComponent<CameraController>().IsFollowing = false;
        camera.GetComponent<CameraController>().IsResetting = true;
        //StartCoroutine("RespawnPlayerCo");
    }
    public void Update()
    {
        if (gameFinished)
        {
            player.GetComponent<PlayerController>().ToggleMovement(false);
            foreach (EnemyController e in enemiesInLevel)
            {
                e.gameObject.SetActive(false);
            }
        }

        if (doRespawn)
        {
            respawnTimer += Time.deltaTime;
            if (respawnTimer > respawnDelay)
            {
                respawnTimer = 0;

                camera.GetComponent<CameraController>().IsFollowing = true;
                player.transform.position = currentCheckpoint.transform.position;
                player.GetComponent<PlayerController>().enabled = true;
                player.GetComponent<SpriteRenderer>().enabled = true;
                player.GetComponent<PlayerHealth>().Reset();
                camera.GetComponent<CameraController>().IsResetting = false;
                //player.GetComponent<PlayerHealth>().AddHearts(player.GetComponent<PlayerHealth>().startHearts);
                Instantiate(respawnPartical, currentCheckpoint.transform.position, currentCheckpoint.transform.rotation);
                camera.orthographicSize = 5f;
                doRespawn = false;
            }
        }
    }

    public static void ChangeBoomerangInDispensers(string newBoomerangName)
    {
        GameObject newBoomerang = Resources.Load<GameObject>("Prefabs/Boomerangs/" + newBoomerangName);

        foreach (BoomerangDispenser b in boomerangDispensersInLevel)
        {
            b.DestroyCurrentBoomerang();
            b.boomerangToDispense = newBoomerang;
        }
    }

    public static void ResurrectAllEnemies()
    {
        foreach (EnemyController e in enemiesInLevel)
        {
            e.Resurrect();
        }
    }

    public static void ChangeCheckpoint(GameObject newCheckpoint)
    {
        if(currentCheckpoint != null)
            currentCheckpoint.GetComponent<Checkpoint>().Deactivate();

        currentCheckpoint = newCheckpoint;
    }

    public static void ChangeMusicToBoss(bool isBossBattle)
    {
        audio.clip = (isBossBattle ? allSounds[1] : allSounds[0]);
        audio.Play();
        audio.loop = true;
    }
}
