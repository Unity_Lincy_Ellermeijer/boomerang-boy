﻿using UnityEngine;
using System.Collections;

//This state makes the boss stand still
//This state is often used as warning state, to show the player an attack is comming up
public class StateRest : State<GorillaBossController>
{
    float timer; //the time to stay in this state

    public override void Enter(GorillaBossController agent)
    {
        agent.Anim.SetBool("Rest", true);

        timer = Time.time + agent.mySettings.restTime;
        agent.Exhausted = 0; //reset exhausted var

        agent.StopMovement();
    }

    public override void Execute(GorillaBossController agent)
    {
        //this state is the first state of the boss, so enter() will not be executed that time
        //this check is just to set the timer that one time
        if (timer == 0)
            timer = Time.time + 5;

        if (timer < Time.time) //time to go on to the next state
        {
            if(agent.GetCountMid % agent.RandomCountMidDiv == 0 && agent.GetCountMid != 0)
            {
                agent.GetFSM.ChangeState(agent.GetFSM.PossibleStates["ClimbingUp"]);
            }

            else
            {
                //choose randomly for A or B, but prefer A
                float r = Random.Range(-1f, 1f);
                agent.GetFSM.ChangeState(r < 0.25 ? agent.GetFSM.PossibleStates["RoamingA"] : agent.GetFSM.PossibleStates["RoamingB"]);
            }
        }

    }

    public override void Exit(GorillaBossController agent)
    {
        agent.Anim.SetBool("Rest", false);
    }
}