﻿using UnityEngine;
using System.Collections;

public class GolemPart : MonoBehaviour 
{
	SoundManager sound;
    public bool isBody = false;
    public int damage = 1;
    PlayerHealth playerHealth;
    Knockback knockback;
    GolemController controller;
	void Start ()
    {
		sound = GameObject.Find("Main Camera").GetComponent<SoundManager>();
        playerHealth = GameObject.Find("Player").GetComponent<PlayerHealth>();
        knockback = GameObject.Find("Player").GetComponent<Knockback>();
        controller = transform.parent.GetComponent<GolemController>();
	}
	
	void Update ()
    {
	
	}

    public void OnTriggerEnter2D(Collider2D other)
    {
		if (damage > 0 && other.gameObject.tag == "Player" && controller.health > 0)
        {
            if (playerHealth.currentHearts > 0)
            {
                playerHealth.TakeDamage(damage);


                knockback.KnockBack(this.gameObject);
            }
        }
        if (isBody && (other.gameObject.tag == "StartBoomerang" || other.gameObject.tag == "Boomerang"))
        {
			sound.PlayHitSound ();
            controller.health--;
            controller.manager.UpdateBossHpBar(controller.health);
        }
    }
}
