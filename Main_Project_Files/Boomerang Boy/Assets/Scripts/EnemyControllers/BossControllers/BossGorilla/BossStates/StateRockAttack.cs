﻿using UnityEngine;
using System.Collections;

//This state makes the boss invulnarable and throwing rocks from one of the two highest platforms
public class StateRockAttack : State<GorillaBossController>
{
    float cooldown; //time before next stone
    float attackTime; //time before end attack

    public override void Enter(GorillaBossController agent)
    {
        //set animator

        attackTime = Time.time + agent.mySettings.stayInMassAttckTime.GetRandomInRange();

        agent.Invulnerable = true;
    }

    public override void Execute(GorillaBossController agent)
    {
        //attack is over
        if (attackTime < Time.time)
            agent.GetFSM.ChangeState(agent.GetFSM.PossibleStates["ClimbingDown"]);

        //time to throw another stone!
        if (cooldown < Time.time)
            ThrowRock(agent);
    }

    public override void Exit(GorillaBossController agent)
    {
        agent.Invulnerable = false;
    }

    //Call this method to instantiate a rock an throw it
    void ThrowRock(GorillaBossController agent)
    {
        cooldown = Time.time + agent.mySettings.cooldownThrowAttack.GetRandomInRange(); //set cooldown

        //create a new rock
        GameObject rock = new GameObject();
        rock.transform.position = agent.transform.position;
        rock.transform.rotation = Quaternion.identity;
        var rockScript = rock.AddComponent<StoneScript>();

        Vector2 targetPos = agent.DetermineShootPoint();
        int randomSpriteIndex = Random.Range(0, agent.RockSprites.Count);
        Sprite randomSprite = agent.RockSprites[randomSpriteIndex];

        rockScript.Create(targetPos, randomSprite, agent);
    }

    
}
