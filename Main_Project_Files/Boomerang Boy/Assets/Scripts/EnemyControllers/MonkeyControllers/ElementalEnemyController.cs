﻿using UnityEngine;
using System.Collections;

//This class is derived from WallEdgeEnemy and has the ability to only be killed by the correct elemental boomerang 
public class ElementalEnemyController : WallEdgeEnemyContoller
{
    public enum Element
    {
        fire,
        ice
    };

    public Element element; //my element 

    //No other behaviors, except ability to only be killed by correct elemental boomerang
    public override void TakeDamage(float damage, int element)
    {
        if (((int)this.element == element))
            return;

        base.TakeDamage(damage, element);
    }
}
