﻿using UnityEngine;
using System.Collections;

public class Dash : PlayerController {

    public float dashStrength;
    public float cooldownTimer;

    float timer;
    int direction = 0;

    bool startCooldownTimer;
    bool startTimer;
    bool isDashing;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Q) && timer == 0)
        {
            startTimer = true;
            dash(-1, dashStrength); 
        }

        if (Input.GetKeyDown(KeyCode.E) && timer == 0)
        {
            startTimer = true;
            dash(1, dashStrength);
        }

        if (startTimer)
        {
            timer += Time.deltaTime;

            if(timer >= cooldownTimer)
            {
                startTimer = false;
                timer = 0;
            }
        }
        /*
        if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.A) && !startCooldownTimer && !isDashing)
        {
            startTimer = true;  
        }

        if (startTimer)
        {
            timer += Time.deltaTime;
        }

        if (timer <= 0.25f)
        {
            if (Input.GetKeyDown(KeyCode.D) && timer >= 0.05f)
            {
                direction = 1;
                isDashing = true;
            }

            if (Input.GetKeyDown(KeyCode.A) && timer >= 0.05f)
            {
                direction = -1;
                isDashing = true;
            }
        }
        else
        {
            direction = 0;
            timer = 0;
            startTimer = false;
        }

        if (isDashing && !startCooldownTimer)
        {
            dash(direction, dashStrength);
            startCooldownTimer = true;

            isDashing = false;
        }

        if (startCooldownTimer)
        {
            cooldownTimer += Time.deltaTime;
            if (cooldownTimer >= .5f)
            {
                startCooldownTimer = false;
                cooldownTimer = 0;
            }
        } */
    }


    void dash(int direction, float force)
    {
        Vector2 vel = rb.velocity;

        vel = new Vector2(vel.x + (force * direction), vel.y);

        rb.velocity = vel;
    }
}