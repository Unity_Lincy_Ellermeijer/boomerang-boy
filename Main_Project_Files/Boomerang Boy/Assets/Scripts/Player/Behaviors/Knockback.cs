﻿using UnityEngine;
using System.Collections;

public class Knockback : PlayerController {


    public bool canBeDealtDamageByBoomerang;

    public void KnockBack(Vector3 impactPos)
    {
        Vector2 diff = impactPos - transform.position;
        diff.Normalize();
        if (diff.x > 0)
            diff.x = 1;
        else
            diff.x = -1;
        rb.AddForce(-diff * 1000f);
    }

    public void KnockBack(GameObject otherObj)
    {
        Vector2 diff = otherObj.transform.position - transform.position;
        diff.Normalize();
        if (diff.x > 0)
            diff.x = 1;
        else
            diff.x = -1;
        rb.AddForce(-diff * 1000f);
    }
}
