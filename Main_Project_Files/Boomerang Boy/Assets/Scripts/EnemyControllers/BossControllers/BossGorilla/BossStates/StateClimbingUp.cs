﻿using UnityEngine;
using System.Collections;

//This state makes the boss jump from ground to the platform in the center to one of the two highest platforms
//It's always executed before the boss will throw monkeys or rocks
public class StateClimbingUp : State<GorillaBossController>
{
    bool passedMid; //are we passed mid yet? better: split this state in two states
    float r; //random var
    float waitTime; //time to wait after a jump
    float movementScalar;

    public override void Enter(GorillaBossController agent)
    {
        agent.Anim.SetBool("Rest", true);

        agent.ChangePath(agent.DetermineSideRoom() ? "ClimbingRightA" : "ClimbingLeftA", 2); //change path based on where the boss is

        agent.ThisDetermineNextPoint = agent.DetermineNextPointIncreasing;

        r = Random.Range(-1, 1); //random value for determining path and next state

        agent.Jump(agent.GetCurrentPoint.transform.position); //we can only enter this state close to the platform in the center, so we jump on it immediately 
        movementScalar = 0.7f;
    }

    public override void Execute(GorillaBossController agent)
    {
        MoveToPoint(agent);
    }

    public override void Exit(GorillaBossController agent)
    {
        waitTime = 0;
        passedMid = false; //reset for next time we're in this state
    }

    //This method contains all logic to make the boss jump up and move
    void MoveToPoint(GorillaBossController agent)
    {
        //we need to get the next point
        if (agent.GetDistToPoint < agent.GetMinDistToPoint && !passedMid)
        {
            //we landed on the platform and are not waiting yet
            if(waitTime == 0 && agent.IsGrounded)
            {
                waitTime = Time.time + 0.5f;
                agent.StopMovement();
            }

            //we're done waiting 
            if (waitTime < Time.time && waitTime > 0)
            {
                //jump to one of the two highest platforms
                agent.ChangePath(r < 0 ? "ClimbingLeftB" : "ClimbingRightB", 2);
                movementScalar = 1.9f;
                agent.Jump(agent.GetCurrentPoint.transform.position + new Vector3(0, 10, 0));
                passedMid = true;
            }
        }

        //if we need to get a next point, but passedmid is true
        //we did all the jumps we had to do
        if (agent.GetDistToPoint < agent.GetMinDistToPoint && passedMid)
            agent.GetFSM.ChangeState(r < 0 ? agent.GetFSM.PossibleStates["MonkeyAttack"] : agent.GetFSM.PossibleStates["RockAttack"]);

        if (waitTime < Time.time)
            agent.SetMovementSpeed((agent.GetCurrentPoint.transform.position - agent.transform.position).normalized * movementScalar);
        else
            agent.StopMovement();
    }
}