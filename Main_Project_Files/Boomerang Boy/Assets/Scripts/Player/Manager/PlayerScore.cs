﻿using UnityEngine;
using System.Collections;

public class PlayerScore : MonoBehaviour
{
	public Texture2D star_Tex;
	int amountStar = 0;
	void Start () 
	{
	
	}
	public void AddStar()
	{
		amountStar++;
	}

	void OnGUI()
	{
		for (int i = 0; i < amountStar; i++)
		{
			GUI.DrawTexture(new Rect(Screen.width-star_Tex.width-10 -i * 15, 10, 35, 35), star_Tex);
		}
	}

	void Update ()
	{
	
	}
}
