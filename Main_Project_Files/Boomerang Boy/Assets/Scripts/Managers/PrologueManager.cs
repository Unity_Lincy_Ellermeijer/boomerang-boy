﻿using UnityEngine;
using System.Collections;

public class PrologueManager : MonoBehaviour
{

    PlayerController player;
    Camera camera;
    CameraController cameraController;

    public AutomaticTalkScript triggerC;

    public float evilMonkeyTime;
    public ChangePlayerInRange enteredPartB;

    public GameObject evilMonkey;
    GameObject instanceEvilMonkey;
    GameObject boomerang;
    Animator evilMonkeyAC;
    bool wayback;
    Vector3 start;
    Vector3 end;
    float distToPoint;
    float minDistToPoint = 0.3f;
    Vector3 currentPoint;

    float timerB;
    bool partAOn = false;
    bool partAFinished = false;
    bool partBOn = false;
    bool partBFinished = false;
    bool displayExplanation = false;

    public virtual void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        camera = FindObjectOfType<Camera>();
        cameraController = camera.GetComponent<CameraController>();

        start = new Vector3(40, -5.75f, 0);
        end = new Vector3(32, -5.75f, 0);
    }


    public virtual void Update()
    {

        if (partBOn && !partBFinished)
        {
            timerB += Time.deltaTime;

            distToPoint = (instanceEvilMonkey.transform.position - currentPoint).magnitude;

            if (distToPoint > minDistToPoint)
                instanceEvilMonkey.transform.position = Vector3.Lerp(instanceEvilMonkey.transform.position, currentPoint, 1 * Time.deltaTime);

            if (distToPoint < minDistToPoint && !wayback)
            {
                player.GetComponent<Boomerang>().Steal(instanceEvilMonkey, new Vector3(1f, 0.5f, 0f));
                //TO DO:
                //Steal boomerang from player
                //Make boomerang move with monkey
                currentPoint = start;
                distToPoint = (instanceEvilMonkey.transform.position - currentPoint).magnitude;
                wayback = true;
            }

            if (wayback)
                instanceEvilMonkey.transform.position = Vector3.Lerp(instanceEvilMonkey.transform.position, currentPoint, 1 * Time.deltaTime);

            if (distToPoint < minDistToPoint && wayback)
                ExitB();
            
        }

        if(enteredPartB.playerInZone && !partBOn && !partBFinished)
        {
            cameraController.IsFollowing = false;
            player.ToggleMovement(false);
            MonkeyB();
            partBOn = true;
        }

        if (triggerC.doneTalking)
        {
            //LevelManager.SetLevelReady();
            Application.LoadLevel("level01");
        }
    }

    void MonkeyB()
    {
        instanceEvilMonkey = Instantiate(evilMonkey, new Vector2(40.12f, -5.74f), Quaternion.identity) as GameObject;
        evilMonkeyAC = instanceEvilMonkey.GetComponent<Animator>();
        currentPoint = end;
    }

    void ExitB()
    {
        partBFinished = true;
        partBOn = false;

        Destroy(instanceEvilMonkey);

        cameraController.IsFollowing = true;
        player.ToggleMovement(true);

        //Talk: and that's how the journey begun... 
    }
}
