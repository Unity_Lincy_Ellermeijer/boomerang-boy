﻿using UnityEngine;
using System.Collections;

public class WallSlide : PlayerController {

	public float slideVelocity;
	public float slideMultiplier;
    public bool onWallDetected;
    public float stickTime;

    float stickTimer = 0f;
    float timeElapsed = 0f;

    void Update () {

        if (collisionState.leftOnWall || collisionState.rightOnWall)
        {
            if (!onWallDetected)
            {
                OnStick();
                onWallDetected = true;
                ToggleScripts(false);
            }

        }
        else
        {
            if (onWallDetected)
            {
                OffWall();
                onWallDetected = false;
                ToggleScripts(true);
                stickTimer = 0;
            }
        }

        if (onWallDetected && !collisionState.standing)
        {
            float velY = slideVelocity;

			if(Input.GetKey(KeyCode.S))
				velY *= slideMultiplier;

            if(collisionState.leftOnWall && Input.GetKey(KeyCode.D))
            {
                stickTimer += Time.deltaTime;

                if (stickTimer >= stickTime)
                {
                    ToggleScripts(true);
                    stickTimer = 0;
                }
            }

            if(collisionState.rightOnWall && Input.GetKey(KeyCode.A))
            {
                stickTimer += Time.deltaTime;

                if (stickTimer >= 0.1f)
                {
                    ToggleScripts(true);
                    stickTimer = 0;
                }
            }

            rb.velocity = new Vector2(rb.velocity.x, velY);

			timeElapsed += Time.deltaTime;
		}

        if(onWallDetected && collisionState.standing)
        {
            ToggleScripts(true);
        }
    }

	void OnStick(){
		rb.velocity = Vector2.zero;
	}

	void OffWall(){
		timeElapsed = 0;
	}
} 
