﻿using UnityEngine;
using System.Collections;

public class BetweenPointsEnemyController : EnemyController
{
    public Transform currentPoint;
    public GameObject[] path;
    public int pointSelection = 1;

    public float minDistToPoint = 0.7f;
    [HideInInspector]
    public float distToPoint = 0f;

    public override void Start()
    {
        base.Start();
        currentPoint = path[pointSelection].transform;
    }

    public override void Update()
    {
        base.Update();

        distToPoint = (transform.position - currentPoint.position).magnitude;

        if (distToPoint < minDistToPoint)
        {
            Flip();
            DetermineNextPoint();
        }

        rb.velocity = (currentPoint.transform.position - transform.position).normalized * moveSpeed;
    }

    public void DetermineNextPoint()
    {
        pointSelection++;

        if (pointSelection >= path.Length)
        {
            pointSelection = 0;
        }

        currentPoint = path[pointSelection].transform;
    }

    public virtual void OnCollisionEnter2D(Collision2D other)
    {
        base.OnCollisionEnter2D(other);

        if (other.gameObject.tag == "Player")
        {
            Flip();
        }
    }
}
