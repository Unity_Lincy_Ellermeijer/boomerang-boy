﻿using UnityEngine;
using System.Collections;

public class WallJump : PlayerController {

	public AudioClip jumpSound;
	AudioSource audio;
	public Vector2 jumpVelocity;
	public bool jumpingOffWall;
	public float resetDelay;

	private float timeElapsed = 0;

    int jumpDirection;

    // Update is called once per frame
    void Update () {

        if (collisionState.rightOnWall && !collisionState.standing) {

            if (Input.GetKeyDown(KeyCode.Space) && !jumpingOffWall){

                ToggleScripts(false);

                rb.velocity = new Vector2(jumpVelocity.x * -1, jumpVelocity.y);

				jumpingOffWall = true;
				GetComponent<AudioSource>().PlayOneShot(jumpSound, 0.7F);
			}

		}

        if (collisionState.leftOnWall && !collisionState.standing)
        {

            if (Input.GetKeyDown(KeyCode.Space) && !jumpingOffWall)
            {

                ToggleScripts(false);

                rb.velocity = new Vector2(jumpVelocity.x * 1, jumpVelocity.y);

                jumpingOffWall = true;
				GetComponent<AudioSource>().PlayOneShot(jumpSound, 0.7F);
            }

        }

        if (jumpingOffWall) {
			timeElapsed += Time.deltaTime;

            
            if (timeElapsed > resetDelay){
                ToggleScripts(true);
                jumpingOffWall = false;
				timeElapsed = 0;
			}
		}

	}
}
