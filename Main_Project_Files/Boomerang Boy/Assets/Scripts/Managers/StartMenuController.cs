﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StartMenuController : MonoBehaviour
{
    bool enterPressed = false;
    public Text startText;
    public Canvas optionsMenu;
    public Canvas buttons;
    public AudioSource testMusic;

    void Start()
    {
        startText.text = "PRESS 'ENTER' TO CONTINUE";
        startText.enabled = true;
        optionsMenu.enabled = false;
        buttons.enabled = false;
    }

    void Update()
    {
        if (Input.GetKeyDown("return") && enterPressed == false)
        {
            enterPressed = true;
            startText.enabled = false;
            buttons.enabled = true;
        }
    }

    public void StartButtonClicked()
    {
        Application.LoadLevel("PrologueLevel");
    }

    public void OptionsButtonClicked()
    {
        optionsMenu.enabled = true;
        buttons.enabled = false;
    }

    public void VolumeControl(float volumeControl)
    {
        testMusic.volume = volumeControl;
    }

    public void BackButtonClicked()
    {
        optionsMenu.enabled = false;
        buttons.enabled = true;
    }

    public void CreditsButtonClicked()
    {
        Application.LoadLevel("Credits");
    }

    public void ExitButtonClicked()
    {
        Application.Quit();
    }
}
