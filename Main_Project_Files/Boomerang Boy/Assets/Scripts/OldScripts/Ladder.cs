﻿using UnityEngine;
using System.Collections;

public class Ladder : MonoBehaviour
{
    CollisionState collisionState;

    public void Start()
    {
        collisionState = FindObjectOfType<CollisionState>();
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            collisionState.onLadder = true;
        }
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            collisionState.onLadder = false;
        }
    }
}


