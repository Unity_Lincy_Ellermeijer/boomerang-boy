﻿using UnityEngine;
using System.Collections;

public class GorBossManager : BossRoomManager
{
    public GameObject boomerangDispenser; //drops the boomerang needed in battle
    public GameObject rightEndRoom, leftEndRoom; //end of room to keep the player in

    public override void Start()
    {
        base.Start();

        boundary = GetComponent<BoxCollider2D>();
        min = boundary.bounds.min;
        max = boundary.bounds.max;
    }

    public override void Update()
    {
        base.Update();

        //boss dead
        if (!LevelManager.inGorBattle && BattleOn)
        {
            Exit();
        }
    }

    //Make boss ready to battle
    public override void SetUpBoss()
    {
        base.SetUpBoss();
        rightEndRoom.GetComponent<BoxCollider2D>().isTrigger = false;
        leftEndRoom.GetComponent<BoxCollider2D>().isTrigger = false;
        LevelManager.inGorBattle = true;
        boomerangDispenser.SetActive(true);
        SpawnBossHpBar((int)spawnedBoss.GetComponent<GorillaBossController>().mySettings.startHealth);
    }

    //Method to exit boss battle if boss died
    public override void Exit()
    {
        base.Exit();
        LevelManager.gorBattleFinished = true;

        rightEndRoom.GetComponent<BoxCollider2D>().isTrigger = true;
        leftEndRoom.GetComponent<BoxCollider2D>().isTrigger = true;
    }

    //Method to exit boss battle if player died
    public override void Reset()
    {
        if (spawnedBoss != null)
        {
            spawnedBoss.GetComponent<GorillaBossController>().ClearBattle();
        }
        base.Reset();

        rightEndRoom.GetComponent<BoxCollider2D>().isTrigger = true;
        leftEndRoom.GetComponent<BoxCollider2D>().isTrigger = true;
    }
}
