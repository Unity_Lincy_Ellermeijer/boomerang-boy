﻿using UnityEngine;
using System;

//Birdboss has an instance of this class to set all his adjustable variables
[Serializable]
public class BirdBossSettings
{
    [Header("Health Settings")]
    [SerializeField]
    public float startHealth = 5f;

    [Header("Movement Settings")]
    [SerializeField]
    public float movementSpeed = 10f;
    [SerializeField]
    public float climbScalar = 3f;
    [SerializeField]
    public float diveScalar = 5f;
    [SerializeField]
    public float growScalar = 2f;

    [Header("Egg and Bird Settings")]
    [SerializeField]
    public Range eggsToSpawn = new Range(1, 3);
    [SerializeField]
    public float startHatchTime = 3f;
    [SerializeField]
    public float hatchTime = 1.5f;
    [SerializeField]
    public float startFlyingTime = 1f;
    [SerializeField]
    public float movementSpeedMinion = 6f;

    [Header("Icicle Settings")]
    [SerializeField]
    public Range icicleSpeed = new Range(10, 16);
    [SerializeField]
    public Range iciclesToSpawn = new Range(6, 9);

    [Header("Flame Settings")]
    [SerializeField]
    public Range flameSpeed = new Range(7, 15);
    [SerializeField]
    public Range flamesToSpawn = new Range(5, 7);

    [Header("Feather Hurt Particle Settings")]
    [SerializeField]
    public Range amountFeatherHurt = new Range(3, 5);
    [SerializeField]
    public Range speedFeatherHurt = new Range(1, 2);
    [SerializeField]
    public Range scaleFeatherHurt = new Range(0.4f, 0.8f);
    [SerializeField]
    public float lifeTimeFeatherHurt = 1.5f, fadeTimeFeatherHurt = 0.5f;

    [Header("Feather Dead Particle Settings")]
    [SerializeField]
    public Range amountFeatherDead = new Range(20, 25);
    [SerializeField]
    public Range speedFeatherDead = new Range(1, 3);
    [SerializeField]
    public Range scaleFeatherDead = new Range(0.7f, 1);
    [SerializeField]
    public float lifeTimeFeatherDead = 10f, fadeTimeFeatherDead = 7f;

    [Header("Check Variables")]
    [SerializeField]
    public Range dividerCountMid = new Range(4, 6);
    [SerializeField]
    public float startMassAttackTimer = 50f;
    [SerializeField]
    public Range stayInMassAttckTimer = new Range(5, 7);
    [SerializeField]
    public float cooldownMassAttack = 1.5f;
    [SerializeField]
    public float dieTime = 2f;
}
