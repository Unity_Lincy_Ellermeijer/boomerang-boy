﻿using UnityEngine;
using System.Collections;

//This class will always be on an object that is a child of a checkpoint
//It triggers when the checkpoint is active and gives fire animation as visual feedback
public class CampfireController : MonoBehaviour
{
    Animator anim;

	void Start ()
    {
        anim = GetComponent<Animator>();
	}
	
	public void Activate()
    {
        anim.SetBool("On", true);
    }

    public void Deactivate()
    {
        anim.SetBool("On", false);
    }
}
