﻿using UnityEngine;
using System.Collections;

//This state makes the boss jump down again
//Executed after an attack from one of the highest platforms
public class StateClimbingDown : State<GorillaBossController>
{
    bool passedMid; //var to check if we're passed mid. Better solution: split this state in two states
    float waitTime; //time to wait after a jump
    float movementScalar;

    public override void Enter(GorillaBossController agent)
    {
        agent.Anim.SetBool("Rest", true);

        agent.ChangePath(agent.DetermineSideRoom() ? "ClimbingLeftA" : "ClimbingRightA", 2);

        agent.ThisDetermineNextPoint = agent.DetermineNextPointDecreasing;

        agent.Jump(new Vector2(agent.GetCurrentPoint.transform.position.x, agent.GetCurrentPoint.transform.position.y - 10f)); //since we're on one of the highest platforms for sure, jump of it
        movementScalar = 1.9f;
    }

    public override void Execute(GorillaBossController agent)
    {
        MoveToPoint(agent);
    }

    public override void Exit(GorillaBossController agent)
    {
        waitTime = 0;
        passedMid = false;
        agent.RandomCountMidDiv = (int)agent.mySettings.dividerCountMid.GetRandomInRange() + agent.GetCountMid;
        agent.Anim.SetBool("Rest", false);
    }

    void MoveToPoint(GorillaBossController agent)
    {
        //we need to determine a new point and we didn't pass mid yet
        if (agent.GetDistToPoint < agent.GetMinDistToPoint && !passedMid)
        {
            //we just landed on the platform in the center
            if (waitTime == 0 && agent.IsGrounded)
            {
                waitTime = Time.time + 0.5f;
                agent.StopMovement();
            }

            //we're done waiting
            if (waitTime < Time.time && waitTime > 0)
            {
                //jump to ground of boss room
                agent.ThisDetermineNextPoint();
                movementScalar = 0.9f;
                agent.Jump(agent.GetCurrentPoint.transform.position + new Vector3(0, 0, 0));
                passedMid = true;
            }
        }

        //we need to determine a new point, but we passed mid, so we're on the ground
        if (agent.GetDistToPoint < agent.GetMinDistToPoint && passedMid)
            agent.GetFSM.ChangeState(agent.GetFSM.PossibleStates["RoamingA"]);

        if (waitTime < Time.time)
            agent.SetMovementSpeed((agent.GetCurrentPoint.transform.position - agent.transform.position).normalized * movementScalar);
        else
            agent.StopMovement();
    }
}