﻿using UnityEngine;
using System.Collections;

public class MushroomController : MonoBehaviour
{
    public GameObject[] poisonDrop;
    private Vector3[] spawnPosition;
    bool isSpawning = false;
    public float minTime = 1.0f;
    public float maxTime = 3.0f; 
    // Maybe test these values out a bit more, I think 1 and 3 are nice

    void Start()
    {
        spawnPosition = new Vector3[3] {new Vector3(-2, -0.5f), new Vector3(0, -0.5f), new Vector3(2, -0.5f) };
    }

    IEnumerator SpawnPoison(int i, float seconds)
    {
        //Debug.Log("Waiting for " + seconds + " seconds");
        // Waits for a certain amount of time and then sets the bool to true and spawns a poisonDrop
        yield return new WaitForSeconds(seconds);
        Instantiate(poisonDrop[i], transform.position + spawnPosition[i], Quaternion.identity);
    
        isSpawning = false;
    }

    void Update()
    {
        if (!isSpawning)
        {
            // Spawns a new poisondrop if isSpawning is true (so after a few seconds)
            isSpawning = true;
            int poisonIndex = Random.Range(0, poisonDrop.Length);
            StartCoroutine(SpawnPoison(poisonIndex, Random.Range(minTime, maxTime)));
        }
    }
}
