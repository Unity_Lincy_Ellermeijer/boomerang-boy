﻿using UnityEngine;
using System.Collections;

public class ObstacleController : MonoBehaviour
{

    [HideInInspector] public BoxCollider2D collider;
    [HideInInspector] public GameObject player;
    [HideInInspector] public PlayerHealth playerHealth;
    [HideInInspector] public PlayerController playerController;
    [HideInInspector] public Knockback knockback;

    public int damage;

    public virtual void Start()
    {
        collider = GetComponent<BoxCollider2D>();
        player = GameObject.FindGameObjectWithTag("Player");
        playerHealth = FindObjectOfType<PlayerHealth>();
        playerController = FindObjectOfType<PlayerController>();
        knockback = FindObjectOfType<Knockback>();
    }

    public virtual void Update()
    {
        
    }

    public virtual void OnCollisionEnter2D(Collision2D other)
    {
        if (damage > 0 && other.gameObject.tag == "Player")
        {
            if (playerHealth.currentHearts > 0)
            {
                playerHealth.TakeDamage(damage);

                knockback.KnockBack(this.gameObject);
            }
        }
    }
}
