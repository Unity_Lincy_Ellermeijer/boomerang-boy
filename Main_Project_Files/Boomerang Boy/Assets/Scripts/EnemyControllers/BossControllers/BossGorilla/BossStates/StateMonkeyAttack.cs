﻿using UnityEngine;
using System.Collections;

//This state makes the boss throw monkeys from one of the two highest platforms
//it also makes him invulnerable
public class StateMonkeyAttack : State<GorillaBossController>
{
    float cooldown; //time before throwing next monkey
    float attackTime; //total time of the attack

    public override void Enter(GorillaBossController agent)
    {
        attackTime = Time.time + agent.mySettings.stayInMassAttckTime.GetRandomInRange();

        agent.Invulnerable = true;
    }

    public override void Execute(GorillaBossController agent)
    {
        if (attackTime < Time.time)
            agent.GetFSM.ChangeState(agent.GetFSM.PossibleStates["ClimbingDown"]);

        if (cooldown < Time.time)
            ThrowMonkey(agent);
    }

    public override void Exit(GorillaBossController agent)
    {
        agent.Invulnerable = false;
    }

    //Call this method to throw a monkey!
    void ThrowMonkey(GorillaBossController agent)
    {
        cooldown = Time.time + (agent.mySettings.cooldownThrowAttack.GetRandomInRange() * 3f); //setting cooldown

        //creating a new monkey here...
        GameObject monkeyDrop = new GameObject();
        monkeyDrop.transform.position = agent.transform.position;
        monkeyDrop.transform.rotation = Quaternion.identity;
        var monkeyDropScript = monkeyDrop.AddComponent<MonkeyDropScript>();

        Vector2 targetpos = agent.DetermineShootPoint();
        Sprite thisSprite = Resources.Load<Sprite>("Art/Sprites/Enemies/Monkeys/throwNormal");
        monkeyDropScript.Create(targetpos, thisSprite, agent);
    }
}