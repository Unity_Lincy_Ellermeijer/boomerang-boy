﻿using UnityEngine;
using System.Collections;

public class SwoopingEnemyMovement : BetweenPointsEnemyController
{
    public float swoopSpeed; //achtervolgingspeed

    //to control current point?
	public bool heenWeg;
	public bool terugWeg;
	public bool tussenWeg;

    //bool attackTarget = false;
    public bool playerInRange = false;

    public float minDistToPlayer;
    [HideInInspector] public float distToPlayer = 0f; 
    
    private Vector3 targetPos; //isn't same as currentpoint?
	private Vector3 tussenPos; //?
	private Vector3 endPos; //?
    

	// Use this for initialization
    public override void Start() 
	{
        base.Start();
	}

    public override void Update() 
    {
        base.Update();

        distToPlayer = (transform.position - player.transform.position).magnitude;

        if (distToPlayer < minDistToPlayer)
        {
            playerInRange = true;
            rb.velocity = (new Vector3(player.transform.position.x, player.transform.position.y, 0f) - transform.position).normalized * moveSpeed;
        }
        else
        {
            playerInRange = false;
        }

        /*if (playerInRange && !attackTarget)
        {
            attackTarget = true;
            targetPos = player.transform.position;
            endPos = transform.position;
            heenWeg = true; //begin attack
        }*/

        //if attacktarget = true
        /*if (attackTarget)
        {
            //move towards player	
            if (heenWeg)
            {
                transform.position = Vector3.MoveTowards(transform.position, targetPos, swoopSpeed * Time.deltaTime);
            }

            //in case enemy reaches players old pos set new position
            if (transform.position == targetPos)
            {
                heenWeg = false;
                tussenPos = player.transform.position;
                tussenWeg = true;
            }

            //?
            if (tussenWeg)
            {
                transform.position = Vector3.MoveTowards(transform.position, tussenPos, swoopSpeed * Time.deltaTime);
            }

            if (transform.position == tussenPos)
            {
                tussenWeg = false;
                terugWeg = true;
            }

            if (terugWeg)
            {
                transform.position = Vector3.MoveTowards(transform.position, endPos, swoopSpeed * Time.deltaTime);
            }

            if (transform.position == endPos)
            {
                terugWeg = false;
                attackTarget = false;
            }
        }*/
	}
}
