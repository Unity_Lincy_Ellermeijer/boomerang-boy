﻿using UnityEngine;
using System.Collections;

public class EventScript : MonoBehaviour {

    public GameObject evilMonkey;
    public Transform[] cameraPathPoints;
    public float speed;

    Transform startPoint, endPoint;

    Camera camera;
    CameraController cameraController;
    PlayerController player;

    int currentStartPoint;
    float startTime;
    float journeyLength;
    public bool isTriggered;
    float lerpPercentage;

    public void Start()
    {
        camera = FindObjectOfType<Camera>();
        cameraController = camera.GetComponent<CameraController>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        currentStartPoint = 0;
        lerpPercentage = 0;
        SetPoints();
    }

    public void Update()
    {
        if (isTriggered) //CameraController.isFollowing = false, not needed for some reason.
        {
            evilMonkey.transform.position = cameraPathPoints[0].position;
            cameraController.IsFollowing = false;
            evilMonkey.gameObject.SetActive(true);
            player.ToggleMovement(false);

            //monkey misschien bewegen.

            lerpPercentage += Time.deltaTime * speed;
            camera.transform.position = Vector2.Lerp(startPoint.position, endPoint.position, lerpPercentage);
            camera.transform.position = new Vector3(camera.transform.position.x, camera.transform.position.y, -10);
            if (lerpPercentage >= 1f && currentStartPoint + 1 < cameraPathPoints.Length)
            {
                lerpPercentage = 0;
                currentStartPoint++;
                SetPoints();
            }
           
            if(currentStartPoint + 1 == cameraPathPoints.Length)
            {
                isTriggered = false;
                evilMonkey.gameObject.SetActive(false);
                cameraController.IsFollowing = true;
                player.ToggleMovement(true);
            }
        }
    }

    void SetPoints()
    {
        startPoint = cameraPathPoints[currentStartPoint];
        endPoint = cameraPathPoints[currentStartPoint + 1];
    }

}
