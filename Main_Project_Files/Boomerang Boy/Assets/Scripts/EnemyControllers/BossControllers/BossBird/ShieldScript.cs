﻿using UnityEngine;
using System.Collections;

//Shield for bird boss
public class ShieldScript : MonoBehaviour 
{
    //Call this method to create a shield with fire or ice element
    public void Create(int tempElement)
    {
        SpriteRenderer spriteRender = gameObject.AddComponent<SpriteRenderer>();
        CircleCollider2D coll = gameObject.AddComponent<CircleCollider2D>();
        coll.offset = new Vector2(0, -0.4f);
        coll.radius = 1.9f;

        gameObject.tag = "BirdShield";

        Sprite tempSprite = Resources.Load<Sprite>("Art/Sprites/Enemies/Boss/Bird/shieldIce");

        if (tempElement == 1)
            spriteRender.sprite = Resources.Load<Sprite>("Art/Sprites/Enemies/Boss/Bird/shieldIce");
        else if (tempElement == 0)
            spriteRender.sprite = Resources.Load<Sprite>("Art/Sprites/Enemies/Boss/Bird/shieldFire");
        
        spriteRender.sortingLayerName = "Particles";

        gameObject.layer = 11;
    }
}
