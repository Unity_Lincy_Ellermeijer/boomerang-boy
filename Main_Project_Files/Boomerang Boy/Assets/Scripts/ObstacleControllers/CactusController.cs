﻿using UnityEngine;
using System.Collections;

public class CactusController : ObstacleController
{
    private Vector3[] spawnPosition;
    private float[] spawnRotation;
    public GameObject cactusSpike;
    private float fireRate = 2f;
    private float nextFire = 0.0f;
    public float speed;

    public override void Start()
    {
        spawnPosition = new Vector3[5] { new Vector3(0, 0), new Vector3(0, 0), new Vector3(0, 0), new Vector3(0, 0), new Vector3(0, 0) };
        spawnRotation = new float[5] {50,70,0,290,310};
        base.Start();
    }

    public override void Update()
    {
        if (Time.time > nextFire) 
        {
            nextFire = Time.time + fireRate;

            for (int i = 0; i < 5; i++)
            {
                GameObject spike = Instantiate(cactusSpike, transform.position + spawnPosition[i], Quaternion.identity) as GameObject;
                spike.transform.eulerAngles = new Vector3(spike.transform.eulerAngles.x, spike.transform.eulerAngles.y, spawnRotation[i]);
                spike.GetComponent<Rigidbody2D>().AddForce(spike.transform.up * speed);
            }
        }
    }
}
