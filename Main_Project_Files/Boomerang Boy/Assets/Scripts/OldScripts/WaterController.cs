﻿using UnityEngine;
using System.Collections;

public class WaterController : ObstacleController {

    public bool playerIsIn;

    public  void OnTriggerEnter2D(Collider2D other)
    {
        if(other.transform.tag == "Player")
        {
            playerIsIn = true;
        }
    }

    public void OnTriggerExit2D(Collider2D other)
    {
        if (other.transform.tag == "Player")
        {
            playerIsIn = false;
        }
    }
}
