﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//This class is the controller of the gorilla boss, which controls the states, paths ect.
public class GorillaBossController : MonoBehaviour
{
	SoundManager sound;
    public GorillaBossSettings mySettings;
    GorBossManager manager;

    //statemachine ref and property
    StateMachine<GorillaBossController> stateMachine;
    public StateMachine<GorillaBossController> GetFSM { get { return stateMachine; } set { stateMachine = value; } }

    //path variables
    Dictionary<string, PathScript> paths = new Dictionary<string, PathScript>(); //ref to all paths gorilla can follow
    PathScript currentPath; //ref to current path to follow, will be passed to global state to make gorilla follow path
    Transform currentPoint; //ref to current point
    public Transform GetCurrentPoint { get { return currentPoint; } }
    int pointSelection;
    public int GetPointSelection { get { return pointSelection; } }

    //health variables
    public bool Invulnerable { get; set; }
    public bool Dead { get; set; }
    float health;
    public float Health
    {
        get { return health; }
        set
        {
            health = value;
            if (health <= 0)
            {
                Die();
            }
        }
    }

    //movement variables
    Transform groundCheck;
    Transform edgeCheck;
    public bool IsGrounded { get { return Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground")); } }
    public bool AtEdge { get { return !Physics2D.Linecast(transform.position, edgeCheck.position, 1 << LayerMask.NameToLayer("Ground")); } }
    float MoveSpeed { get { return mySettings.movementSpeed; } }
    float JumpSpeed { get { return mySettings.jumpSpeed; } }
    public int JumpCooldown { get; set; }
    public bool IsFacingLeft { get; set; }
    public bool JumpingDown { get; set; }
    public float Exhausted { get; set; }

    //delegate for determining next point in the path
    public delegate void DetermineNextPoint();
    public DetermineNextPoint ThisDetermineNextPoint;

    Rigidbody2D rb;
    public Vector2 GetVelocity { get { return rb.velocity; } }
    SpriteRenderer sprRender;
    public Animator Anim { get; set; }
    public GameObject Player { get; set; }
    PlayerHealth playerHealth;
    Knockback knockback;

    //checks for dist to current target point
    float distToPoint;
    public float GetDistToPoint { get { return (transform.position - currentPoint.position).magnitude; } }
    float minDistToPoint = 1f;
    public float GetMinDistToPoint { get { return minDistToPoint; } }

    //boundary bossroom to check conditions in certain states for example diving
    BoxCollider2D boundary;
    Vector3 min, max;
    float cameraHalfWidth, cameraSize;
    public float GetMinXPos { get { return min.x; } }
    public float GetMinYPos { get { return min.y; } }
    public float GetMaxXPos { get { return max.x; } }
    public float GetMaxYPos { get { return max.y; } }

    //refs to certain points in bossroom
    public Transform MidPointRoom { get; set; }
    public Transform LeftPointUnderRoom { get; set; }
    public Transform RightPointUnderRoom { get; set; }

    //variables that determines change of state
    int countMid = 0; //if coundMid % x == 0 > do dive attack
    public int GetCountMid { get { return countMid; } }
    public int RandomCountMidDiv { get; set; }

    //variables for stone attack
    public List<Sprite> RockSprites { get; set; }

    //particle information
    public Sprite BananaParticle { get; set; }
    int randomAmount;
    Vector2[] randomDirections;
    float[] randomMovespeeds;
    Sprite[] randomSprites;
    float[] randomScales;
    float lifeTimeBanana;
    float fadeTimeBanana;

    void Start()
    {
		sound = GameObject.Find("Main Camera").GetComponent<SoundManager>();
        manager = GameObject.FindGameObjectWithTag("GorBossManager").GetComponent<GorBossManager>();

        health = mySettings.startHealth;
        IsFacingLeft = true;

        groundCheck = GameObject.FindGameObjectWithTag("GorGroundCheck").transform;
        edgeCheck = GameObject.FindGameObjectWithTag("GorEdgeCheck").transform;
        JumpCooldown = 2;

        ThisDetermineNextPoint = DetermineNextPointIncreasing;

        MidPointRoom = GameObject.FindGameObjectWithTag("GorBossMidPoint").transform;

        //get all possible paths
        var tempPaths = GameObject.FindGameObjectsWithTag("GorBossPath");
        foreach (GameObject p in tempPaths)
        {
            paths.Add(p.name, p.GetComponent<PathScript>());
        }

        rb = GetComponent<Rigidbody2D>();
        sprRender = GetComponent<SpriteRenderer>();
        Anim = GetComponent<Animator>();

        Player = GameObject.FindGameObjectWithTag("Player");
        playerHealth = Player.GetComponent<PlayerHealth>();
        knockback = Player.GetComponent<Knockback>();

        RandomCountMidDiv = (int)mySettings.dividerCountMid.GetRandomInRange();

        RockSprites = new List<Sprite>();
        for (int i = 1; i < 5; i++)
        {
            RockSprites.Add(Resources.Load<Sprite>("Art/Sprites/Enemies/Boss/Gorilla/Attacks/Stones/stone" + i));
        }

        BananaParticle = Resources.Load<Sprite>("art/Sprites/Enemies/Boss/Gorilla/ParticleBanana");

        camera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<CameraController>();

        SetUpStateMachine();
    }

    //Call this method to set the FSM ready
    void SetUpStateMachine()
    {
        stateMachine = new StateMachine<GorillaBossController>(this);

        //instantiate all possible states
        StateBeserk stateBeserk = new StateBeserk();
        StateClimbingDown stateClimbingDown = new StateClimbingDown();
        StateClimbingUp stateClimbingUp = new StateClimbingUp();
        StateDyingGor stateDying = new StateDyingGor();
        StateMonkeyAttack stateMonkeyAttack = new StateMonkeyAttack();
        StateRest stateRest = new StateRest();
        StateRoamingA stateRoamingA = new StateRoamingA();
        StateRoamingB stateRoamingB = new StateRoamingB();
        StateRockAttack stateRockAttack = new StateRockAttack();
        
        //add all states to FSM
        stateMachine.PossibleStates.Add("Beserk", stateBeserk);
        stateMachine.PossibleStates.Add("ClimbingDown", stateClimbingDown);
        stateMachine.PossibleStates.Add("ClimbingUp", stateClimbingUp);
        stateMachine.PossibleStates.Add("Dying", stateDying);
        stateMachine.PossibleStates.Add("MonkeyAttack", stateMonkeyAttack);
        stateMachine.PossibleStates.Add("Rest", stateRest);
        stateMachine.PossibleStates.Add("RoamingA", stateRoamingA);
        stateMachine.PossibleStates.Add("RoamingB", stateRoamingB);
        stateMachine.PossibleStates.Add("RockAttack", stateRockAttack);

        stateMachine.CurrentState = stateMachine.PossibleStates["Rest"];
        stateMachine.GlobalState = new StateGlobalGor();
    }

    public void ChangePath(string nameNewPath, int _pointSelection)
    {
        pointSelection = _pointSelection; //start off at 1 because parent path will also be found

        currentPath = paths[nameNewPath];
        currentPoint = currentPath.PathPoints[pointSelection];
    }

    // ----------------- VARIATIONS ON DETERMINING NEXT POINT, CAN BE ASSIGNED TO DELEGATE ---------------------

    public void DetermineNextPointIncreasing()
    {
        pointSelection++;

        if (pointSelection >= currentPath.PathPoints.Length)
            pointSelection = 1;

        currentPoint = currentPath.PathPoints[pointSelection];
    }

    public void DetermineNextPointDecreasing()
    {
        pointSelection--;

        if (pointSelection <= 0)
            pointSelection = currentPath.PathPoints.Length;

        currentPoint = currentPath.PathPoints[pointSelection];
    }

    public void DetermineNextPointWithInterval()
    {
        if (pointSelection % 2 == 0)
            pointSelection++;
        else
        {
            var rand = Random.Range(-1, 1);
            pointSelection += (rand < 0 ? 1 : 2);

            if (pointSelection % 2 == 0 && JumpCooldown > 0)
            {
                pointSelection++;
                JumpCooldown -= (rand < 0 ? 1 : 2); ;
            }
        }

        if (pointSelection >= currentPath.PathPoints.Length)
            pointSelection = 1;

        currentPoint = currentPath.PathPoints[pointSelection];
    }

    // ---------------- END METHODES FOR DETERMINING NEXT POINT ---------------------------

    public void Flip()
    {
        IsFacingLeft = !IsFacingLeft;

        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    public void SetMovementSpeed(Vector2 newVel)
    {
        rb.velocity = new Vector2(newVel.x * MoveSpeed, rb.velocity.y);
    }

    //Call to make the boss jump on top of a platform
    public void Jump(Vector2 target)
    {
        if (!IsGrounded)
            return;

        Anim.SetTrigger("Jump");
        Anim.SetBool("Rest", true);
        Anim.SetBool("Roam", false);

        var jumpSpeed = target.y - transform.position.y + 35f;

        rb.velocity += new Vector2(0f, jumpSpeed);

        JumpCooldown = 2;
    }

    //Call to make the boss jump off a platform
    public void JumpOff()
    {
        if (!IsGrounded)
            return;

        Anim.SetTrigger("JumpOff");

        JumpingDown = true;

        var jumpSpeed = transform.position.y + 20f;

        rb.velocity += new Vector2(0f, jumpSpeed);

        Anim.SetBool("Rest", false);
        Anim.SetBool("Roam", true);
    }

    public void StopMovement()
    {
        rb.velocity = Vector2.zero;
    }

    public bool DetermineSideRoom()
    {
        if (transform.position.x < MidPointRoom.position.x)
            return false;

        else
            return true;
    }

    public Vector2 DetermineShootPoint()
    {
        Vector2 determinedPos;

        float randomXOffset = Random.Range(-2, 2);
        determinedPos = new Vector2(Player.transform.position.x + randomXOffset, Player.transform.position.y);

        return determinedPos;
    }

    void Update()
    {
        stateMachine.Update();
    }

    //sets all information for feather particle. should be called before instantiating 
    public void SetParticleInformation(Range amount, Range speed, Range scale, float lifeTime, float fadeTime)
    {
        randomAmount = (int)amount.GetRandomInRange();
        randomDirections = new Vector2[randomAmount];
        randomMovespeeds = new float[randomAmount];
        randomScales = new float[randomAmount];
        lifeTimeBanana = lifeTime;
        fadeTimeBanana = fadeTime;

        for (int i = 0; i < randomAmount; i++)
        {
            randomDirections[i] = new Vector2(Random.Range(-1, 2), Random.Range(-1, 2));
            randomMovespeeds[i] = speed.GetRandomInRange();
            randomScales[i] = scale.GetRandomInRange();
        }
    }

    //instantiates feather particle. called after setting information 
    public void InstantiateParticle()
    {
        for (int i = 0; i < randomAmount; i++)
        {
            GameObject tempBanana = new GameObject();
            tempBanana.transform.position = transform.position;
            tempBanana.transform.localScale *= randomScales[i];
            tempBanana.transform.rotation = Quaternion.identity;
            var bananaScript = tempBanana.AddComponent<FeatherScript>();

            bananaScript.Create(randomDirections[i], randomMovespeeds[i], BananaParticle, lifeTimeBanana, fadeTimeBanana);
        }
    }

    public void TakeDamage(float damage)
    {
        if (Invulnerable)
            return;

        Health -= damage;
		sound.PlayHitSound();

        manager.UpdateBossHpBar((int)Health);

        SetParticleInformation(mySettings.amountParticlesHurt, mySettings.speedParticlesHurt, mySettings.scaleParticlesHurt, mySettings.lifeTimeParticlesHurt, mySettings.fadeTimeParticlesHurt);
        InstantiateParticle();
    }

    //Destroy the boss
    public void Destroy()
    {
        rb.velocity = Vector2.zero;
        sprRender.sprite = null;
        Destroy(gameObject);
    }

    //First call this method before actually destroying the boss. 
    void Die()
    {
        Dead = true;
		sound.PlayGorillaDieSound();
    }

    //Call this method when the player or the boss died. 
    public void ClearBattle()
    {
        LevelManager.inGorBattle = false;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "GorBossMidPoint" && GetFSM.CurrentState == GetFSM.PossibleStates["RoamingA"])
            countMid++;
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (playerHealth.currentHearts > 0)
            {
                playerHealth.TakeDamage(1);

                knockback.KnockBack(this.gameObject);

                Flip();
            }
        }
    }
}
