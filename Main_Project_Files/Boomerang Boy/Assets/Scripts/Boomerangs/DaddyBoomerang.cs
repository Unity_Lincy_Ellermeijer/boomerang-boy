﻿using UnityEngine;
using System.Collections;

public class DaddyBoomerang : BoomerangScript
{
    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start()
    {
        base.Start();
    }

    protected override void Update()
    {
        base.Update();
    }

    public override void Shoot(Vector3 _targetPos)
    {
        base.Shoot(_targetPos);
    }

    protected override void OnTriggerEnter2D(Collider2D other)
    {
        if (isFired)
        {
            if (other.tag == "GorBoss")
            {
                other.GetComponent<GorillaBossController>().TakeDamage(1);
            }
        }

        base.OnTriggerEnter2D(other);
    }
}

