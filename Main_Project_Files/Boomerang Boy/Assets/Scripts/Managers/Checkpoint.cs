﻿using UnityEngine;
using System.Collections;

//This class sets a current activated checkpoint in the levelmanager to respawn the player if he's dead
public class Checkpoint : MonoBehaviour
{
    bool isActivated = false;
    CampfireController myCampfire; //campfire will burn if checkpoint is activated to give the player visual feedback

    void Start()
    {
        myCampfire = GetComponentInChildren<CampfireController>(); //get campfire controller
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.name == "Player" && isActivated == false) //if the player is at this checkpoint and it's not yet activated...
        {
            //...activate this checkpoint
            GetComponent<AudioSource>().Play();
            isActivated = true;
            myCampfire.Activate();
            LevelManager.ChangeCheckpoint(gameObject);
        }
    }

    //Call from levelmanager if a new checkpoint is assigned and this one needs to be deactivated 
    public void Deactivate()
    {
        isActivated = false;
        myCampfire.Deactivate();
    }
}
