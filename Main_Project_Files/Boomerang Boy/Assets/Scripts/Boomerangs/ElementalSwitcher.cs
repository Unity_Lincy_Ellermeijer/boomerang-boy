﻿using UnityEngine;
using System.Collections;

public class ElementalSwitcher : MonoBehaviour
{
    bool isUsedBefore = false;
    float timeLeft = 0.3f;

    void Update()
    {
        if (isUsedBefore)
        {
            timeLeft -= Time.deltaTime;
            if (timeLeft < 0)
            {
                isUsedBefore = false;
                timeLeft = 1.0f;
            }
        }

    }
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.transform.tag == "Boomerang")
        {
            ElementalBoomerang elementalScript = other.GetComponent<ElementalBoomerang>();
            if (elementalScript != null && elementalScript.isFired && !isUsedBefore)
            {
                elementalScript.IsIceElement = !elementalScript.IsIceElement;
                elementalScript.ChangeParticleColor(elementalScript.IsIceElement);
                isUsedBefore = true;
            }
        }
    }
}
