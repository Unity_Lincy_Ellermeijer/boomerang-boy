﻿using UnityEngine;
using System.Collections;

public class FollowPlayerEnemyController : EnemyController
{
    public float distX, distY;


    public Transform currentPoint;
    public GameObject[] path;
    public int pointSelection = 1;

    public float minDistToPoint = 0.7f;
    [HideInInspector]
    public float distToPoint = 0f;

    float cooldown;
    bool canFollow = true;

    public bool playerInRange = false;

    public float minDistxToPlayer;
    public float minDistyToPlayer;
    //[HideInInspector] public float distToPlayer = 0f;

    Collider2D coll;

    public override void Start()
    {
        base.Start();
        currentPoint = path[pointSelection].transform;
        coll = GetComponent<Collider2D>();
    }

    public override void Update()
    {
        base.Update();

        CheckForPlayerInRange();

        if (cooldown > 0)
        {
            cooldown -= Time.deltaTime;
            if (cooldown < 0)
                canFollow = true;
        }

        distToPoint = (transform.position - currentPoint.position).magnitude;

        if (distToPoint < minDistToPoint && !playerInRange)
        {
            Flip();
            DetermineNextPoint();
        }

        if (playerInRange)
        {
            rb.velocity = (new Vector3(player.transform.position.x, transform.position.y, 0f) - transform.position).normalized * moveSpeed;
        }
        else
        {
            rb.velocity = (currentPoint.transform.position - transform.position).normalized * moveSpeed;
        }
    }

    public void DetermineNextPoint()
    {
        pointSelection++;

        if (pointSelection >= path.Length)
        {
            pointSelection = 0;
        }

        currentPoint = path[pointSelection].transform;
    }

    void CheckForPlayerInRange()
    {
        distX = Mathf.Abs(transform.position.x - player.transform.position.x);
        distY = Mathf.Abs((transform.position.y - coll.bounds.size.y) - (player.transform.position.y - player.GetComponent<Collider2D>().bounds.size.y));

        if (distX < 0.05f)
            return;

        if (distX < minDistxToPlayer && distY < minDistyToPlayer)
        {
            playerInRange = true;      
        }
        else
        {
            playerInRange = false;
        }
    }

    public virtual void OnCollisionEnter2D(Collision2D other)
    {
        base.OnCollisionEnter2D(other);

        if (other.gameObject.tag == "Player")
        {
            cooldown = 3f;
            canFollow = false;
        }
    }

}
