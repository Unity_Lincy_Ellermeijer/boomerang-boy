﻿using UnityEngine;
using System.Collections;

public class Pause : MonoBehaviour {

	private bool isPaused;                              //Boolean to check if the game is paused or not
    Canvas pauseMenu;

    void Start()
    {
        pauseMenu = GetComponent<Canvas>();
        pauseMenu.enabled = false;
    }
    
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P) && !(Application.loadedLevelName == "start") && !(Application.loadedLevelName == "Credits"))
        {
            isPaused = togglePause();
        }
    }

    bool togglePause() // if isPaused then freeze time
    {
        if (Time.timeScale == 0f)
        {
            Time.timeScale = 1f;
            pauseMenu.enabled = false;
            return (false);
        }
        else
        {
            Time.timeScale = 0f;
            pauseMenu.enabled = true;
            return (true);
        }
    }
}
