﻿using UnityEngine;
using System.Collections;

public class StalactiteController : ObstacleController
{
    public LayerMask collisionLayer;
    [Range(0.0f, 1.0f)]
    public float spawnChance = 0.5f;
    public float growDuration = 1f;
    public float fallDownSpeed = 10f;
    SpriteRenderer renderer;
    public bool isEnabled;
    public bool startGrow;
    float growStartTime;
    bool isFallingDown;
    Vector3 startPos;
    SoundManager sound;

    //sorry dat ik aan je code zit wouter, ik maak niks kapot <3
    float randomDropChance = 0.1f;
    GameObject healthDrop;

    public bool IsEnabled
    {
        get { return isEnabled; }
        set
        {
            if (!value)
            {
                transform.position = startPos;
            }
            if (value && !isEnabled)
            {
                startGrow = true;
                growStartTime = Time.time;
                transform.localScale = new Vector3(0, 0, 0);
            }
            isEnabled = value;
            renderer.enabled = value;
        }
    }

	public override void Start () 
    {
        base.Start();
        startPos = transform.position;
        renderer = GetComponent<SpriteRenderer>();
        IsEnabled = false;
        GrowCheck();

        healthDrop = Resources.Load<GameObject>("Prefabs/Objects/HeartPickup");
        sound = GameObject.Find("Main Camera").GetComponent<SoundManager>();
    }

    public override void Update ()
    {
        base.Update();
        if (startGrow && !isFallingDown)
        {
            float scale = Mathf.Lerp(0f, 1f, (Time.time - growStartTime) / growDuration);
            transform.localScale = new Vector3(scale, scale, scale);
        }

        if (isFallingDown)
        {
            transform.position = new Vector3(transform.position.x, transform.position.y - fallDownSpeed * Time.deltaTime, transform.position.z);
        }


        if (Physics2D.Raycast(transform.position - new Vector3(0, 0.1f, 0), new Vector3(0f, -1f, 0f), 0.2f, collisionLayer))
        {
            //instantiate a health drop if random value is smaller than 0.1f
            if (Random.value < randomDropChance)
                Instantiate(healthDrop, transform.position, Quaternion.identity);

            sound.PlayHitSound();
            IsEnabled = false;
            isFallingDown = false;
            startGrow = false;
        }
	}

    public void GrowCheck()
    {
        if (!isFallingDown && !startGrow)
        {
            float chance = Random.Range(0.0f, 1.0f);
            if (chance < spawnChance)
            {
                IsEnabled = true;
            }
        }
        
    }

    public void GoFallDown()
    {
        transform.localScale = new Vector3(1f, 1f, 1f);
        isFallingDown = true;
    }

    public void OnTriggerEnter2D(Collider2D other)
    {
        if (damage > 0 && other.gameObject.tag == "Player" && isEnabled)
        {
            if (playerHealth.currentHearts > 0)
            {
                playerHealth.TakeDamage(damage);

                knockback.KnockBack(this.gameObject);
                Destroy(this.gameObject);
            }
        }        
    }
}
