﻿using UnityEngine;
using System.Collections;

//This class handles the flames that the bird boss drops in his ice attack, derives from attackobstacle 
public class FlameScript : AttackObstacleScript<BirdBossController>
{
    Vector2 target;
    float lookOffset = 120;

    Animator anim;
    bool positiveDir = false;
    
    //Call to create a flame with a target 
    public override void Create(Vector2 tempTarget, Vector2 mid, BirdBossController agent)
    {
        base.Create(tempTarget, mid, agent);

        thisSprite = Resources.Load<Sprite>("Art/Sprites/Enemies/Boss/Bird/fireparticle");
        particleOffset = new Vector3(0, 1.5f, 0);

        if (tempTarget.x < mid.x)
            positiveDir = false;
        else
            positiveDir = true;

        target = tempTarget;

        anim = gameObject.AddComponent<Animator>();
        AnimatorOverrideController animAOC = Resources.Load<AnimatorOverrideController>("Art/AnimatorControllers/Enemy/Birdboss/FireAttackAOC");
        anim.runtimeAnimatorController = animAOC;

        SpriteRender.sprite = Resources.Load<Sprite>("Art/Sprites/Enemies/Boss/Bird/fireAttack");

        BoxCollider2D coll = gameObject.AddComponent<BoxCollider2D>();
        coll.size = new Vector2(0.6f, 2.8f);
        coll.isTrigger = true;

        MoveSpeed = agent.mySettings.flameSpeed.GetRandomInRange();
    }

    //Make flame do in direction of target and make it face it
    public override void FixedUpdate()
    {
        if (Rb == null)
            return;

        Vector2 distToTarget = new Vector2(target.x, target.y) - new Vector2(transform.position.x, transform.position.y);
        distToTarget.Normalize();
        
        FacePoint();

        Rb.velocity = new Vector2(distToTarget.x * MoveSpeed, Rb.velocity.y);
    }

    void FacePoint()
    {
        var lookPos = new Vector2(transform.position.x, transform.position.y) + new Vector2(Rb.velocity.x, Rb.velocity.y - 25);
        var angle = Mathf.Atan2(lookPos.x, lookPos.y) * Mathf.Rad2Deg;
        transform.eulerAngles = new Vector3(0f, 0f, -angle + lookOffset);
    }
}