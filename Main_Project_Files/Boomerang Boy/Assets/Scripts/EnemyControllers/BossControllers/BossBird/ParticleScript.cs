﻿using UnityEngine;
using System.Collections;

//This class handles particles from objects that bosses throw/drop
public class ParticleScript : MonoBehaviour {

    Rigidbody2D rb;

    //Call this method to create a particle with a sprite
    public void Create(Sprite sprite)
    {
        rb = gameObject.AddComponent<Rigidbody2D>();
        rb.gravityScale = 0;

        var dir = Random.Range(-1, 2); 
        var speed = Random.Range(3, 6);
        rb.velocity = new Vector2(dir * speed, speed);

        SpriteRenderer sprRender = gameObject.AddComponent<SpriteRenderer>();
        sprRender.sprite = sprite;
        sprRender.sortingLayerName = "Particles";

        var deathScript = gameObject.AddComponent<DestoryAfterLifeTime>();
        deathScript.lifeTime = 0.5f;

        var fadeScript = gameObject.AddComponent<FadeOutBeforeDestory>();
        fadeScript.waitForFadeTime = 0.1f;
    }

    void Update()
    {
        FacePoint();
    }

    void FacePoint()
    {
        var angle = Mathf.Atan2(rb.velocity.x, rb.velocity.y) * Mathf.Rad2Deg;
        transform.eulerAngles = new Vector3(0f, 0f, -angle);
    }
}
