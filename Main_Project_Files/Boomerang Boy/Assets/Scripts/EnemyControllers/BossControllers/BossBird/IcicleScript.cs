﻿using UnityEngine;
using System.Collections;

//This class handles the icicles that the bird boss drops in his ice attack, derives from attackobstacle 
public class IcicleScript : AttackObstacleScript<BirdBossController>
{
    //Call to create an icicle
    public override void Create(BirdBossController agent) 
    {
        base.Create(agent);

        thisSprite = Resources.Load<Sprite>("Art/Sprites/Enemies/Boss/Bird/iceShard");
        particleOffset = new Vector3(0, 1.5f, 0);

        SpriteRender.sprite = Resources.Load<Sprite>("Art/Sprites/Enemies/Boss/Bird/iceAttack");
        
        BoxCollider2D coll = gameObject.AddComponent<BoxCollider2D>();
        coll.size = new Vector2(0.6f, 2.8f);
        coll.isTrigger = true;

        MoveSpeed = agent.mySettings.icicleSpeed.GetRandomInRange();
	}

    //Make icicle fall down
    public override void FixedUpdate()
    {
        if (Rb == null)
            return;

        Rb.velocity = new Vector2(0f, -MoveSpeed);
	}
}
