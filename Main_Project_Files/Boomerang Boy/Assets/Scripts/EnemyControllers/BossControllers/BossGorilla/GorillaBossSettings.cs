﻿using UnityEngine;
using System;

//The Gorilla boss owns an instance of this class
//This way all the variables that need to be tweaked
//can be tweaked in the inspector
[Serializable]
public class GorillaBossSettings
{
    [Header("Health Settings")]
    [SerializeField]
    public float startHealth = 5f;
    public float healthAmountStartBeserk = 1f;

    [Header("Movement Settings")]
    [SerializeField]
    public float movementSpeed = 15f;
    [SerializeField]
    public float jumpSpeed = 10f;

    [Header("Check Variables")]
    [SerializeField]
    public Range dividerCountMid = new Range(4, 6);
    [SerializeField]
    public Range cooldownThrowAttack = new Range(0.5f, 1f);
    [SerializeField]
    public Range stayInMassAttckTime = new Range(5, 9);
    [SerializeField]
    public int maxExhausted = 30;
    [SerializeField]
    public float restTime = 1.2f;
    [SerializeField]
    public float dieTime = 2f;

    [Header("Attack Settings")]
    [SerializeField]
    public Range throwSpeed = new Range(20, 40);

    [Header("Hurt Particle Settings")]
    [SerializeField]
    public Range amountParticlesHurt = new Range(4, 6);
    [SerializeField]
    public Range speedParticlesHurt = new Range(2, 3);
    [SerializeField]
    public Range scaleParticlesHurt = new Range(0.4f, 1.2f);
    [SerializeField]
    public float lifeTimeParticlesHurt = 1.5f, fadeTimeParticlesHurt = 0.8f;

    [Header("Dead Particle Settings")]
    [SerializeField]
    public Range amountParticlesDead = new Range(20, 25);
    [SerializeField]
    public Range speedParticlesDead = new Range(1, 3);
    [SerializeField]
    public Range scaleParticlesDead = new Range(0.7f, 1);
    [SerializeField]
    public float lifeTimeParticlesDead = 10f, fadeTimeParticlesDead = 5f;
}
