﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//This class is the controller of the bird boss, which controls the states, paths ect.
public class BirdBossController : MonoBehaviour 
{
	SoundManager sound;

    public BirdBossSettings mySettings;
    BirdBossManager manager;

    //statemachine ref and property
    StateMachine<BirdBossController> stateMachine;
    public StateMachine<BirdBossController> GetFSM { get { return stateMachine; } set { stateMachine = value; } }
    
    //path variables
    Dictionary<string, PathScript> paths = new Dictionary<string, PathScript>(); //ref to all paths bird can follow
    PathScript currentPath; //ref to current path to follow, will be passed to global state to make bird follow path
    Transform currentPoint; //ref to current point
    public Transform GetCurrentPoint { get { return currentPoint; } }
    int pointSelection;
    public int GetPointSelection { get { return pointSelection; } }

    //element enumeration
    public enum Element
    {
        fire,
        ice
    };
    Element element = Element.fire;
    public Element MyElement { get { return element; } }

    //health variables 
    GameObject shield;
    public bool Invulnerable { get; set; }
    public bool Dead { get; set; }
    float startHealth;
    float health;
    float Health
    {
        get { return health; }
        set
        {
            health = value;
            if (health <= 0)
            {
                Die();

            }
        }
    }

    //movement variables
    float moveSpeed; 
    public float ClimbScalar { get { return mySettings.climbScalar; } } 
    public float DiveScalar { get { return mySettings.diveScalar; } }
    public float GrowScalar { get { return mySettings.growScalar; } } 
    public bool IsFacingLeft { get; set; }

    Rigidbody2D rb;
    public Vector2 GetVelocity { get { return rb.velocity; } }
    SpriteRenderer sprRender;
    public Animator Anim { get; set; }
    GameObject player;
    PlayerHealth playerHealth;
    Knockback knockback;

    //variables for feather particle system
    public List<Sprite> FireFeathers { get; set; }
    public List<Sprite> IceFeathers { get; set; }
    int randomAmount;
    Vector2[] randomDirections;
    float[] randomMovespeeds;
    Sprite[] randomSprites;
    float[] randomScales;
    float lifeTimeFeather;
    float fadeTimeFeather;

    //checks for dist to current target point
    float distToPoint;
    public float GetDistToPoint { get { return (transform.position - currentPoint.position).magnitude; } }
    float minDistToPoint = 0.3f;
    public float GetMinDistToPoint { get { return minDistToPoint; } }

    //boundary bossroom to check conditions in certain states for example diving
    BoxCollider2D boundary;
    Vector3 min, max;
    float cameraHalfWidth, cameraSize;
    public float GetMinXPos { get { return min.x; } }
    public float GetMinYPos { get { return min.y; } }
    public float GetMaxXPos { get { return max.x; } }
    public float GetMaxYPos { get { return max.y; } }

    //refs to certain points in bossroom
    public Transform MidPointRoom { get; set; }
    public Transform LeftPointUnderRoom { get; set; }
    public Transform RightPointUnderRoom { get; set; }
    public List<Transform> AttackPoints { get; set; }

    //variables that determines change of state
    int countMid = 0; //if coundMid % x == 0 > do dive attack
    public int GetCountMid { get { return countMid; } }
    public float StartMassAttackTimer { get { return mySettings.startMassAttackTimer - (1 * 15 - health); } }
    public float MassAttackTimer { get; set; }
    public float DecreaseAmountTimer { get; set; } //decrease timer if boss is hit 

    void Start ()
    {
		sound = GameObject.Find("Main Camera").GetComponent<SoundManager>();
        manager = GameObject.FindGameObjectWithTag("BirdBossManager").GetComponent<BirdBossManager>();

        startHealth = mySettings.startHealth;
        moveSpeed = mySettings.movementSpeed;

        MidPointRoom = GameObject.FindGameObjectWithTag("BirdBossMidPoint").transform;
        LeftPointUnderRoom = GameObject.FindGameObjectWithTag("BirdBossLUnderPoint").transform;
        RightPointUnderRoom = GameObject.FindGameObjectWithTag("BirdBossRUnderPoint").transform;

        var tempAttackPoints = GameObject.FindGameObjectsWithTag("BirdBossAttackPoint");
        AttackPoints = new List<Transform>();
        foreach (GameObject p in tempAttackPoints)
        {
            AttackPoints.Add(p.transform);
        }

        //get all possible paths
        var tempPaths = GameObject.FindGameObjectsWithTag("BirdBossPath");
        foreach (GameObject p in tempPaths)
        {
            paths.Add(p.name, p.GetComponent<PathScript>());
        }

        ChangePath("FlyingPath", 1);

        health = startHealth;

        IsFacingLeft = true;

        rb = GetComponent<Rigidbody2D>();
        sprRender = GetComponent<SpriteRenderer>();
        Anim = GetComponent<Animator>();

        player = GameObject.FindGameObjectWithTag("Player");
        playerHealth = player.GetComponent<PlayerHealth>();
        knockback = player.GetComponent<Knockback>();

        boundary = GameObject.FindGameObjectWithTag("BirdBossBoundary").GetComponent<BoxCollider2D>();
        min = boundary.bounds.min;
        max = boundary.bounds.max;
        cameraHalfWidth = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().orthographicSize * ((float)Screen.width / Screen.height);
        cameraSize = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>().orthographicSize;

        MassAttackTimer = Time.fixedTime + StartMassAttackTimer;

        FireFeathers = new List<Sprite>();
        IceFeathers = new List<Sprite>();

        for (int i = 1; i < 7; i++ )
        {
            FireFeathers.Add(Resources.Load<Sprite>("Art/Sprites/Enemies/Boss/Bird/Feathers/FireF" + i));
            IceFeathers.Add(Resources.Load<Sprite>("Art/Sprites/Enemies/Boss/Bird/Feathers/IceF" + i));
        }

        SetUpStateMachine();
    }

    void SetUpStateMachine()
    {
        stateMachine = new StateMachine<BirdBossController>(this);

        StateFlying stateFly = new StateFlying();
        StateClimbing stateClimb = new StateClimbing();
        StateDiving stateDive = new StateDiving();
        StateCentering stateCentering = new StateCentering();
        StateMassiveIceAttack stateMassIceAttack = new StateMassiveIceAttack();
        StateMassiveFireAttack stateMassFireAttack = new StateMassiveFireAttack();
        StateDyingBird stateDie = new StateDyingBird();

        stateMachine.PossibleStates.Add("Flying", stateFly);
        stateMachine.PossibleStates.Add("Climbing", stateClimb);
        stateMachine.PossibleStates.Add("Diving", stateDive);
        stateMachine.PossibleStates.Add("Centering", stateCentering);
        stateMachine.PossibleStates.Add("MassIceAttack", stateMassIceAttack);
        stateMachine.PossibleStates.Add("MassFireAttack", stateMassFireAttack);
        stateMachine.PossibleStates.Add("Dying", stateDie);

        stateMachine.CurrentState = stateMachine.PossibleStates["Flying"];
        stateMachine.GlobalState = new StateGlobalBird();
    }

    public void ChangePath(string nameNewPath, int pointSelect)
    {
        pointSelection = pointSelect; //start off at 1 because parent path will also be found

        currentPath = paths[nameNewPath];
        currentPoint = currentPath.PathPoints[pointSelection];
    }

    public void DetermineNextPoint()
    {
        pointSelection++;

        if (pointSelection >= currentPath.PathPoints.Length)
            pointSelection = 1;

        currentPoint = currentPath.PathPoints[pointSelection];
    }

    public void Flip()
    {
        IsFacingLeft = !IsFacingLeft;

        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    public void SetMovementSpeed(Vector2 newVel)
    {
        rb.velocity = newVel * moveSpeed;
    }

    public void ResetRot()
    {
        transform.rotation = Quaternion.identity;
    }

    public bool DetermineSideRoom()
    {
        if(transform.position.x < MidPointRoom.position.x)
            return false;

        else 
            return true;
    }

	void Update ()
    {
        stateMachine.Update();
	}

    public void InstatiateShield()
    {
        if (shield != null)
            return;

        shield = new GameObject();
        shield.transform.position = transform.position + new Vector3(0, 1f, 0);
        shield.transform.rotation = Quaternion.identity;
        var shieldScript = shield.AddComponent<ShieldScript>();

        shieldScript.Create((int)MyElement);
    }

    public void DestroyShield()
    {
        GameObject.Destroy(shield);
    }

    //sets all information for feather particle. should be called before instantiating 
    public void SetFeatherParticleInformation(Range amount, Range speed, Range scale, float lifeTime, float fadeTime)
    {
        randomAmount = (int)amount.GetRandomInRange();
        randomDirections = new Vector2[randomAmount];
        randomMovespeeds = new float[randomAmount];
        randomSprites = new Sprite[randomAmount];
        randomScales = new float[randomAmount];
        lifeTimeFeather = lifeTime;
        fadeTimeFeather = fadeTime;

        for (int i = 0; i < randomAmount; i++)
        {
            randomDirections[i] = new Vector2(Random.Range(-1, 2), Random.Range(-1, 2));
            randomMovespeeds[i] = speed.GetRandomInRange();
            randomScales[i] = scale.GetRandomInRange();

            if (MyElement == BirdBossController.Element.ice)
                randomSprites[i] = FireFeathers[Random.Range(0, FireFeathers.Count)];
            else if (MyElement == BirdBossController.Element.fire)
                randomSprites[i] = IceFeathers[Random.Range(0, FireFeathers.Count)];
        }
    }

    //instantiates feather particle. called after setting information 
    public void InstantiateDeadParticle()
    {
        for(int i = 0; i < randomAmount; i++)
        {
            GameObject tempFeather = new GameObject();
            tempFeather.transform.position = transform.position;
            tempFeather.transform.localScale *= randomScales[i];
            tempFeather.transform.rotation = Quaternion.identity;
            var featherScript = tempFeather.AddComponent<FeatherScript>();

            featherScript.Create(randomDirections[i], randomMovespeeds[i], randomSprites[i], lifeTimeFeather, fadeTimeFeather);
        }
    }

    //called by object that deals damage
    public void TakeDamage(float damage, int element)
    {
        if (((int)this.element == element) || Invulnerable)
            return;

        if (GetFSM.CurrentState == GetFSM.PossibleStates["Flying"])
            MassAttackTimer -= DecreaseAmountTimer;

        SetFeatherParticleInformation(mySettings.amountFeatherHurt, mySettings.speedFeatherHurt, mySettings.scaleFeatherHurt, mySettings.lifeTimeFeatherHurt, mySettings.fadeTimeFeatherHurt);
        InstantiateDeadParticle();

		sound.PlayHitSound ();
        Health -= damage;
        Anim.SetTrigger("Hit");

        manager.UpdateBossHpBar((int)Health);

        if (Health != 0)
            ChangeElement();
    }

    //called when hit
    void ChangeElement()
    {
        if (MyElement == Element.fire)
            element = Element.ice;
        else
            element = Element.fire;
    }

    //called if hp <= 0
    void Die()
    {
        Dead = true;
		sound.PlayBirdDieSound();
    }

    //called if everything that has to be done to die is done
    public void Destroy()
    {
        rb.velocity = Vector2.zero;
        sprRender.sprite = null;
        Destroy(gameObject);
    }

    //clears the battle when boss or player dies
    public void ClearBattle()
    {
        LevelManager.inBirdBattle = false;

        GameObject[] birds = GameObject.FindGameObjectsWithTag("MinionBird");
        for (int i = 0; i < birds.Length; i++)
        {
            GameObject.Destroy(birds[i]);
        }

        GameObject shield = GameObject.FindGameObjectWithTag("BirdShield");
        if (shield != null)
            GameObject.Destroy(shield);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "BirdBossMidPoint" && GetFSM.CurrentState == GetFSM.PossibleStates["Flying"])
            countMid++;

        if (other.tag == "Player")
        {
            if (playerHealth.currentHearts > 0)
            {
                playerHealth.TakeDamage(1);

                knockback.KnockBack(this.gameObject);

                Flip();
            }
        }
    }
}
