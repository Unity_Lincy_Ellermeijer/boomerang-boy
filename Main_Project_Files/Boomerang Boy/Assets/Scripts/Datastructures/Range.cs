﻿using System;
using UnityEngine;

//This class is mostly used by bosses to give a random factor to certain attacks 
[Serializable]
public class Range
{
    //min and max values of the total range 
    [SerializeField]
    public float min;
    [SerializeField]
    public float max;

    //constructor to assign min and max values
    public Range(float _min, float _max)
    {
        min = _min;
        max = _max;
    }

    //methode to get a random value from the total range, mostly called in start methode of a state to assign new random values for an attack
    public float GetRandomInRange()
    {
        return UnityEngine.Random.Range(min, max);
    }
}
