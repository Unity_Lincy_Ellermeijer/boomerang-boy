﻿using UnityEngine;
using System.Collections;


//haha gasscontroller *pffkstgtstt*
public class GassController : ObstacleController 
{
    public float spinSpeed = 100f;
    public float growSpeed = 1f;
    public float pulsateInterval = 1f;
    float pulseTimer;
    int growDirection = -1;
    int spinDirection;
	public override void Start () //OVERRIDE HURDUR
    {
        base.Start();
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, Random.Range(0, 360));
        transform.localScale = new Vector3(transform.localScale.x * Random.Range(0.8f, 1f), transform.localScale.y * Random.Range(0.8f, 1f), transform.localScale.z);
        spinDirection = Random.Range(0, 2);
        if (spinDirection == 0)
        {
            spinDirection = -1;
        }
	}


	public override void Update () 
    {
        base.Update();
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, transform.localEulerAngles.z + spinDirection * spinSpeed * Time.deltaTime);

        transform.localScale = new Vector3(transform.localScale.x + growDirection * growSpeed * Time.deltaTime, transform.localScale.y + growDirection * growSpeed * Time.deltaTime, transform.localScale.z);

        pulseTimer += Time.deltaTime;
        if (pulseTimer > pulsateInterval)
        {
            pulseTimer -= pulsateInterval;
            growDirection = -growDirection;
        }


	}

    public override void OnCollisionEnter2D(Collision2D other)
    {
        base.OnCollisionEnter2D(other);
    }
}
