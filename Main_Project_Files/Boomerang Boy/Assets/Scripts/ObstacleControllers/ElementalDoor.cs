﻿using UnityEngine;
using System.Collections;

public class ElementalDoor : MonoBehaviour {
	
	int currentHp;
	Renderer renderer;
	bool canBeDestroyed;
	public bool isFire;



	// Use this for initialization
	void Start () 
	{
		renderer = GetComponent<Renderer>();
		canBeDestroyed = true;
	}
	
	void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.layer == LayerMask.NameToLayer("Boomerang"))
		{
			BoomerangScript boomerang = other.GetComponent<BoomerangScript>();
			ElementalBoomerang elementalBoomerang = other.GetComponent<ElementalBoomerang>();
			if (boomerang.isFired && canBeDestroyed && !elementalBoomerang.IsIceElement && !isFire)
			{
				Destroy(this.gameObject);
			}
			if (boomerang.isFired && canBeDestroyed && elementalBoomerang.IsIceElement && isFire)
			{
				Destroy(this.gameObject);
			}
		}

	}
}