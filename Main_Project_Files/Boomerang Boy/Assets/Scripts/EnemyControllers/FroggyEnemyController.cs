﻿using UnityEngine;
using System.Collections;

public class FroggyEnemyController : EnemyController
{
	public AudioClip tongueSound;
	AudioSource audio;
    FroggyTongueController tongue;
    SpriteRenderer spriteRenderer;
    float animationTimer;
    State currentState;
    float baseWidth;
    bool tongueIsOut;
    float attackTimer;
    public float attackDistance = 10f;
    public float attackInterval = 2f;
    public float tongueSpeed = 0.1f;
    public float jumpInterval = 1f;
    public float jumpPower = 1000f;
    bool hasJumped;
    float jumpTimer;
    enum State
    {
        Idle,
        Ribbit,
        Attack
    }

	public override void Start ()
    {
        base.Start();
        tongue = transform.FindChild("Tongue").GetComponent<FroggyTongueController>();
        tongue.tongueSpeed = tongueSpeed;
        baseWidth = transform.localScale.x;
        spriteRenderer = GetComponent<SpriteRenderer>();
        SwitchState(State.Idle);
	}
	
	void Update () 
    {
        if (!tongueIsOut)
        {
            if (player.transform.position.x > transform.position.x)
            {
                transform.localScale = new Vector3(baseWidth, transform.localScale.y, transform.localScale.z);
                tongue.direction = -1;
            }
            else
            {
                transform.localScale = new Vector3(-baseWidth, transform.localScale.y, transform.localScale.z);
                tongue.direction = 1;
            }
        }

        animationTimer += Time.deltaTime;
        switch (currentState)
        {
            case State.Idle: case State.Ribbit:
                //lelijke code!
                GameObject boomerang = player.GetComponent<Boomerang>().boomerang;
                if (boomerang != null)
                {
                    BoomerangScript activeBoomerang = boomerang.GetComponent<BoomerangScript>();
                    if (activeBoomerang != null)
                    {
                        float distanceToBoomerang = Vector2.Distance(activeBoomerang.transform.position, transform.position);
                        if (activeBoomerang.transform.position.y > transform.position.y - 1 &&
                            activeBoomerang.transform.position.y < transform.position.y + 1 &&
                            distanceToBoomerang < 5.5f && !hasJumped && activeBoomerang.isFired)
                        {
                            SwitchState(State.Idle);
                            rb.AddForce(new Vector2(0, 1) * jumpPower);
                            hasJumped = true;
                        }
                    }

                    if (hasJumped)
                    {
                        jumpTimer += Time.deltaTime;
                        if (jumpTimer > jumpInterval)
                        {
                            jumpTimer = 0;
                            hasJumped = false;
                        }

                    }
                    
                }
                if (rb.velocity.magnitude < 0.5f)
                {
                    attackTimer += Time.deltaTime;
                    if (attackTimer > attackInterval)
                    {
                        float distanceToPlayer = Mathf.Abs(player.transform.position.x - transform.position.x);
                        if (player.transform.position.y > transform.position.y - 1 &&
                            player.transform.position.y < transform.position.y + 1 &&
                            distanceToPlayer < attackDistance)
                        {
                            attackTimer = 0;
                            SwitchState(State.Attack);
							GetComponent<AudioSource>().PlayOneShot(tongueSound, 1F);
                        }
                    }
                }
                
                break;
            case State.Attack:
				
                if (animationTimer > 0.43f)
                {
                    if (!tongueIsOut)
                    {
						
                        tongue.SetTongue(true);
                        tongueIsOut = true;
						
						
                    }
                    else
                    {
                        if (tongue.isDone)
                        {
                            SwitchState(State.Idle);
                        }
                    }
                }
                
                break;
            default:
                break;
        }
	}

    void SwitchState(State newState)
    {
        animationTimer = 0;
        tongueIsOut = false;
        anim.SetBool("DoIdle", false);
        anim.SetBool("DoRibbit", false);
        anim.SetBool("DoAttack", false);
        tongue.SetTongue(false);
        currentState = newState;
        switch (currentState)
        {
            case State.Idle:
                anim.SetBool("DoIdle", true);
                break;
            case State.Ribbit:
                anim.SetBool("DoRibbit", true);
                break;
            case State.Attack:
                anim.SetBool("DoAttack", true);
                break;
            default:
                break;
        }
    }

    public override void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Enemy")
        {
            Physics2D.IgnoreCollision(other.collider, collider);
        } 
        if (other.gameObject.tag == "Player")
        {
            if (playerHealth.currentHearts > 0)
            {
                playerHealth.TakeDamage(damage);
                knockback.KnockBack(this.gameObject);
            }
        }
    }
}
