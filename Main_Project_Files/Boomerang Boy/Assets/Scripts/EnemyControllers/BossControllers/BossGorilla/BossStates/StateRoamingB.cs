﻿using UnityEngine;
using System.Collections;

//This state makes the boss roam on the ground and jump every now and than on a platform 
public class StateRoamingB : State<GorillaBossController>
{
    float movementScalar;
    float waitTime; //time to wait after a jump to make landing look more realistic 

    public override void Enter(GorillaBossController agent)
    {
        agent.Anim.SetBool("Roam", true);

        agent.ChangePath("GroundPlatformPath", 1);

        agent.ThisDetermineNextPoint = agent.DetermineNextPointWithInterval; //set delegate equal to correct methode to determine a next point in the path
    }

    public override void Execute(GorillaBossController agent)
    {
        //if health is low enough to go to beserk
        //or boss is exhausted
        //and he is not on jumpcooldown anymore
        if ((agent.Health < agent.mySettings.healthAmountStartBeserk || agent.Exhausted > agent.mySettings.maxExhausted) && agent.JumpCooldown == 0)
            agent.GetFSM.ChangeState(agent.GetFSM.PossibleStates["Rest"]);

        MoveToPoint(agent);

        agent.Exhausted += Time.fixedDeltaTime;
    }

    public override void Exit(GorillaBossController agent)
    {
        agent.Anim.SetBool("Roam", false);
    }

    //This method contains all movement logic of this state
    //Mostly requests, delegates to the boss class
    void MoveToPoint(GorillaBossController agent)
    {
        //if we need to calculate the next point
        if (agent.GetDistToPoint < agent.GetMinDistToPoint)
        {
            if(agent.GetPointSelection % 2 == 0) //if this is true, we are on top of a platform
                waitTime = Time.time + 0.5f;

            agent.ThisDetermineNextPoint();

            if (agent.GetCurrentPoint.transform.position.y > agent.transform.position.y) //if our target is above us
            {
                agent.Jump(agent.GetCurrentPoint.transform.position);
            }
        }

        //if we're at the edge of a platform 
        if (agent.AtEdge && !agent.JumpingDown && agent.JumpCooldown>0)
            agent.JumpOff();

        //adjusting the movementscalar based on current action
        if (waitTime > Time.time)
            movementScalar = 0;
        else if (agent.IsGrounded || agent.GetCurrentPoint.transform.position.y < agent.transform.position.y)
            movementScalar = 1;
        else if (agent.AtEdge)
            movementScalar = 0.5f;
        else
            movementScalar = 0.7f;

        agent.SetMovementSpeed((agent.GetCurrentPoint.transform.position - agent.transform.position).normalized * movementScalar);
    }
}