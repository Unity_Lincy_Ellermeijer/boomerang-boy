﻿using UnityEngine;
using System.Collections;

public class LeafController : ObstacleController
{
    public LeafSpawner leafSpawner;
    SpriteRenderer renderer;
    public bool goFallDownImmediately;

    public float endScale;
    public float duration;
    public float startScale;
    public int shakeWarningDuration;
    public float fallSpeed;


    public float fallDuration;
    float fallTimer;
    bool isActivated = false;
    float currentGrowPerc;
    int currentShake = 0;
    float beginTime;

    bool goShake;
    bool goFallDown;
    int direction = 1;
    float shakeTimer;
    float shakeSpeed = 20f;
    void Awake()
    {
        renderer = GetComponent<SpriteRenderer>();
        startScale = transform.localScale.x;
        beginTime = Time.time;
    }

	public override void Start ()
    {
        base.Start();
	}
	
	void Update ()
    {
        currentGrowPerc = (Time.time - beginTime) / duration;
        float newScale = Mathf.Lerp(startScale, endScale, currentGrowPerc);
        transform.localScale = new Vector3(newScale, leafSpawner.imageFlip*newScale, transform.localScale.z);

        if (goFallDownImmediately)
        {
            if (currentGrowPerc > 0.9f && !goShake && !isActivated)
            {
                StartShake();
            }
        }

        if (goShake && isActivated)
        {
            if (currentShake < shakeWarningDuration)
            {
                transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z + direction * shakeSpeed * Time.deltaTime);
                shakeTimer += Time.deltaTime;
                if (shakeTimer > 0.2f)
                {
                    direction = -direction;
                    shakeTimer = 0;
                    currentShake++;
                }
            }
            else
            {
                goShake = false;
                goFallDown = true;
            }
        }
        if (goFallDown)
        {
            fallTimer += Time.deltaTime;

            transform.position = new Vector3(transform.position.x, transform.position.y - fallSpeed * Time.deltaTime, transform.position.z);

            if (fallTimer > fallDuration)
            {
                Color color = renderer.color;
                color = new Color(color.r, color.g, color.b, color.a - 1f * Time.deltaTime);
                renderer.color = color;
                if (color.a <= 0.1f)
                {
                    Destroy(this.gameObject);

                    if (!goFallDownImmediately)
                    {
                        leafSpawner.GoCount();
                    }                   
                }
            }
            
        }
	}

    public void OnCollisionStay2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (currentGrowPerc > 0.9f && !goShake && !isActivated && other.gameObject.transform.position.y > transform.position.y)
            {
                StartShake();
            }
        }
    }

    public void StartShake()
    {
        direction = 1;
        goShake = true;
        isActivated = true;
    }
}
