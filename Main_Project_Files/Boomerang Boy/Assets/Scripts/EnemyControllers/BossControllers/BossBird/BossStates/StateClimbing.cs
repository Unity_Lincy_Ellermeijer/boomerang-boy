﻿using UnityEngine;
using System.Collections;

//This state makes the bird climb up out of the screen
//It's mostly used as visual feedback to the player to let him know some attack started or finished
public class StateClimbing : State<BirdBossController>
{
    float lookOffset = -90f;
    int dir = 1; //1 = from right; -1 = from left;

    public override void Enter(BirdBossController agent)
    {
        //is bird left from mid? go out of the screen at upper right
        if (agent.transform.position.x < agent.MidPointRoom.position.x)
        {
            agent.ChangePath("RightStartDivingPath", 1);
            dir = -1;
        }
        else
        {
            agent.ChangePath("LeftStartDivingPath", 1);
            dir = 1;
        }

        agent.Invulnerable = true; //cannot be hurt while in this state
    }

    public override void Execute(BirdBossController agent)
    {
        if ((agent.GetMaxYPos > agent.transform.position.y)) //is bird not yet above max y position? than make him go there
        {
            FacePoint(agent);
            MoveToPoint(agent, agent.ClimbScalar);
        }

        else //if bird boss is above max y position, change state
        {
            if ((agent.GetFSM.PreviousState == agent.GetFSM.PossibleStates["MassIceAttack"]) || (agent.GetFSM.PreviousState == agent.GetFSM.PossibleStates["MassFireAttack"]))
                agent.GetFSM.ChangeState(agent.GetFSM.PossibleStates["Flying"]);
            else
            {
                agent.GetFSM.ChangeState(agent.GetFSM.PossibleStates["Diving"]);
            }
        }

    }

    public override void Exit(BirdBossController agent)
    {
        agent.ResetRot(); //bird was rotated in this state, set it back
        agent.Flip(); 
        agent.Invulnerable = false; //enable damaging
    }

    void FacePoint(BirdBossController agent)
    {
        var lookPos = agent.GetCurrentPoint.transform.position - agent.transform.position;
        var angle = Mathf.Atan2(lookPos.x, lookPos.y) * Mathf.Rad2Deg;
        agent.transform.eulerAngles = new Vector3(0f, 0f, -angle + lookOffset * dir);
    }

    void MoveToPoint(BirdBossController agent, float scalar)
    {
        if (agent.GetDistToPoint < agent.GetMinDistToPoint)
            agent.DetermineNextPoint();

        agent.SetMovementSpeed(((agent.GetCurrentPoint.transform.position - agent.transform.position).normalized) * scalar);
    }
}
