﻿using UnityEngine;
using System.Collections;

public class FroggyTongueController : MonoBehaviour
{
    GameObject player;
    GameObject endTongue;
    GameObject middleTongue;
    public int direction;
    float length = 1f;
    float targetDistance = 1f;
    bool isActivated = false;
    bool goingBack = false;
    public bool isDone = false;
    public float tongueSpeed = 0.1f;
    void Awake()
    {
        player = GameObject.Find("Player");
        endTongue = transform.FindChild("FrogTongueEnd").gameObject;
        middleTongue = transform.FindChild("FrogTongueMiddle").gameObject;
        SetTongue(false);
    }

    public void SetTongue(bool value)
    {
        isActivated = value;
        goingBack = false;
        length = 1f;
        endTongue.GetComponent<SpriteRenderer>().enabled = value;
        middleTongue.GetComponent<SpriteRenderer>().enabled = value;
        if (value)
        {
            isDone = false;
            targetDistance = Mathf.Abs(player.transform.position.x - transform.position.x) * 21f + 10f;
        }
    }

	void Start ()
    {
        

	}
	
	void Update ()
    {
        if (!isActivated)
        {
            return;
        }

        transform.localScale = new Vector3(direction, transform.localScale.y, transform.localScale.z);
        middleTongue.transform.localScale = new Vector3(length * direction, middleTongue.transform.localScale.y, middleTongue.transform.localScale.z);
        middleTongue.transform.localPosition = new Vector3(direction * 0.23f, middleTongue.transform.localPosition.y, middleTongue.transform.localPosition.z);
        endTongue.transform.localPosition = new Vector3((0.23f + length * 0.045f) * direction, middleTongue.transform.localPosition.y, middleTongue.transform.localPosition.z);

        
        if (goingBack)
        {
            length -= 8f;
            if (length < 3f)
            {
                isDone = true;
            }
        }
        else
        {
            length += (targetDistance - length) * tongueSpeed;
            if (length > targetDistance * 0.95f)
            {
                goingBack = true;
            }
        }
	}
}
