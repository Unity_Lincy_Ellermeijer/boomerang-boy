﻿using UnityEngine;
using System.Collections;

public class SoundManager : MonoBehaviour
{
	public AudioClip hitSound;
	public AudioClip dieSound;
	public AudioClip birdDieSound;
	public AudioClip GolemDieSound;
	public AudioClip GorillaDieSound;
    public AudioClip victorySound;

	AudioSource source;
	void Start ()
	{
		source = GetComponent<AudioSource> ();
	}

	void Update ()
	{
	
	}

	public void PlayHitSound()
	{
        if (!source.isPlaying)
		    source.PlayOneShot (hitSound);
	}

	public void PlayDieSound()
	{
		source.PlayOneShot (dieSound);
	}

	public void PlayBirdDieSound()
	{
		source.PlayOneShot (birdDieSound);
	}

	public void PlayGolemDieSound()
	{
		source.PlayOneShot (GolemDieSound);
	}

	public void PlayGorillaDieSound()
	{
		source.PlayOneShot (GorillaDieSound);
	}

    public void PlayVictorySound()
    {
        source.PlayOneShot(victorySound);
    }
}
