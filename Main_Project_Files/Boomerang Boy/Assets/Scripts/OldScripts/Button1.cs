﻿using UnityEngine;
using System.Collections;

public class Button1 : MonoBehaviour 
{
    public bool triggered = false;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            triggered = true;
        }
    }
}
