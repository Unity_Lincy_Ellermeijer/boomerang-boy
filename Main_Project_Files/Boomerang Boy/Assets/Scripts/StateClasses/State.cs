﻿using UnityEngine;
using System.Collections;

//Generic state class 
public class State <T>
{
    //called once, if state is entered
    public virtual void Enter(T agent) { }

    //called every frame, contains behavior and transitions
    public virtual void Execute(T agent) { }

    //called once, if state is exited
    public virtual void Exit(T agent) { }
}
