﻿using UnityEngine;
using System.Collections;

public class Walk : PlayerController {

	public float maxSpeed;
    public float accel;
    public float smoothing;
	Animator animator;



	// Update is called once per frame
	void FixedUpdate () {
		animator = GetComponent<Animator>();
        float horizontal = Input.GetAxis("Horizontal");
        

        if (horizontal < 0)
        {
            if (rb.velocity.x > -maxSpeed)
            {
                rb.AddForce(new Vector2(-accel, 0.0f));
            }
            else
            {
                rb.velocity = new Vector2(-maxSpeed, rb.velocity.y);
            }
        }
        else if (horizontal > 0)
        {
            if (rb.velocity.x < maxSpeed)
            {
                rb.AddForce(new Vector2(accel, 0.0f));
            }
            else
            {
                rb.velocity = new Vector2(maxSpeed, rb.velocity.y);
            }
        }
        else
        {
            rb.velocity = new Vector2(rb.velocity.x * smoothing, rb.velocity.y); //smooth if no input
        }

        if (rb.velocity.x > 0.05f && !isFacingRight)
        {
            Flip();
        }
        else if (rb.velocity.x < -0.05f && isFacingRight)
        {
            Flip();
        }

		animator.SetFloat("Speed",horizontal);
           
    }
    public void Flip()
    {
        isFacingRight = !isFacingRight;
        
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

}
