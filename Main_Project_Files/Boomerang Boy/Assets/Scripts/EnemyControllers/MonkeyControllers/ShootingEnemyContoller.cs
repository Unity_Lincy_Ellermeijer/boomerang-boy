﻿using UnityEngine;
using System.Collections;

//This class has the ability to shoot at the player if he's close 
public class ShootingEnemyContoller : WallEdgeEnemyContoller
{
    public GameObject projectile; //ref to projectile to shoot
    const float PROJECTILE_SPEED = 4f;

    //variables to check for total time of an attack
    const float START_ATTACK_TIME = 0.5f;
    float attackTime;
    bool isAttacking;

    //variables to check for cooldown till next attack
    const float START_COOLDOWN = 3f;
    float coolDown;
    bool onCoolDown;

    //variables to check if we need to attack
    public float minDistToPlayer = 5f;
    public float distToPlayer = 0f;
    public bool playerInRange = false;

    public override void Start()
    {
        base.Start();
    }

    public override void Update()
    {
        base.Update();

        distToPlayer = (transform.position - player.transform.position).magnitude;

        if (distToPlayer < minDistToPlayer) //the player is in range
            playerInRange = true;
        else
            playerInRange = false;
        
        if(playerInRange && !onCoolDown) //we can shoot
            Shoot();

        if (onCoolDown) //we're on cooldown
        {
            coolDown -= Time.deltaTime;
            if (coolDown <= 0)
                onCoolDown = false;
        }
    }

    public override void FixedUpdate()
    {
        if (!isAttacking) //we're not attacking, walk around normally
            base.FixedUpdate();
        else //we're attacking, stand still and wait till attack is over
        { 
            rb.velocity = Vector2.zero;
            attackTime -= Time.deltaTime;
            if (attackTime <= 0)
                isAttacking = false;
        }
    }

    //Call this method to shoot the projectile to the player
    void Shoot()
    {
        anim.SetTrigger("Shoot");
        isAttacking = true;
        attackTime = START_ATTACK_TIME;

        onCoolDown = true;
        coolDown = START_COOLDOWN;

        GameObject projectileTemp = Instantiate(projectile, transform.position, transform.rotation) as GameObject; //ref to projectile

        //check if player is left or right from us
        //set speed of projectile and if needed flip it
        if (player.transform.position.x > transform.position.x)
        {
            if (!isFacingRight)
                Flip();
            projectileTemp.GetComponent<Rigidbody2D>().velocity = new Vector2(PROJECTILE_SPEED, 0f);
            Vector3 theScale = projectileTemp.transform.localScale;
            theScale.x *= -1;
            projectileTemp.transform.localScale = theScale;
        }
        else
        {
            if (isFacingRight)
                Flip();
            projectileTemp.GetComponent<Rigidbody2D>().velocity = new Vector2(-PROJECTILE_SPEED, 0f);
        }
    }
}
