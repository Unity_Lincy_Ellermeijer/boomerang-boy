﻿using UnityEngine;
using System.Collections;

public class Pickup : MonoBehaviour
{
	Texture2D star_Tex;
	bool transferStar;
	Vector2 beginPoint;
	float duration = 0.5f;
	Vector2 point;
	float timer;
	PlayerScore playerScore;

	void Start()
	{
		playerScore = GameObject.Find ("Player").GetComponent<PlayerScore> ();
		star_Tex = playerScore.star_Tex;
	}

    void OnTriggerEnter2D(Collider2D other) 
    { 
		if (other.tag == "Player" && !transferStar)
        {
            LevelManager.currentStars++;
            //Debug.Log(LevelManager.currentStars);
			transferStar = true;
			beginPoint = Camera.main.WorldToScreenPoint(transform.position);
			beginPoint.x -= 30;
			point = beginPoint;
			beginPoint.y = Screen.height - beginPoint.y - 15;
			GetComponent<SpriteRenderer>().enabled = false;
			timer = Time.time;
            //Destroy(gameObject);
        }
    }
	void Update()
	{
		if (transferStar)
		{
			float time = (Time.time - timer) / duration;
			point = Vector2.Lerp(beginPoint, new Vector2(Screen.width-star_Tex.width-10 - LevelManager.currentStars * 15, 10), time);
			if (time > 1f)
			{
				playerScore.AddStar();
				Destroy(this.gameObject);
			}
		}
	}

	void OnGUI()
	{
		if (transferStar) 
		{
			GUI.DrawTexture(new Rect(point.x, point.y, 35, 35), star_Tex);
		}
	}
}
