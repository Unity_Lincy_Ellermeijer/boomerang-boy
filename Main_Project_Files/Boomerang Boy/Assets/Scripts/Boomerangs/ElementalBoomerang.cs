﻿using UnityEngine;
using System.Collections;

public class ElementalBoomerang : BoomerangScript
{
    //public gemaakt omdat unity niet setters in inspector laat zien *sadface*
    [SerializeField] bool isIceElement = true;
    public bool IsIceElement
    {
        get { return isIceElement; }
        set
        {
            isIceElement = value;
        }
    }

    public Sprite iceElementSprite;
    public Sprite fireElementSprite;

    ParticleSystem partSystem;
    Color fireColor, iceColor;

    protected override void Awake()
    {
        base.Awake();
        IsIceElement = isIceElement;
        
    }

    protected override void Start()
    {
        base.Start();
        partSystem = GetComponentInChildren<ParticleSystem>();
        fireColor = new Color(1f, 0.44f, 0.27f);
        iceColor = new Color(0.37f, 0.7f, 0.925f);
        ChangeParticleColor(isIceElement);
    }

    protected override void Update()
    {
        base.Update();
		if (isIceElement)
		{
			renderer.sprite = iceElementSprite;
		}
		else
		{
			renderer.sprite = fireElementSprite;
		}
    }

	public override void PickUpBoomerang()
	{
		base.PickUpBoomerang();
	}

    public override void Shoot(Vector3 _targetPos)
    {
        base.Shoot(_targetPos);
    }

    public void ChangeParticleColor(bool ice)
    {
        partSystem.startColor = (ice ? iceColor : fireColor);
    }

    protected override void OnTriggerEnter2D(Collider2D other)
    {
		if (isFired) 
		{
			if (other.tag == "ElementSwitch") 
			{
                BounceBack();
			}
		}

        if (other.tag == "BirdBoss")
        {
            other.GetComponent<BirdBossController>().TakeDamage(1, isIceElement ? 1 : 0);
        }

        if (other.tag == "ElementalEnemy")
        {
            other.GetComponent<ElementalEnemyController>().TakeDamage(1, isIceElement ? 1 : 0);
            BounceBack();
        }

        if (other.tag == "MinionBird")
        {
            other.GetComponent<BirdMinionController>().TakeDamage(1, isIceElement ? 1 : 0);
            BounceBack();
        }

        base.OnTriggerEnter2D(other);
    }
}
