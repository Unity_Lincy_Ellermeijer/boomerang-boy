﻿using UnityEngine;
using System.Collections;

//This class was made to handle the hit particles of the birdboss,
//Now its also used for the gorillaboss
public class FeatherScript : MonoBehaviour 
{
    Rigidbody2D rb;

    //Create a particle 
    public void Create(Vector2 dir, float speed, Sprite sprite, float lifeTime, float fadeTime) 
    {
        rb = gameObject.AddComponent<Rigidbody2D>();
        rb.gravityScale = Random.Range(0.1f, 0.3f);
        rb.velocity = dir * speed;

        SpriteRenderer sprRender = gameObject.AddComponent<SpriteRenderer>();
        sprRender.sprite = sprite;
        sprRender.sortingLayerName = "Particles";

        var deathScript = gameObject.AddComponent<DestoryAfterLifeTime>();
        deathScript.lifeTime = lifeTime;

        var fadeScript = gameObject.AddComponent<FadeOutBeforeDestory>();
        fadeScript.waitForFadeTime = fadeTime;
    }

    void Update()
    {
        FacePoint();
    }

    void FacePoint()
    {
        var angle = Mathf.Atan2(rb.velocity.x, rb.velocity.y) * Mathf.Rad2Deg;
        transform.eulerAngles = new Vector3(0f, 0f, -angle);
    }
}
