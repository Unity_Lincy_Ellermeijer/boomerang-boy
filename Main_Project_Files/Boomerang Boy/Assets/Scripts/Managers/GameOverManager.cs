﻿using UnityEngine;
using System.Collections;

public class GameOverManager : MonoBehaviour
{
    Animator anim;                          // Reference to the animator component.
    bool gameOver = false;
    SoundManager sound;


    void Awake()
    {
        // Set up the reference.
        anim = GetComponent<Animator>();
        sound = GameObject.Find("Main Camera").GetComponent<SoundManager>();
    }
    
    void Update()
    {
        // If the player has defeated the final boss SetTrigger("GameOver")
        if (LevelManager.gorBattleFinished) // voor testen van het eindscherm nu getriggerd door q in te drukken, maar hier moet dus if (finalboss defeated)
        {
            if (!LevelManager.gameFinished)
            {
                sound.PlayVictorySound();
            }
            LevelManager.gameFinished = true;
            anim.SetTrigger("GameOver");
            gameOver = true;
        }

        // if player hits spacebar, then go to the credits screen
        if (Input.GetKeyDown(KeyCode.Space) && gameOver)
        {
            LevelManager.SetLevelReady();
            Application.LoadLevel("Credits");
        } 
    }
}