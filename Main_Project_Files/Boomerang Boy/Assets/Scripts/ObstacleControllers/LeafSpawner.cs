﻿using UnityEngine;
using System.Collections;

public class LeafSpawner : MonoBehaviour
{
    public GameObject leafPrefab;
    SpriteRenderer renderer;
    public SpawnDirection spawnSide;

    public float spawnInterval = 3f;
    float spawnTimer;
    public bool goFallDownImmediately;
	public float imageFlip = 1;

    bool goCount;
    float finalScale;
    public float leafStartScale = 0f;
    public float leafEndScale = 1f;
    public float leafGrowDuration = 5f;
    public int leafShakeWarningDuration = 3;
    public float leafFallSpeed = 3f;
    public float leafFallDuration = 3f;
    public enum SpawnDirection
    {
        Left,
        Right
    }

    void Awake()
    {
        renderer = GetComponent<SpriteRenderer>();
        renderer.enabled = false;

        float scale = 0;
        switch (spawnSide)
        {
            case SpawnDirection.Left:
                scale = -0.01f;
                leafEndScale = -leafEndScale;
				imageFlip = -1;
				
                break;
            case SpawnDirection.Right:
                scale = 0.01f;
				imageFlip = 1;
                break;
            default:
                break;
        }

        finalScale = leafStartScale + scale;

    }

	void Start ()
    {
        SpawnLeaf();
        if (goFallDownImmediately)
        {
            GoCount();
        }
	}

    void Update()
    {
        if (goCount)
        {
            spawnTimer += Time.deltaTime;
            if (spawnTimer >= spawnInterval)
            {
                spawnTimer -= spawnInterval;
                SpawnLeaf();
            }
        }
        if (goFallDownImmediately)
        {
            GoCount();
        }
        
    }

    public void GoCount()
    {
        goCount = true;
    }

    void SpawnLeaf()
    {
        GameObject leafObj = Instantiate(leafPrefab, transform.position, Quaternion.identity) as GameObject;
        leafObj.transform.parent = this.transform;

        leafObj.transform.localScale = new Vector3(finalScale, finalScale, leafObj.transform.localScale.z);
        LeafController leafController = leafObj.GetComponent<LeafController>();
        leafController.leafSpawner = this;
        leafController.goFallDownImmediately = goFallDownImmediately;
        leafController.startScale = finalScale;
        leafController.endScale = leafEndScale;
        leafController.duration = leafGrowDuration;
        leafController.shakeWarningDuration = leafShakeWarningDuration;
        leafController.fallSpeed = leafFallSpeed;
        leafController.fallDuration = leafFallDuration;
        goCount = false;
    }
	
	
}
