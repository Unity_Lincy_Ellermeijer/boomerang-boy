﻿using UnityEngine;
using System.Collections;

public class BoomerangScript : MonoBehaviour
{

    public GameObject catchCircle;
    SpriteRenderer catchCircleRenderer;
    public float targetAlphaCircle;

    protected Animator animator;
    public LayerMask collisionLayer;
    GameObject player;
    protected SpriteRenderer renderer;
    protected Rigidbody2D rigidBody;
    protected Collider2D collider;
    Vector3 offset;

    public float travelSpeed = 14.5f;
    float currentSpeed;
    float rotateSpeed = 3200f;
    int stage = 1;
    int direction;

    Vector3 beginPos;
    Vector3 mouseTarget;

    Vector3 targetPos;
    Vector3 startPos;
    Vector3 farPoint;

    Vector3 P1;
    Vector3 P2;
    Vector3 curvePoint;
    Vector3 midWayPoint;

    float fFraction = 0;

    Vector3 oldPos;
    Vector3 lastMoveVector;

    public bool isGrowing; //growing on boomerang dispenser
    bool canDealDamageToPlayer = true;
    public bool isJustRecentlyFired;
    float justRecentlyFiredCounter;
    public bool isBeingUsed;
    public bool isFired;
    public bool isCatched;
    public bool isDropped;
    public bool isBeingStolen;
	public bool IsDropped
    {
	    get { return isDropped; }
	    set
	    {
	        isDropped = value;
	        if (isDropped)
	        {
	            collider.isTrigger = false;
	            rigidBody.gravityScale = 1;
                //renderer.enabled = true;
	        }
	        else
	        {
	            collider.isTrigger = true;
	            rigidBody.gravityScale = 0;
                //renderer.enabled = false;
	        }
	    }
	}

    protected virtual void Awake()
    {
        //isCatched = true;
        animator = GetComponent<Animator>();
        rigidBody = GetComponent<Rigidbody2D>();
        collider = GetComponent<Collider2D>();
        renderer = GetComponent<SpriteRenderer>();
        catchCircleRenderer = catchCircle.GetComponent<SpriteRenderer>();
        
    }

    protected virtual void Start()
    {
        player = GameObject.Find("Player");
        if (isCatched)
        {
            offset = transform.position - player.transform.position;
        }   
    }

    protected virtual void Update()
    {
        if (isBeingStolen)
        {
            return;
        }
        if (isCatched)
        {
            transform.position = player.transform.position + offset;
        }
        oldPos = transform.position;

        if (isFired && !isDropped)
        {
            switch (stage)
            {
                case 1:
                    fFraction += Time.deltaTime * currentSpeed;
                    if (fFraction > 0.75f)
                    {
                        MakeCurve();
                    }
                    break;
                case 2:
                    fFraction += Time.deltaTime * currentSpeed * 1.57f;
                    if (fFraction > 1f)
                    {
                        GoBack();
                    }
                    break;
                case 3:
                    isJustRecentlyFired = false;
                    fFraction += Time.deltaTime * currentSpeed;
                    if (fFraction > 1f)
                    {
                        isFired = false;
                        IsDropped = true;
                        rigidBody.AddForce(lastMoveVector * 1900f, ForceMode2D.Force);
                    }
                    break;
                default:
                    break;
            }

            Vector3 v3Delta = targetPos - startPos;
            Vector3 v3Pos = startPos;
            v3Pos.x += v3Delta.x * fFraction + Mathf.Sin(fFraction * Mathf.PI) * curvePoint.x;
            v3Pos.y += v3Delta.y * fFraction + Mathf.Sin(fFraction * Mathf.PI) * curvePoint.y;

            transform.position = v3Pos;
        }
        lastMoveVector = transform.position - oldPos;


        if (isDropped || !isCatched)
        {
            transform.eulerAngles = new Vector3(transform.eulerAngles.x, transform.eulerAngles.y, transform.eulerAngles.z - lastMoveVector.magnitude * rotateSpeed * Time.deltaTime);
        }

        if (!isCatched && !isGrowing)
        {
            float distanceToPlayer = Vector2.Distance(transform.position, player.transform.position);
            if (!isJustRecentlyFired) 
            {
                //knockback and damage boomerang return
                if (distanceToPlayer < 0.3f && canDealDamageToPlayer)
                {
                    Knockback knockbackScript = player.GetComponent<Knockback>();
                    knockbackScript.KnockBack(mouseTarget);
                    canDealDamageToPlayer = false;
                    if (knockbackScript.canBeDealtDamageByBoomerang)
                    {
                        player.GetComponent<PlayerHealth>().TakeDamage(1);
                    }
                }

                //catchcircle appearing

				if (!catchCircleRenderer.enabled)
                {
					Boomerang boomerangController = player.GetComponent<Boomerang>();
					BoomerangScript activeBoomerang = null;
					if (boomerangController.boomerang != null) 
					{
						activeBoomerang = boomerangController.boomerang.GetComponent<BoomerangScript>();
					}

					if (activeBoomerang == null || !activeBoomerang.isCatched)
					{
						catchCircleRenderer.enabled = true;
					}
                    
                }
            }
            else
            {
                catchCircleRenderer.enabled = false;
            }
        }
		if (catchCircleRenderer.enabled) 
		{
			Boomerang boomerangController = player.GetComponent<Boomerang>();
			BoomerangScript activeBoomerang = null;
			if (boomerangController.boomerang != null) 
			{
				activeBoomerang = boomerangController.boomerang.GetComponent<BoomerangScript>();
			}
			if (activeBoomerang != null && activeBoomerang.isCatched)
			{
				catchCircleRenderer.enabled = false;
			}
		}

       // animator.SetBool("inSky", isFired);
       // animator.SetBool("onGround", isDropped);
       // animator.SetBool("inHands", isCatched);
        renderer.enabled = true;
        if (isCatched)
        {
            renderer.enabled = false;
        }
    }

	public virtual void PickUpBoomerang()
    {
        isCatched = true;
        IsDropped = false;
        isFired = false;
        transform.position = player.transform.position + offset;
        rigidBody.velocity = Vector3.zero;

        if (catchCircleRenderer.enabled)
        {
            catchCircleRenderer.enabled = false;
        }
    }


    public virtual void Shoot(Vector3 _targetPos)
    {

        if (!isFired && !IsDropped && isCatched)
        {
            canDealDamageToPlayer = true;
            fFraction = 0;
            isJustRecentlyFired = true;
            mouseTarget = _targetPos;
            stage = 1;
            transform.position = player.transform.position + offset;
            startPos = transform.position;
            beginPos = startPos;

            if ((mouseTarget.x > startPos.x && mouseTarget.y < startPos.y) ||
                (mouseTarget.x < startPos.x && mouseTarget.y >= startPos.y))
                direction = 2;
            else
                direction = 1;

            isCatched = false;
            isFired = true;
            targetPos = _targetPos;
            Vector3 diffVector = (targetPos - startPos) * 0.75f;
            P1 = new Vector3(-diffVector.y, diffVector.x) / Mathf.Sqrt(Mathf.Pow(diffVector.x, 2) + Mathf.Pow(diffVector.y, 2)) * 1f;
            
            P2 = new Vector3(-diffVector.y, diffVector.x) / Mathf.Sqrt(Mathf.Pow(diffVector.x, 2) + Mathf.Pow(diffVector.y, 2)) * -1f;

            if (direction == 1)
                curvePoint = P1 * 0.88f;
            else
                curvePoint = P2 * 0.88f;
            

            targetPos = startPos + (targetPos - startPos) * 1;
            midWayPoint = targetPos;
            currentSpeed = 1f / (mouseTarget - transform.position).magnitude * travelSpeed;
            if (currentSpeed > 4.5f)
                currentSpeed = 4.5f;

        }

    }

    private void MakeCurve()
    {
        stage = 2;
        fFraction = 0;
        curvePoint = (mouseTarget - startPos) * 0.23f;
        Vector3 diffVector = startPos + (mouseTarget - startPos) * 0.75f;
        startPos = transform.position;
       
        if (direction == 1)
            targetPos = diffVector + P2 * 0.5f;
        else
            targetPos = diffVector + P1 * 0.5f;

        Vector3 v3Delta = targetPos - startPos;
        Vector3 v3Pos = startPos;
        v3Pos.x += v3Delta.x * 0.5f + Mathf.Sin(fFraction * Mathf.PI) * curvePoint.x;
        v3Pos.y += v3Delta.y * 0.5f + Mathf.Sin(fFraction * Mathf.PI) * curvePoint.y;
    }
    

    

    private void GoBack()
    {
        stage = 3;
        startPos = midWayPoint;
        fFraction = 0.25f;
        if (direction == 1)
            curvePoint = P2 * 0.67f;
        else
            curvePoint = P1 * 0.67f;
        targetPos = beginPos;
    }

    protected void BounceBack()
    {
        stage = 3;
        startPos = transform.position;
        fFraction = 0;
        curvePoint = Vector3.zero;// P2 * 0.67f;
        targetPos = beginPos;
        currentSpeed = 1f / (startPos - targetPos).magnitude * (travelSpeed - 1f);
        if (currentSpeed > 4.75f)
            currentSpeed = 4.75f;
    }

    protected virtual void OnTriggerEnter2D(Collider2D other)
    {
        
        if (isFired)
        {
            if (other.tag == "Enemy")
            {
                BounceBack();
                EnemyController enemy = other.GetComponent<EnemyController>();
                enemy.TakeDamage(1f);
            }
            if (other.tag == "Collision")
            {
                
                Vector3 fromPosition = transform.position;
                Vector3 toPosition = player.transform.position;
                Vector3 direction = toPosition - fromPosition;
                float distance = direction.magnitude;
                if (Physics2D.Raycast(transform.position + direction.normalized * 0.5f, direction, distance, collisionLayer))
                {
                    isFired = false;
                    IsDropped = true;
                    isCatched = false;
                    isJustRecentlyFired = false;
                }
                else
                {
                    BounceBack();
                }
            }

            if(other.tag == "BirdBoss")
            {
                BounceBack();
            }
            
        }

    }

}