﻿using UnityEngine;
using System.Collections;

//This class returns true if the player is in range of its trigger collider
public class ChangePlayerInRange : MonoBehaviour
{
    public bool playerInZone = false;

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            playerInZone = true;
        }
    }
    void OnTriggerExit2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            playerInZone = false;
        }
    }
}
