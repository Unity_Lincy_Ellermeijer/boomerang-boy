﻿using UnityEngine;
using System.Collections;

//In this state the boss dies and gets destroyed after he's done dying
public class StateDyingGor : State<GorillaBossController>
{
    float timer; //die time

    public override void Enter(GorillaBossController agent)
    {
        agent.SetMovementSpeed(Vector2.zero); //don't move

        agent.Anim.SetBool("Dead", true);

        agent.Invulnerable = true;

        timer = Time.fixedTime + agent.mySettings.dieTime;

        //spawn epic particles
        agent.SetParticleInformation(agent.mySettings.amountParticlesDead, agent.mySettings.speedParticlesDead, agent.mySettings.scaleParticlesDead, agent.mySettings.lifeTimeParticlesDead, agent.mySettings.fadeTimeParticlesDead);
        agent.InstantiateParticle();
    }

    public override void Execute(GorillaBossController agent)
    {
        if (timer < Time.fixedTime) //done dying? battle done.
        {
            agent.ClearBattle();
            agent.Destroy();
        }
    }

    public override void Exit(GorillaBossController agent)
    {
    }

}