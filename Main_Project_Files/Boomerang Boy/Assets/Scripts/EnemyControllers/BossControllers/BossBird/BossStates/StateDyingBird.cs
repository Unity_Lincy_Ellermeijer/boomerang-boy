﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//This is the state in which the bird dies
//He is already centered at the mid pos
public class StateDyingBird : State<BirdBossController>
{
    float timer; //time it takes to die

    public override void Enter(BirdBossController agent)
    {
        //just in case!!!
        agent.SetMovementSpeed(Vector2.zero);

        agent.Anim.SetTrigger("Centering");
        agent.Anim.SetBool("Die", true);

        agent.Invulnerable = true;

        //set information for particles and spawn them afterwards
        agent.SetFeatherParticleInformation(agent.mySettings.amountFeatherDead, agent.mySettings.speedFeatherDead, agent.mySettings.scaleFeatherDead, agent.mySettings.lifeTimeFeatherDead, agent.mySettings.fadeTimeFeatherDead);
        agent.InstantiateDeadParticle();

        timer = Time.fixedTime + agent.mySettings.dieTime;
    }

    public override void Execute(BirdBossController agent)
    {
        if (timer < Time.fixedTime) //done dying
        {
            agent.ClearBattle(); 
            agent.Destroy();
        }
    }

    public override void Exit(BirdBossController agent)
    {
        agent.SetMovementSpeed(Vector2.zero);
        agent.ResetRot();
        agent.Invulnerable = false;
    }    
}
