﻿using UnityEngine;
using System.Collections;

public class WoodenBoomerang : BoomerangScript 
{
    //niet heel nodig om hier een aparte klasse van te maken
    //maar misschien besluiten we de wooden iets extra's te geven idunno
    protected override void Awake()
    {
        base.Awake();
    }

    protected override void Start() 
    {
        base.Start();
	}

	protected override void Update () 
    {
        base.Update();
	}

    public override void Shoot(Vector3 _targetPos)
    {
        base.Shoot(_targetPos);
    }

    protected override void OnTriggerEnter2D(Collider2D other)
    {
        base.OnTriggerEnter2D(other);
    }
}
