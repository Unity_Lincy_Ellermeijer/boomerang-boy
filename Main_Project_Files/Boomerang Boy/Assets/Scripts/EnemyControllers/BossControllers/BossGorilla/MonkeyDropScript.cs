﻿using UnityEngine;
using System.Collections;

//A monkey that can walk between 2 points, thrown in the StateMonkeyAttack
public class MonkeyDropScript : AttackObstacleScript<GorillaBossController>
{
    Vector2 target;
    float lookOffset = 0;
    GameObject monkey; //ref to prefab to drop

    //Call this method to create a monkey
    public override void Create(Vector2 tempTarget, Sprite sprite, GorillaBossController agent)
    {
        base.Create(tempTarget, sprite, agent);

        thisSprite = Resources.Load<Sprite>("Art/Sprites/Enemies/Boss/Gorilla/Attacks/monkeyDropParticle");

        target = tempTarget;
        monkey = Resources.Load<GameObject>("Prefabs/Enemies/Monkeys/EdgeAndWallCheckEnemy");

        CircleCollider2D coll = gameObject.AddComponent<CircleCollider2D>();
        coll.radius = 0.64f;
        coll.isTrigger = true;

        MoveSpeed = agent.mySettings.throwSpeed.GetRandomInRange();

        Vector2 distToTarget = new Vector2(target.x, target.y) - new Vector2(transform.position.x, transform.position.y);
        distToTarget.Normalize();

        Rb.velocity = new Vector2(distToTarget.x * MoveSpeed, distToTarget.y * MoveSpeed/2); //calculate velocity
    }

    public override void FixedUpdate()
    {
        FacePoint();
    }

    //Look where you're going by looking at the position where we will be soon
    void FacePoint()
    {
        var lookPos = new Vector2(transform.position.x, transform.position.y) + new Vector2(Rb.velocity.x, Rb.velocity.y - 25);
        var angle = Mathf.Atan2(lookPos.x, lookPos.y) * Mathf.Rad2Deg; 
        transform.eulerAngles = new Vector3(0f, 0f, -angle + lookOffset);
    }

    public override void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Collision")
        {
            Instantiate(monkey, transform.position, Quaternion.identity); //Instantiate the actual prefab monkey when we hit the ground
        }

        base.OnTriggerEnter2D(other);
    }
}
