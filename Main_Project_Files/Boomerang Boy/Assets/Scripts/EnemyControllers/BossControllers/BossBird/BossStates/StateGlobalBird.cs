﻿using UnityEngine;
using System.Collections;

public class StateGlobalBird : State<BirdBossController>
{
    //float dmgtesttimer;

    public override void Enter(BirdBossController agent)
    {
        //dmgtesttimer = Time.fixedTime + 3f;
    }

    public override void Execute(BirdBossController agent)
    {
        //condition to flip
        if ((agent.GetVelocity.x < 0 && !agent.IsFacingLeft) || (agent.GetVelocity.x > 0 && agent.IsFacingLeft))
            agent.Flip();

        /*if (dmgtesttimer < Time.fixedTime && !agent.Dead)
        {
            agent.TakeDamage(1, agent.MyElement == BirdBossController.Element.fire ? 1 : 0);
           dmgtesttimer = Time.fixedTime + 3f;
        }*/
    }
    
    public override void Exit(BirdBossController agent)
    {
    }
}
