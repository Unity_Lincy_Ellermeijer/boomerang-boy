﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
public class GolemController : MonoBehaviour 
{
	SoundManager sound;
    CameraController cam;
    public GolemBossManager manager;
	GameObject player;
    public GameObject dustCloudPrefab;
    public BoxCollider2D boundingBox;

    Vector2 preActionBasePos;
    Vector2 basePos;
    float baseRotation;

	bool actionToggle;
	bool doAction;
	int segmentInState = 0;

	float throwTime = 0;

	Vector2 targetPos;

	GameObject leftArm;
	Vector2 leftArmStartPos;
	Vector2 preActionLeftArmPos;
    float leftArmStartRotation;

	GameObject rightArm;
	Vector2 rightArmStartPos;
	Vector2 preActionRightArmPos;
    float rightArmStartRotation;

	GameObject leftLeg;
	Vector2 leftLegStartPos;
	Vector2 preActionLeftLegPos;
    float leftLegStartRotation;

	GameObject rightLeg;
	Vector2 rightLegStartPos;
	Vector2 preActionRightLegPos;
    float rightLegStartRotation;

	GameObject body;
	Vector2 bodyStartPos;
	Vector2 preActionBodyPos;
    float bodyStartRotation;

    bool[] flyingParts = new bool[5];

    public State currentState = State.Idle;
    public State nextState = State.Idle;

    public bool activated;

    public int health;
    public int maxHealth;

    public int cycleInt = 0;
	float animationTimer;
    public float idleFrameDuration = 0.5f;
    public float idleTimeDuration = 1.0f;
    public float rockFrameDuration = 0.3f;
    public int bounceTimes = 3;
	public float throwFrameDuration = 0.5f;
	public float throwDuration = 0.65f;
    public int throwTimes = 3;
    public float throwRotationSpeed = 1000f;
    public float climbFrameDuration = 0.5f;
    public float falldownFrameDuration = 0.02f;
    public float walkFrameDuration = 0.03f;
    public float walkSpeed = 2f;
    public float standupFrameDuration = 0.08f;

    public float deathFrameDuration = 1f;

   
    float deathDustSpawnTimer;
    public float deathDustSpawnInterval = 0.1f;

    int bodyPartNumber = 0;
    float deathBodyPartExplodeTimer;
    public float deathBodyPartExplodeInterval = 0.5f;


    List<Vector3> walkPoints = new List<Vector3>();
    float idleTimer;
    float startReposJumpTime;
    int currentThrowTimes;
    int currentBounceTimes;
    int currentPlatform;
    public int currentPoint;
    int targetPoint;
    float deathTimer;

    public bool doKill;
    public bool doSpawnDispenser;

    public float currentFrameDuration;

	public enum State
	{
		Idle,
        Rock,
		Throw,
        StartClimb,
        MidClimb,
        StartClimbJump,
        MidClimbJump,
        EndClimbJump,
        RepositionJump,
        RepositionWalk,
        Sleeping,
        SleepingStandingUp,
        Death
	}

    Dictionary<State, List<AnimationState>> baseInfo = new Dictionary<State, List<AnimationState>>();
    Dictionary<State, List<AnimationState>> leftArmInfo = new Dictionary<State, List<AnimationState>>();
    Dictionary<State, List<AnimationState>> rightArmInfo = new Dictionary<State, List<AnimationState>>();
    Dictionary<State, List<AnimationState>> leftLegInfo = new Dictionary<State, List<AnimationState>>();
    Dictionary<State, List<AnimationState>> rightLegInfo = new Dictionary<State, List<AnimationState>>();
    Dictionary<State, List<AnimationState>> bodyInfo = new Dictionary<State, List<AnimationState>>();

	void Start ()
	{
		sound = GameObject.Find("Main Camera").GetComponent<SoundManager>();
        health = maxHealth;
        currentPlatform = 0;
        currentPoint = 0;
        DecideState();

        walkPoints.Add(transform.localPosition);
        walkPoints.Add(new Vector3(2.0f, 0, transform.localPosition.z));
        walkPoints.Add(new Vector3(-2.0f, 0, transform.localPosition.z));
        walkPoints.Add(new Vector3(11.22f, -4f, transform.localPosition.z));
        walkPoints.Add(new Vector3(9.5f, -4f, transform.localPosition.z));
        walkPoints.Add(new Vector3(7.2f, -4f, transform.localPosition.z));
        
		player = GameObject.Find ("Player"); 
        cam = Camera.main.gameObject.GetComponent<CameraController>();

        SwitchState(State.Sleeping);

        basePos = transform.position;
        baseRotation = transform.localEulerAngles.z;

        body = transform.FindChild("Body").gameObject;
        bodyStartPos = body.transform.localPosition;
        bodyStartRotation = body.transform.localEulerAngles.z;

        leftArm = transform.FindChild("LeftArm").gameObject;
        leftArmStartPos = leftArm.transform.localPosition;
        leftArmStartRotation = leftArm.transform.localEulerAngles.z;

        rightArm = transform.FindChild("RightArm").gameObject;
        rightArmStartPos = rightArm.transform.localPosition;
        rightArmStartRotation = rightArm.transform.localEulerAngles.z;

		leftLeg = transform.FindChild ("LeftLeg").gameObject;
        leftLegStartPos = leftLeg.transform.localPosition;
        leftLegStartRotation = leftLeg.transform.localEulerAngles.z;

		rightLeg = transform.FindChild ("RightLeg").gameObject;
        rightLegStartPos = rightLeg.transform.localPosition;
        rightLegStartRotation = rightLeg.transform.localEulerAngles.z;

		#region idle
        //idle
        List<AnimationState> idleBasePos = new List<AnimationState>();
        idleBasePos.Add(new AnimationState(0, 0.0f, 0));
        idleBasePos.Add(new AnimationState(0, 0.0f, 0));
        baseInfo.Add(State.Idle, idleBasePos);

        List<AnimationState> idleLeftArmPos = new List<AnimationState>();
        idleLeftArmPos.Add(new AnimationState(0, 0.0f, 0));
        idleLeftArmPos.Add(new AnimationState(0, 0.03f, 5));
		leftArmInfo.Add (State.Idle, idleLeftArmPos);

        List<AnimationState> idleRightArmPos = new List<AnimationState>();
        idleRightArmPos.Add(new AnimationState(0, 0.0f, 0));
        idleRightArmPos.Add(new AnimationState(0, 0.03f, 0));
		rightArmInfo.Add (State.Idle, idleRightArmPos);

        List<AnimationState> idleLeftLegPos = new List<AnimationState>();
        idleLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
        idleLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
        leftLegInfo.Add(State.Idle, idleLeftLegPos);

        List<AnimationState> idleRightLegPos = new List<AnimationState>();
        idleRightLegPos.Add(new AnimationState(0, 0.0f, 0));
        idleRightLegPos.Add(new AnimationState(0, 0.0f, 0));
        rightLegInfo.Add(State.Idle, idleRightLegPos);

        List<AnimationState> idleBodyPos = new List<AnimationState>();
        idleBodyPos.Add(new AnimationState(0, 0.0f, 0));
        idleBodyPos.Add(new AnimationState(0, -0.03f, 0));
		bodyInfo.Add (State.Idle, idleBodyPos);

		#endregion

		#region rockjump
        //rock
        List<AnimationState> rockBasePos = new List<AnimationState>();
        rockBasePos.Add(new AnimationState(0, 0.0f, 0));
        rockBasePos.Add(new AnimationState(0, -0.02f, 0));
        rockBasePos.Add(new AnimationState(0, -0.05f, 0));
        rockBasePos.Add(new AnimationState(0, 0.01f, 0));
        rockBasePos.Add(new AnimationState(0, 0.0f, 0));
        //5
        rockBasePos.Add(new AnimationState(0, 0.03f, 1));
        rockBasePos.Add(new AnimationState(0, 0.09f, 2));
        rockBasePos.Add(new AnimationState(0, 0.2f, 2));
        rockBasePos.Add(new AnimationState(0, 0.3f, 2));
        rockBasePos.Add(new AnimationState(0, 0.42f, 3));
        //10
        rockBasePos.Add(new AnimationState(0, 0.5f, 3));
        rockBasePos.Add(new AnimationState(0, 0.65f, 2));
        rockBasePos.Add(new AnimationState(0, 0.8f, 2));
        rockBasePos.Add(new AnimationState(0, 0.9f, 2));
        rockBasePos.Add(new AnimationState(0, 0.95f, 1));
        //15
        rockBasePos.Add(new AnimationState(0, 0.98f, 1));
        rockBasePos.Add(new AnimationState(0, 1f, 0));
        rockBasePos.Add(new AnimationState(0, 0.98f, 0));
        rockBasePos.Add(new AnimationState(0, 0.95f, 0));
        rockBasePos.Add(new AnimationState(0, 0.9f, 0));
        //20
        rockBasePos.Add(new AnimationState(0, 0.8f, 0));
        rockBasePos.Add(new AnimationState(0, 0.65f, 0));
        rockBasePos.Add(new AnimationState(0, 0.5f, 5));
        rockBasePos.Add(new AnimationState(0, 0.34f, 10));
        rockBasePos.Add(new AnimationState(0, 0.18f, 15));
        //25
        rockBasePos.Add(new AnimationState(0, 0.1f, 13, true));
        rockBasePos.Add(new AnimationState(0, 0.09f, 8));
        rockBasePos.Add(new AnimationState(0, 0.07f, 4));
        rockBasePos.Add(new AnimationState(0, 0.05f, 2));
        rockBasePos.Add(new AnimationState(0, 0.03f, 1));
        //30
        rockBasePos.Add(new AnimationState(0, 0.02f, 0));
        rockBasePos.Add(new AnimationState(0, 0.00f, 0));
        baseInfo.Add(State.Rock, rockBasePos);

        List<AnimationState> rockLeftArmPos = new List<AnimationState>();
        rockLeftArmPos.Add(new AnimationState(0, 0.0f, 0));
        rockLeftArmPos.Add(new AnimationState(0.0f, 0.0f, 3));
        rockLeftArmPos.Add(new AnimationState(0.0f, 0.0f, 7));
        rockLeftArmPos.Add(new AnimationState(0.0f, 0.0f, 1));
        rockLeftArmPos.Add(new AnimationState(0, 0.0f, 0));
        //5
        rockLeftArmPos.Add(new AnimationState(-0.01f, 0.00f, -3));
        rockLeftArmPos.Add(new AnimationState(-0.02f, 0.00f, -7));
        rockLeftArmPos.Add(new AnimationState(-0.03f, 0.00f, -10));
        rockLeftArmPos.Add(new AnimationState(-0.04f, 0.00f, -20));
        rockLeftArmPos.Add(new AnimationState(-0.06f, 0.00f, -32));
        //10
        rockLeftArmPos.Add(new AnimationState(-0.08f, 0.00f, -44));
        rockLeftArmPos.Add(new AnimationState(-0.1f, 0.00f, -60));
        rockLeftArmPos.Add(new AnimationState(-0.11f, 0.00f, -76));
        rockLeftArmPos.Add(new AnimationState(-0.12f, 0.00f, -90));
        rockLeftArmPos.Add(new AnimationState(-0.12f, 0.00f, -100));
        //15
        rockLeftArmPos.Add(new AnimationState(-0.13f, 0.00f, -105));
        rockLeftArmPos.Add(new AnimationState(-0.14f, 0.00f, -108));
        rockLeftArmPos.Add(new AnimationState(-0.15f, 0.00f, -112));
        rockLeftArmPos.Add(new AnimationState(-0.17f, 0.00f, -115));
        rockLeftArmPos.Add(new AnimationState(-0.20f, 0.00f, -116));
        //20
        rockLeftArmPos.Add(new AnimationState(-0.24f, -0.02f, -112));
        rockLeftArmPos.Add(new AnimationState(-0.30f, -0.06f, -95));
        rockLeftArmPos.Add(new AnimationState(-0.37f, -0.12f, -88));
        rockLeftArmPos.Add(new AnimationState(-0.42f, -0.20f, -88));
        rockLeftArmPos.Add(new AnimationState(-0.45f, -0.28f, -75));
        //25
        rockLeftArmPos.Add(new AnimationState(-0.50f, -0.40f, -80));
        rockLeftArmPos.Add(new AnimationState(-0.47f, -0.38f, -76));
        rockLeftArmPos.Add(new AnimationState(-0.42f, -0.34f, -70));
        rockLeftArmPos.Add(new AnimationState(-0.33f, -0.28f, -64));
        rockLeftArmPos.Add(new AnimationState(-0.25f, -0.22f, -58));
        //30
        rockLeftArmPos.Add(new AnimationState(-0.14f, -0.12f, -42));
        rockLeftArmPos.Add(new AnimationState(-0.02f, -0.0f, -26));

        leftArmInfo.Add(State.Rock, rockLeftArmPos);

        List<AnimationState> rockRightArmPos = new List<AnimationState>();
        rockRightArmPos.Add(new AnimationState(0, 0.0f, 0));
        rockRightArmPos.Add(new AnimationState(0.0f, 0.0f, 3));
        rockRightArmPos.Add(new AnimationState(0.0f, 0.0f, 7));
        rockRightArmPos.Add(new AnimationState(0.0f, 0.0f, 1));
        rockRightArmPos.Add(new AnimationState(0, 0.0f, 0));
        //5
        rockRightArmPos.Add(new AnimationState(-0.01f, 0.0f, -3));
        rockRightArmPos.Add(new AnimationState(-0.02f, 0.0f, -7));
        rockRightArmPos.Add(new AnimationState(-0.03f, 0.0f, -10));
        rockRightArmPos.Add(new AnimationState(-0.04f, 0.0f, -20));
        rockRightArmPos.Add(new AnimationState(-0.06f, 0.0f, -32));
        //10
        rockRightArmPos.Add(new AnimationState(-0.08f, 0.0f, -50));
        rockRightArmPos.Add(new AnimationState(-0.1f, 0.0f, -68));
        rockRightArmPos.Add(new AnimationState(-0.1f, 0.0f, -82));
        rockRightArmPos.Add(new AnimationState(-0.11f, 0.0f, -94));
        rockRightArmPos.Add(new AnimationState(-0.12f, 0.0f, -104));
        //15
        rockRightArmPos.Add(new AnimationState(-0.13f, 0.0f, -110));
        rockRightArmPos.Add(new AnimationState(-0.14f, 0.0f, -113));
        rockRightArmPos.Add(new AnimationState(-0.15f, 0.0f, -117));
        rockRightArmPos.Add(new AnimationState(-0.17f, 0.0f, -120));
        rockRightArmPos.Add(new AnimationState(-0.20f, 0.0f, -122));
        //20
        rockRightArmPos.Add(new AnimationState(-0.24f, -0.02f, -118));
        rockRightArmPos.Add(new AnimationState(-0.30f, -0.06f, -98));
        rockRightArmPos.Add(new AnimationState(-0.37f, -0.12f, -84));
        rockRightArmPos.Add(new AnimationState(-0.42f, -0.20f, -80));
        rockRightArmPos.Add(new AnimationState(-0.45f, -0.28f, -66));
        //25
        rockRightArmPos.Add(new AnimationState(-0.50f, -0.40f, -62));
        rockRightArmPos.Add(new AnimationState(-0.47f, -0.38f, -53));
        rockRightArmPos.Add(new AnimationState(-0.42f, -0.34f, -40));
        rockRightArmPos.Add(new AnimationState(-0.33f, -0.28f, -31));
        rockRightArmPos.Add(new AnimationState(-0.25f, -0.22f, -25));
        //30
        rockRightArmPos.Add(new AnimationState(-0.14f, -0.12f, -19));
        rockRightArmPos.Add(new AnimationState(-0.02f, -0.0f, -10));
        rightArmInfo.Add(State.Rock, rockRightArmPos);

        List<AnimationState> rockLeftLegPos = new List<AnimationState>();
        rockLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
        rockLeftLegPos.Add(new AnimationState(0, 0.03f, 0));
        rockLeftLegPos.Add(new AnimationState(0, 0.06f, 0));
        rockLeftLegPos.Add(new AnimationState(0, 0.01f, 0));
        rockLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
        //5
        rockLeftLegPos.Add(new AnimationState(0, -0.03f, 0));
        rockLeftLegPos.Add(new AnimationState(0, -0.09f, 0));
        rockLeftLegPos.Add(new AnimationState(0, -0.16f, 0));
        rockLeftLegPos.Add(new AnimationState(0, -0.22f, 0));
        rockLeftLegPos.Add(new AnimationState(0, -0.25f, 0));
        //10
        rockLeftLegPos.Add(new AnimationState(0, -0.21f, 0));
        rockLeftLegPos.Add(new AnimationState(0, -0.14f, 0));
        rockLeftLegPos.Add(new AnimationState(0, -0.06f, 0));
        rockLeftLegPos.Add(new AnimationState(0, -0.03f, 0));
        rockLeftLegPos.Add(new AnimationState(0, -0.03f, 0));
        //15
        rockLeftLegPos.Add(new AnimationState(0, -0.03f, 0));
        rockLeftLegPos.Add(new AnimationState(0, -0.02f, 0));
        rockLeftLegPos.Add(new AnimationState(0, -0.01f, 0));
        rockLeftLegPos.Add(new AnimationState(0, 0.00f, 0));
        rockLeftLegPos.Add(new AnimationState(0, 0.00f, 2));
        //20
        rockLeftLegPos.Add(new AnimationState(0, 0.00f, 4));
        rockLeftLegPos.Add(new AnimationState(0, 0.00f, 7));
        rockLeftLegPos.Add(new AnimationState(0, 0.00f, 9));
        rockLeftLegPos.Add(new AnimationState(0, 0.00f, 9));
        rockLeftLegPos.Add(new AnimationState(0, 0.00f, 9));
        //25
        rockLeftLegPos.Add(new AnimationState(0, 0.00f, 7));
        rockLeftLegPos.Add(new AnimationState(0, 0.00f, 5));
        rockLeftLegPos.Add(new AnimationState(0, 0.00f, 3));
        rockLeftLegPos.Add(new AnimationState(0, 0.00f, 0));
        rockLeftLegPos.Add(new AnimationState(0, 0.00f, 0));
        //30
        rockLeftLegPos.Add(new AnimationState(0, 0.00f, 0));
        rockLeftLegPos.Add(new AnimationState(0, 0.00f, 0));
        leftLegInfo.Add(State.Rock, rockLeftLegPos);

        List<AnimationState> rockRightLegPos = new List<AnimationState>();
        rockRightLegPos.Add(new AnimationState(0, 0.0f, 0));
        rockRightLegPos.Add(new AnimationState(0, 0.03f, 0));
        rockRightLegPos.Add(new AnimationState(0, 0.06f, 0));
        rockRightLegPos.Add(new AnimationState(0, 0.01f, 0));
        rockRightLegPos.Add(new AnimationState(0, 0.0f, 0));
        //5
        rockRightLegPos.Add(new AnimationState(0, -0.03f, 0));
        rockRightLegPos.Add(new AnimationState(0, -0.09f, 0));
        rockRightLegPos.Add(new AnimationState(0, -0.16f, 0));
        rockRightLegPos.Add(new AnimationState(0, -0.22f, 0));
        rockRightLegPos.Add(new AnimationState(0, -0.25f, 0));
        //10
        rockRightLegPos.Add(new AnimationState(0, -0.21f, 0));
        rockRightLegPos.Add(new AnimationState(0, -0.14f, 0));
        rockRightLegPos.Add(new AnimationState(0, -0.06f, 0));
        rockRightLegPos.Add(new AnimationState(0, -0.03f, 0));
        rockRightLegPos.Add(new AnimationState(0, -0.03f, 0));
        //15
        rockRightLegPos.Add(new AnimationState(0, -0.03f, 0));
        rockRightLegPos.Add(new AnimationState(0, -0.02f, 0));
        rockRightLegPos.Add(new AnimationState(0, -0.01f, 0));
        rockRightLegPos.Add(new AnimationState(0, 0.00f, 0));
        rockRightLegPos.Add(new AnimationState(0, 0.00f, 2));
        //20
        rockRightLegPos.Add(new AnimationState(0, 0.00f, 4));
        rockRightLegPos.Add(new AnimationState(0, 0.00f, 7));
        rockRightLegPos.Add(new AnimationState(0, 0.00f, 9));
        rockRightLegPos.Add(new AnimationState(0, 0.00f, 9));
        rockRightLegPos.Add(new AnimationState(0, 0.00f, 9));
        //25
        rockRightLegPos.Add(new AnimationState(0, 0.00f, 7));
        rockRightLegPos.Add(new AnimationState(0, 0.00f, 5));
        rockRightLegPos.Add(new AnimationState(0, 0.00f, 3));
        rockRightLegPos.Add(new AnimationState(0, 0.00f, 0));
        rockRightLegPos.Add(new AnimationState(0, 0.00f, 0));
        //30
        rockRightLegPos.Add(new AnimationState(0, 0.00f, 0));
        rockRightLegPos.Add(new AnimationState(0, 0.00f, 0));
        rightLegInfo.Add(State.Rock, rockRightLegPos);

        List<AnimationState> rockBodyPos = new List<AnimationState>();
        rockBodyPos.Add(new AnimationState(0, 0.0f, 0));
        rockBodyPos.Add(new AnimationState(0, 0.0f, 1));
        rockBodyPos.Add(new AnimationState(0, 0.0f, 3));
        rockBodyPos.Add(new AnimationState(0, 0.0f, 1));
        rockBodyPos.Add(new AnimationState(0, 0.0f, 0));
        //5
        rockBodyPos.Add(new AnimationState(0, 0.00f, -1));
        rockBodyPos.Add(new AnimationState(0, 0.00f, -2));
        rockBodyPos.Add(new AnimationState(0, 0.00f, -3));
        rockBodyPos.Add(new AnimationState(0, 0.00f, -5));
        rockBodyPos.Add(new AnimationState(0, 0.00f, -7));
        //10
        rockBodyPos.Add(new AnimationState(0, 0.00f, -7));
        rockBodyPos.Add(new AnimationState(0, 0.00f, -5));
        rockBodyPos.Add(new AnimationState(0, 0.00f, -3));
        rockBodyPos.Add(new AnimationState(0, 0.00f, -2));
        rockBodyPos.Add(new AnimationState(0, 0.00f, -1));
        //15
        rockBodyPos.Add(new AnimationState(0, 0.00f, 0));
        rockBodyPos.Add(new AnimationState(0, 0.00f, 0));
        rockBodyPos.Add(new AnimationState(0, 0.00f, 1));
        rockBodyPos.Add(new AnimationState(0, 0.00f, 3));
        rockBodyPos.Add(new AnimationState(0, 0.00f, 7));
        //20
        rockBodyPos.Add(new AnimationState(0, 0.00f, 11));
        rockBodyPos.Add(new AnimationState(0, 0.00f, 15));
        rockBodyPos.Add(new AnimationState(0, 0.00f, 22));
        rockBodyPos.Add(new AnimationState(0, 0.00f, 32));
        rockBodyPos.Add(new AnimationState(0, 0.00f, 44));
        //25
        rockBodyPos.Add(new AnimationState(0, 0.00f, 54));
        rockBodyPos.Add(new AnimationState(0, 0.00f, 50));
        rockBodyPos.Add(new AnimationState(0, 0.00f, 40));
        rockBodyPos.Add(new AnimationState(0, 0.00f, 30));
        rockBodyPos.Add(new AnimationState(0, 0.00f, 20));
        //30
        rockBodyPos.Add(new AnimationState(0, 0.00f, 10));
        rockBodyPos.Add(new AnimationState(0, 0.00f, 0));
        bodyInfo.Add(State.Rock, rockBodyPos);
		#endregion

		#region throw
		//throw
		List<AnimationState> throwBasePos = new List<AnimationState>();
		throwBasePos.Add(new AnimationState(0, 0.0f, 0));
		throwBasePos.Add(new AnimationState(0, 0.0f, 0));
		throwBasePos.Add(new AnimationState(0, 0.0f, 0));
		throwBasePos.Add(new AnimationState(0, 0.0f, 0));
		throwBasePos.Add(new AnimationState(0, 0.0f, 0));
		throwBasePos.Add(new AnimationState(0, 0.0f, 0));
		throwBasePos.Add(new AnimationState(0, 0.0f, 0));
		throwBasePos.Add(new AnimationState(0, 0.0f, 0));
		throwBasePos.Add(new AnimationState(0, 0.0f, 0));
		throwBasePos.Add(new AnimationState(0, 0.0f, 0));
		throwBasePos.Add(new AnimationState(0, 0.0f, 0));
		throwBasePos.Add(new AnimationState(0, 0.0f, 0));
		throwBasePos.Add(new AnimationState(0, 0.0f, 0));
		throwBasePos.Add(new AnimationState(0, 0.0f, 0));
		throwBasePos.Add(new AnimationState(0, 0.0f, 0));
		throwBasePos.Add(new AnimationState(0, 0.0f, 0, true));
		baseInfo.Add(State.Throw, throwBasePos);
		
		List<AnimationState> throwLeftArmPos = new List<AnimationState>();
		throwLeftArmPos.Add(new AnimationState(0, 0.0f, 0));
		throwLeftArmPos.Add(new AnimationState(0, 0.0f, -5));
		throwLeftArmPos.Add(new AnimationState(0, 0.0f, -11));
		throwLeftArmPos.Add(new AnimationState(0, 0.0f, -23));
		throwLeftArmPos.Add(new AnimationState(0, 0.0f, -36));
		throwLeftArmPos.Add(new AnimationState(0, 0.0f, -50));
		throwLeftArmPos.Add(new AnimationState(0, 0.0f, -65));
		throwLeftArmPos.Add(new AnimationState(0, 0.0f, -81));
		throwLeftArmPos.Add(new AnimationState(0, 0.0f, -98));
		throwLeftArmPos.Add(new AnimationState(0, 0.0f, -115));
		throwLeftArmPos.Add(new AnimationState(0, 0.0f, -135));
		throwLeftArmPos.Add(new AnimationState(0, 0.0f, -158));
		throwLeftArmPos.Add(new AnimationState(0, 0.0f, -130));
		throwLeftArmPos.Add(new AnimationState(0, 0.0f, -100));
		throwLeftArmPos.Add(new AnimationState(0, 0.0f, -60));
		throwLeftArmPos.Add(new AnimationState(0, 0.0f, -40));
		leftArmInfo.Add (State.Throw, throwLeftArmPos);
		
		List<AnimationState> throwRightArmPos = new List<AnimationState>();
		throwRightArmPos.Add(new AnimationState(0, 0.0f, 0));
		throwRightArmPos.Add(new AnimationState(0, 0.0f, -5));
		throwRightArmPos.Add(new AnimationState(0, 0.0f, -11));
		throwRightArmPos.Add(new AnimationState(0, 0.0f, -23));
		throwRightArmPos.Add(new AnimationState(0, 0.0f, -36));
		throwRightArmPos.Add(new AnimationState(0, 0.0f, -50));
		throwRightArmPos.Add(new AnimationState(0, 0.0f, -65));
		throwRightArmPos.Add(new AnimationState(0, 0.0f, -81));
		throwRightArmPos.Add(new AnimationState(0, 0.0f, -98));
		throwRightArmPos.Add(new AnimationState(0, 0.0f, -115));
		throwRightArmPos.Add(new AnimationState(0, 0.0f, -135));
		throwRightArmPos.Add(new AnimationState(0, 0.0f, -158));
		throwRightArmPos.Add(new AnimationState(0, 0.0f, -130));
		throwRightArmPos.Add(new AnimationState(0, 0.0f, -100));
		throwRightArmPos.Add(new AnimationState(0, 0.0f, -60));
		throwRightArmPos.Add(new AnimationState(0, 0.0f, -40));
		rightArmInfo.Add (State.Throw, throwRightArmPos);
		
		List<AnimationState> throwLeftLegPos = new List<AnimationState>();
		throwLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
		leftLegInfo.Add(State.Throw, throwLeftLegPos);
		
		List<AnimationState> throwRightLegPos = new List<AnimationState>();
		throwRightLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwRightLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwRightLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwRightLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwRightLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwRightLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwRightLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwRightLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwRightLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwRightLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwRightLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwRightLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwRightLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwRightLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwRightLegPos.Add(new AnimationState(0, 0.0f, 0));
		throwRightLegPos.Add(new AnimationState(0, 0.0f, 0));
		rightLegInfo.Add(State.Throw, throwRightLegPos);
		
		List<AnimationState> throwBodyPos = new List<AnimationState>();
		throwBodyPos.Add(new AnimationState(0, 0.0f, 0));
		throwBodyPos.Add(new AnimationState(0, 0.0f, 0));
		throwBodyPos.Add(new AnimationState(0, 0.0f, 0));
		throwBodyPos.Add(new AnimationState(0, 0.0f, 0));
		throwBodyPos.Add(new AnimationState(0, 0.0f, 0));
		throwBodyPos.Add(new AnimationState(0, 0.0f, 0));
		throwBodyPos.Add(new AnimationState(0, 0.0f, 0));
		throwBodyPos.Add(new AnimationState(0, 0.0f, 0));
		throwBodyPos.Add(new AnimationState(0, 0.0f, 0));
		throwBodyPos.Add(new AnimationState(0, 0.0f, 0));
		throwBodyPos.Add(new AnimationState(0, 0.0f, 0));
		throwBodyPos.Add(new AnimationState(0, 0.0f, 0));
		throwBodyPos.Add(new AnimationState(0, 0.0f, 0));
		throwBodyPos.Add(new AnimationState(0, 0.0f, 0));
		throwBodyPos.Add(new AnimationState(0, 0.0f, 0));
		throwBodyPos.Add(new AnimationState(0, 0.0f, 0));
		bodyInfo.Add (State.Throw, throwBodyPos);
		#endregion

        #region startclimb
        //startclimb
        List<AnimationState> startClimbBasePos = new List<AnimationState>();
        startClimbBasePos.Add(new AnimationState(0, 0.0f, 0));
        startClimbBasePos.Add(new AnimationState(0, -0.02f, 0));
        startClimbBasePos.Add(new AnimationState(0, -0.05f, 0));
        startClimbBasePos.Add(new AnimationState(0, 0.01f, 0));
        startClimbBasePos.Add(new AnimationState(0, 0.0f, 0));
        //5
        startClimbBasePos.Add(new AnimationState(0, 0.03f, 0));
        startClimbBasePos.Add(new AnimationState(0, 0.09f, 1));
        startClimbBasePos.Add(new AnimationState(0, 0.2f, 3));
        startClimbBasePos.Add(new AnimationState(0.025f, 0.3f, 5));
        startClimbBasePos.Add(new AnimationState(0.06f, 0.42f, 7));
        //10
        startClimbBasePos.Add(new AnimationState(0.09f, 0.5f, 11));
        startClimbBasePos.Add(new AnimationState(0.13f, 0.65f, 17));
        startClimbBasePos.Add(new AnimationState(0.16f, 0.8f, 23));
        startClimbBasePos.Add(new AnimationState(0.20f, 0.9f, 27));
        startClimbBasePos.Add(new AnimationState(0.24f, 0.95f, 32, true));
        baseInfo.Add(State.StartClimb, startClimbBasePos);

        List<AnimationState> startClimbLeftArmPos = new List<AnimationState>();
        startClimbLeftArmPos.Add(new AnimationState(0, 0.0f, 0));
        startClimbLeftArmPos.Add(new AnimationState(0.0f, 0.0f, 3));
        startClimbLeftArmPos.Add(new AnimationState(0.0f, 0.0f, 7));
        startClimbLeftArmPos.Add(new AnimationState(0.0f, 0.0f, 1));
        startClimbLeftArmPos.Add(new AnimationState(0, 0.0f, 0));
        //5
        startClimbLeftArmPos.Add(new AnimationState(-0.01f, 0.00f, -3));
        startClimbLeftArmPos.Add(new AnimationState(-0.02f, 0.00f, -7));
        startClimbLeftArmPos.Add(new AnimationState(-0.03f, 0.00f, -10));
        startClimbLeftArmPos.Add(new AnimationState(-0.04f, 0.00f, -20));
        startClimbLeftArmPos.Add(new AnimationState(-0.06f, 0.00f, -32));
        //10
        startClimbLeftArmPos.Add(new AnimationState(-0.1f, 0.02f, -44));
        startClimbLeftArmPos.Add(new AnimationState(-0.12f, 0.04f, -60));
        startClimbLeftArmPos.Add(new AnimationState(-0.15f, 0.06f, -76));
        startClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.08f, -82));
        startClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -90));
        leftArmInfo.Add(State.StartClimb, startClimbLeftArmPos);

        List<AnimationState> startClimbRightArmPos = new List<AnimationState>();
        startClimbRightArmPos.Add(new AnimationState(0, 0.0f, 0));
        startClimbRightArmPos.Add(new AnimationState(0.0f, 0.0f, 3));
        startClimbRightArmPos.Add(new AnimationState(0.0f, 0.0f, 7));
        startClimbRightArmPos.Add(new AnimationState(0.0f, 0.0f, 1));
        startClimbRightArmPos.Add(new AnimationState(0, 0.0f, 0));
        //5
        startClimbRightArmPos.Add(new AnimationState(-0.01f, 0.0f, -3));
        startClimbRightArmPos.Add(new AnimationState(-0.02f, 0.0f, -7));
        startClimbRightArmPos.Add(new AnimationState(-0.03f, 0.0f, -10));
        startClimbRightArmPos.Add(new AnimationState(-0.04f, 0.0f, -20));
        startClimbRightArmPos.Add(new AnimationState(-0.06f, 0.0f, -32));
        //10
        startClimbRightArmPos.Add(new AnimationState(-0.1f, 0.02f, -50));
        startClimbRightArmPos.Add(new AnimationState(-0.12f, 0.04f, -68));
        startClimbRightArmPos.Add(new AnimationState(-0.15f, 0.06f, -82));
        startClimbRightArmPos.Add(new AnimationState(-0.2f, 0.08f, -90));
        startClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -90));
        rightArmInfo.Add(State.StartClimb, startClimbRightArmPos);

        List<AnimationState> startClimbLeftLegPos = new List<AnimationState>();
        startClimbLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
        startClimbLeftLegPos.Add(new AnimationState(0, 0.03f, 0));
        startClimbLeftLegPos.Add(new AnimationState(0, 0.06f, 0));
        startClimbLeftLegPos.Add(new AnimationState(0, 0.01f, 0));
        startClimbLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
        //5
        startClimbLeftLegPos.Add(new AnimationState(0, -0.03f, 0));
        startClimbLeftLegPos.Add(new AnimationState(0, -0.09f, 0));
        startClimbLeftLegPos.Add(new AnimationState(0, -0.16f, 0));
        startClimbLeftLegPos.Add(new AnimationState(0, -0.22f, 0));
        startClimbLeftLegPos.Add(new AnimationState(0, -0.25f, -5));
        //10
        startClimbLeftLegPos.Add(new AnimationState(0, -0.21f, -12));
        startClimbLeftLegPos.Add(new AnimationState(0, -0.14f, -25));
        startClimbLeftLegPos.Add(new AnimationState(0, -0.06f, -40));
        startClimbLeftLegPos.Add(new AnimationState(0, -0.03f, -44));
        startClimbLeftLegPos.Add(new AnimationState(0, -0.03f, -44));
        leftLegInfo.Add(State.StartClimb, startClimbLeftLegPos);

        List<AnimationState> startClimbRightLegPos = new List<AnimationState>();
        startClimbRightLegPos.Add(new AnimationState(0, 0.0f, 0));
        startClimbRightLegPos.Add(new AnimationState(0, 0.03f, 0));
        startClimbRightLegPos.Add(new AnimationState(0, 0.06f, 0));
        startClimbRightLegPos.Add(new AnimationState(0, 0.01f, 0));
        startClimbRightLegPos.Add(new AnimationState(0, 0.0f, 0));
        //5
        startClimbRightLegPos.Add(new AnimationState(0, -0.03f, 0));
        startClimbRightLegPos.Add(new AnimationState(0, -0.09f, 0));
        startClimbRightLegPos.Add(new AnimationState(0, -0.16f, 0));
        startClimbRightLegPos.Add(new AnimationState(0.05f, -0.22f, 0));
        startClimbRightLegPos.Add(new AnimationState(0.1f, -0.25f, -5));
        //10
        startClimbRightLegPos.Add(new AnimationState(0.2f, -0.21f, -12));
        startClimbRightLegPos.Add(new AnimationState(0.2f, -0.14f, -25));
        startClimbRightLegPos.Add(new AnimationState(0.2f, -0.06f, -40));
        startClimbRightLegPos.Add(new AnimationState(0.2f, -0.03f, -44));
        startClimbRightLegPos.Add(new AnimationState(0.2f, -0.03f, -44));
        rightLegInfo.Add(State.StartClimb, startClimbRightLegPos);

        List<AnimationState> startClimbBodyPos = new List<AnimationState>();
        startClimbBodyPos.Add(new AnimationState(0, 0.0f, 0));
        startClimbBodyPos.Add(new AnimationState(0, 0.0f, 1));
        startClimbBodyPos.Add(new AnimationState(0, 0.0f, 3));
        startClimbBodyPos.Add(new AnimationState(0, 0.0f, 1));
        startClimbBodyPos.Add(new AnimationState(0, 0.0f, 0));
        //5
        startClimbBodyPos.Add(new AnimationState(0, 0.00f, -1));
        startClimbBodyPos.Add(new AnimationState(0, 0.00f, -2));
        startClimbBodyPos.Add(new AnimationState(0, 0.00f, -3));
        startClimbBodyPos.Add(new AnimationState(0, 0.00f, -5));
        startClimbBodyPos.Add(new AnimationState(0, 0.00f, -7));
        //10
        startClimbBodyPos.Add(new AnimationState(0, 0.00f, -7));
        startClimbBodyPos.Add(new AnimationState(0, 0.00f, -5));
        startClimbBodyPos.Add(new AnimationState(0, 0.00f, -3));
        startClimbBodyPos.Add(new AnimationState(0, 0.00f, -2));
        startClimbBodyPos.Add(new AnimationState(0, 0.00f, -1));
        bodyInfo.Add(State.StartClimb, startClimbBodyPos);
        #endregion

        #region midclimb
        //midclimb
        List<AnimationState> midClimbBasePos = new List<AnimationState>();
        midClimbBasePos.Add(new AnimationState(0.24f, 0.95f, 33));
        midClimbBasePos.Add(new AnimationState(0.24f, 0.95f, 33));
        midClimbBasePos.Add(new AnimationState(0.24f, 0.95f, 33));
        midClimbBasePos.Add(new AnimationState(0.24f, 0.95f, 33));
        midClimbBasePos.Add(new AnimationState(0.24f, 0.95f, 33));
        midClimbBasePos.Add(new AnimationState(0.24f, 0.95f, 33));
        midClimbBasePos.Add(new AnimationState(0.24f, 0.95f, 33));
        midClimbBasePos.Add(new AnimationState(0.24f, 0.95f, 33));
        midClimbBasePos.Add(new AnimationState(0.24f, 0.95f, 33));
        midClimbBasePos.Add(new AnimationState(0.24f, 0.95f, 33));
        midClimbBasePos.Add(new AnimationState(0.24f, 0.95f, 33));
        midClimbBasePos.Add(new AnimationState(0.24f, 0.95f, 33));
        baseInfo.Add(State.MidClimb, midClimbBasePos);

        List<AnimationState> midClimbLeftArmPos = new List<AnimationState>();
        midClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -90));
        midClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -84));
        midClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -78));
        midClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -72));
        midClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -78));
        midClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -84));
        midClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -90));
        midClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -96));
        midClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -102));
        midClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -108));
        midClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -102));
        midClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -96));
        leftArmInfo.Add(State.MidClimb, midClimbLeftArmPos);

        List<AnimationState> midClimbRightArmPos = new List<AnimationState>();
        midClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -90));
        midClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -96));
        midClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -102));
        midClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -108));
        midClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -102));
        midClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -96));
        midClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -90));
        midClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -84));
        midClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -78));
        midClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -72));
        midClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -78));
        midClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -84));
        rightArmInfo.Add(State.MidClimb, midClimbRightArmPos);

        List<AnimationState> midClimbLeftLegPos = new List<AnimationState>();
        midClimbLeftLegPos.Add(new AnimationState(0, -0.03f, -44));
        midClimbLeftLegPos.Add(new AnimationState(0, -0.03f, -50));
        midClimbLeftLegPos.Add(new AnimationState(0, -0.03f, -56));
        midClimbLeftLegPos.Add(new AnimationState(0, -0.03f, -62));
        midClimbLeftLegPos.Add(new AnimationState(0, -0.03f, -56));
        midClimbLeftLegPos.Add(new AnimationState(0, -0.03f, -50));
        midClimbLeftLegPos.Add(new AnimationState(0, -0.03f, -44));
        midClimbLeftLegPos.Add(new AnimationState(0, -0.03f, -38));
        midClimbLeftLegPos.Add(new AnimationState(0, -0.03f, -32));
        midClimbLeftLegPos.Add(new AnimationState(0, -0.03f, -26));
        midClimbLeftLegPos.Add(new AnimationState(0, -0.03f, -32));
        midClimbLeftLegPos.Add(new AnimationState(0, -0.03f, -38));
        leftLegInfo.Add(State.MidClimb, midClimbLeftLegPos);

        List<AnimationState> midClimbRightLegPos = new List<AnimationState>();
        midClimbRightLegPos.Add(new AnimationState(0.2f, -0.03f, -44));
        midClimbRightLegPos.Add(new AnimationState(0.2f, -0.03f, -38));
        midClimbRightLegPos.Add(new AnimationState(0.2f, -0.03f, -32));
        midClimbRightLegPos.Add(new AnimationState(0.2f, -0.03f, -26));
        midClimbRightLegPos.Add(new AnimationState(0.2f, -0.03f, -32));
        midClimbRightLegPos.Add(new AnimationState(0.2f, -0.03f, -38));
        midClimbRightLegPos.Add(new AnimationState(0.2f, -0.03f, -44));
        midClimbRightLegPos.Add(new AnimationState(0.2f, -0.03f, -50));
        midClimbRightLegPos.Add(new AnimationState(0.2f, -0.03f, -56));
        midClimbRightLegPos.Add(new AnimationState(0.2f, -0.03f, -62));
        midClimbRightLegPos.Add(new AnimationState(0.2f, -0.03f, -56));
        midClimbRightLegPos.Add(new AnimationState(0.2f, -0.03f, -50));
        rightLegInfo.Add(State.MidClimb, midClimbRightLegPos);

        List<AnimationState> midClimbBodyPos = new List<AnimationState>();
        midClimbBodyPos.Add(new AnimationState(0, 0.00f, -1));
        midClimbBodyPos.Add(new AnimationState(0, 0.00f, -1));
        midClimbBodyPos.Add(new AnimationState(0, 0.00f, -1));
        midClimbBodyPos.Add(new AnimationState(0, 0.00f, -1));
        midClimbBodyPos.Add(new AnimationState(0, 0.00f, -1));
        midClimbBodyPos.Add(new AnimationState(0, 0.00f, -1));
        midClimbBodyPos.Add(new AnimationState(0, 0.00f, -1));
        midClimbBodyPos.Add(new AnimationState(0, 0.00f, -1));
        midClimbBodyPos.Add(new AnimationState(0, 0.00f, -1));
        midClimbBodyPos.Add(new AnimationState(0, 0.00f, -1));
        midClimbBodyPos.Add(new AnimationState(0, 0.00f, -1));
        midClimbBodyPos.Add(new AnimationState(0, 0.00f, -1));
        bodyInfo.Add(State.MidClimb, midClimbBodyPos);
        #endregion

        #region startclimbjump
        //startclimbjump
        List<AnimationState> startJumpClimbBasePos = new List<AnimationState>();
        startJumpClimbBasePos.Add(new AnimationState(0.24f, 0.95f, 33));
        startJumpClimbBasePos.Add(new AnimationState(0.24f, 0.95f, 38));
        startJumpClimbBasePos.Add(new AnimationState(0.24f, 0.95f, 43));
        startJumpClimbBasePos.Add(new AnimationState(0.24f, 0.95f, 48));
        startJumpClimbBasePos.Add(new AnimationState(0.24f, 0.95f, 53));
        startJumpClimbBasePos.Add(new AnimationState(0.24f, 0.95f, 58));
        startJumpClimbBasePos.Add(new AnimationState(0.24f, 0.95f, 63));
        startJumpClimbBasePos.Add(new AnimationState(0.24f, 0.95f, 68));
        startJumpClimbBasePos.Add(new AnimationState(0.24f, 0.95f, 68));
        startJumpClimbBasePos.Add(new AnimationState(0.24f, 0.95f, 68));
        startJumpClimbBasePos.Add(new AnimationState(0.24f, 0.95f, 73, true));
        baseInfo.Add(State.StartClimbJump, startJumpClimbBasePos);

        List<AnimationState> startJumpClimbLeftArmPos = new List<AnimationState>();
        startJumpClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -96));
        startJumpClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -111));
        startJumpClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -128));
        startJumpClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -145));
        startJumpClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -162));
        startJumpClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -179));
        startJumpClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -196));
        startJumpClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -213));
        startJumpClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -230));
        startJumpClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -250));
        startJumpClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -270));
        leftArmInfo.Add(State.StartClimbJump, startJumpClimbLeftArmPos);

        List<AnimationState> startJumpClimbRightArmPos = new List<AnimationState>();
        startJumpClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -84));
        startJumpClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -99));
        startJumpClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -116));
        startJumpClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -133));
        startJumpClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -150));
        startJumpClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -167));
        startJumpClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -184));
        startJumpClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -201));
        startJumpClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -218));
        startJumpClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -238));
        startJumpClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -258));
        rightArmInfo.Add(State.StartClimbJump, startJumpClimbRightArmPos);

        List<AnimationState> startJumpClimbLeftLegPos = new List<AnimationState>();
        startJumpClimbLeftLegPos.Add(new AnimationState(0, -0.03f, -38));
        startJumpClimbLeftLegPos.Add(new AnimationState(0, -0.03f, -20));
        startJumpClimbLeftLegPos.Add(new AnimationState(0, -0.03f, -2));
        startJumpClimbLeftLegPos.Add(new AnimationState(0, -0.03f, 16));
        startJumpClimbLeftLegPos.Add(new AnimationState(0, -0.03f, 30));
        startJumpClimbLeftLegPos.Add(new AnimationState(0, -0.03f, 48));
        startJumpClimbLeftLegPos.Add(new AnimationState(0, -0.03f, 66));
        startJumpClimbLeftLegPos.Add(new AnimationState(0, -0.03f, 82));
        startJumpClimbLeftLegPos.Add(new AnimationState(0, -0.03f, 100));
        startJumpClimbLeftLegPos.Add(new AnimationState(0, -0.03f, 100));
        startJumpClimbLeftLegPos.Add(new AnimationState(0, -0.03f, 100));
        leftLegInfo.Add(State.StartClimbJump, startJumpClimbLeftLegPos);

        List<AnimationState> startJumpClimbRightLegPos = new List<AnimationState>();
        startJumpClimbRightLegPos.Add(new AnimationState(0.2f, -0.03f, -50));
        startJumpClimbRightLegPos.Add(new AnimationState(0.2f, -0.03f, -32));
        startJumpClimbRightLegPos.Add(new AnimationState(0.2f, -0.03f, -14));
        startJumpClimbRightLegPos.Add(new AnimationState(0.2f, -0.03f, 4));
        startJumpClimbRightLegPos.Add(new AnimationState(0.2f, -0.03f, 22));
        startJumpClimbRightLegPos.Add(new AnimationState(0.2f, -0.03f, 40));
        startJumpClimbRightLegPos.Add(new AnimationState(0.2f, -0.03f, 58));
        startJumpClimbRightLegPos.Add(new AnimationState(0.2f, -0.03f, 76));
        startJumpClimbRightLegPos.Add(new AnimationState(0.2f, -0.03f, 90));
        startJumpClimbRightLegPos.Add(new AnimationState(0.2f, -0.03f, 90));
        startJumpClimbRightLegPos.Add(new AnimationState(0.2f, -0.03f, 90));
        rightLegInfo.Add(State.StartClimbJump, startJumpClimbRightLegPos);

        List<AnimationState> startJumpClimbBodyPos = new List<AnimationState>();
        startJumpClimbBodyPos.Add(new AnimationState(0, 0.00f, 22, false, -1));
        startJumpClimbBodyPos.Add(new AnimationState(0, 0.00f, 22, false, -1));
        startJumpClimbBodyPos.Add(new AnimationState(0, 0.00f, 22, false, -1));
        startJumpClimbBodyPos.Add(new AnimationState(0, 0.00f, 22, false, -1));
        startJumpClimbBodyPos.Add(new AnimationState(0, 0.00f, 22, false, -1));
        startJumpClimbBodyPos.Add(new AnimationState(0, 0.00f, 22, false, -1));
        startJumpClimbBodyPos.Add(new AnimationState(0, 0.00f, 22, false, -1));
        startJumpClimbBodyPos.Add(new AnimationState(0, 0.00f, 22, false, -1));
        startJumpClimbBodyPos.Add(new AnimationState(0, 0.00f, 22, false, -1));
        startJumpClimbBodyPos.Add(new AnimationState(0, 0.00f, 22, false, -1));
        startJumpClimbBodyPos.Add(new AnimationState(0, 0.00f, 22, false, -1));
        bodyInfo.Add(State.StartClimbJump, startJumpClimbBodyPos);
        #endregion

        #region midclimbjump
        //midclimbjump
        List<AnimationState> midJumpClimbBasePos = new List<AnimationState>();
        midJumpClimbBasePos.Add(new AnimationState(-0.12f, 0.63f, 50f));
        baseInfo.Add(State.MidClimbJump, midJumpClimbBasePos);

        List<AnimationState> midJumpClimbLeftArmPos = new List<AnimationState>();
        midJumpClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -96));
        leftArmInfo.Add(State.MidClimbJump, midJumpClimbLeftArmPos);

        List<AnimationState> midJumpClimbRightArmPos = new List<AnimationState>();
        midJumpClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -84));
        rightArmInfo.Add(State.MidClimbJump, midJumpClimbRightArmPos);

        List<AnimationState> midJumpClimbLeftLegPos = new List<AnimationState>();
        midJumpClimbLeftLegPos.Add(new AnimationState(-0.1f, -0.03f, -38));
        leftLegInfo.Add(State.MidClimbJump, midJumpClimbLeftLegPos);

        List<AnimationState> midJumpClimbRightLegPos = new List<AnimationState>();
        midJumpClimbRightLegPos.Add(new AnimationState(0.3f, -0.03f, -50));
        rightLegInfo.Add(State.MidClimbJump, midJumpClimbRightLegPos);

        List<AnimationState> midJumpClimbBodyPos = new List<AnimationState>();
        midJumpClimbBodyPos.Add(new AnimationState(0, 0.00f, 0));
        bodyInfo.Add(State.MidClimbJump, midJumpClimbBodyPos);
        #endregion

        #region endclimbjump
        //endclimbjump
        List<AnimationState> endJumpClimbBasePos = new List<AnimationState>();
        endJumpClimbBasePos.Add(new AnimationState(-0.12f, 0.63f, 50f));
        endJumpClimbBasePos.Add(new AnimationState(-0.08f, 0.43f, 50f));
        endJumpClimbBasePos.Add(new AnimationState(-0.04f, 0.23f, 50f));
        endJumpClimbBasePos.Add(new AnimationState(0.00f, 0.0f, 50f));
        endJumpClimbBasePos.Add(new AnimationState(0.0f, 0.0f, 45f, true));
        endJumpClimbBasePos.Add(new AnimationState(0.0f, 0.0f, 35f));
        endJumpClimbBasePos.Add(new AnimationState(0.0f, 0.0f, 25f));
        endJumpClimbBasePos.Add(new AnimationState(0.0f, 0.0f, 15f));
        endJumpClimbBasePos.Add(new AnimationState(0.0f, 0.0f, 5f));
        endJumpClimbBasePos.Add(new AnimationState(0.0f, 0.0f, 0));
        endJumpClimbBasePos.Add(new AnimationState(0.0f, 0.0f, 0, true));
        baseInfo.Add(State.EndClimbJump, endJumpClimbBasePos);

        List<AnimationState> endJumpClimbLeftArmPos = new List<AnimationState>();
        endJumpClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -96));
        endJumpClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -96));
        endJumpClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -96));
        endJumpClimbLeftArmPos.Add(new AnimationState(-0.2f, 0.1f, -96));
        endJumpClimbLeftArmPos.Add(new AnimationState(-0.175f, 0.075f, -86));
        endJumpClimbLeftArmPos.Add(new AnimationState(-0.125f, 0.045f, -71));
        endJumpClimbLeftArmPos.Add(new AnimationState(-0.125f, 0.045f, -51));
        endJumpClimbLeftArmPos.Add(new AnimationState(-0.09f, 0.025f, -31));
        endJumpClimbLeftArmPos.Add(new AnimationState(-0.05f, 0f, -11));
        endJumpClimbLeftArmPos.Add(new AnimationState(0.0f, 0f, 0));
        endJumpClimbLeftArmPos.Add(new AnimationState(0.0f, 0f, 0));
        leftArmInfo.Add(State.EndClimbJump, endJumpClimbLeftArmPos);

        List<AnimationState> endJumpClimbRightArmPos = new List<AnimationState>();
        endJumpClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -84));
        endJumpClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -84));
        endJumpClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -84));
        endJumpClimbRightArmPos.Add(new AnimationState(-0.2f, 0.1f, -84));
        endJumpClimbRightArmPos.Add(new AnimationState(-0.175f, 0.075f, -74));
        endJumpClimbRightArmPos.Add(new AnimationState(-0.125f, 0.045f, -59));
        endJumpClimbRightArmPos.Add(new AnimationState(-0.125f, 0.045f, -39));
        endJumpClimbRightArmPos.Add(new AnimationState(-0.09f, 0.025f, -19));
        endJumpClimbRightArmPos.Add(new AnimationState(-0.03f, 0f, 0));
        endJumpClimbRightArmPos.Add(new AnimationState(0.0f, 0f, 0));
        endJumpClimbRightArmPos.Add(new AnimationState(0.0f, 0f, 0));
        rightArmInfo.Add(State.EndClimbJump, endJumpClimbRightArmPos);

        List<AnimationState> endJumpClimbLeftLegPos = new List<AnimationState>();
        endJumpClimbLeftLegPos.Add(new AnimationState(-0.1f, -0.03f, -38));
        endJumpClimbLeftLegPos.Add(new AnimationState(-0.1f, -0.03f, -38));
        endJumpClimbLeftLegPos.Add(new AnimationState(-0.1f, -0.03f, -38));
        endJumpClimbLeftLegPos.Add(new AnimationState(-0.1f, -0.03f, -38));
        endJumpClimbLeftLegPos.Add(new AnimationState(-0.075f, 0f, -33));
        endJumpClimbLeftLegPos.Add(new AnimationState(-0.05f, 0f, -28));
        endJumpClimbLeftLegPos.Add(new AnimationState(-0.025f, 0f, -23));
        endJumpClimbLeftLegPos.Add(new AnimationState(0f, 0f, -18));
        endJumpClimbLeftLegPos.Add(new AnimationState(0f, 0f, -10));
        endJumpClimbLeftLegPos.Add(new AnimationState(0f, 0f, 0));
        endJumpClimbLeftLegPos.Add(new AnimationState(0f, 0f, 0));
        leftLegInfo.Add(State.EndClimbJump, endJumpClimbLeftLegPos);

        List<AnimationState> endJumpClimbRightLegPos = new List<AnimationState>();
        endJumpClimbRightLegPos.Add(new AnimationState(0.3f, -0.03f, -50));
        endJumpClimbRightLegPos.Add(new AnimationState(0.3f, -0.03f, -50));
        endJumpClimbRightLegPos.Add(new AnimationState(0.3f, -0.03f, -50));
        endJumpClimbRightLegPos.Add(new AnimationState(0.3f, -0.03f, -50));
        endJumpClimbRightLegPos.Add(new AnimationState(0.275f, 0f, -45));
        endJumpClimbRightLegPos.Add(new AnimationState(0.25f, 0f, -40));
        endJumpClimbRightLegPos.Add(new AnimationState(0.21f, 0f, -35));
        endJumpClimbRightLegPos.Add(new AnimationState(0.16f, 0f, -25));
        endJumpClimbRightLegPos.Add(new AnimationState(0.10f, 0f, -15));
        endJumpClimbRightLegPos.Add(new AnimationState(0.05f, 0f, 0));
        endJumpClimbRightLegPos.Add(new AnimationState(0.0f, 0f, 0));
        rightLegInfo.Add(State.EndClimbJump, endJumpClimbRightLegPos);

        List<AnimationState> endJumpClimbBodyPos = new List<AnimationState>();
        endJumpClimbBodyPos.Add(new AnimationState(0, 0.00f, 0));
        endJumpClimbBodyPos.Add(new AnimationState(0, 0.00f, 0));
        endJumpClimbBodyPos.Add(new AnimationState(0, 0.00f, 0));
        endJumpClimbBodyPos.Add(new AnimationState(0, 0.00f, 0));
        endJumpClimbBodyPos.Add(new AnimationState(0, 0.00f, 0));
        endJumpClimbBodyPos.Add(new AnimationState(0, 0.00f, 0));
        endJumpClimbBodyPos.Add(new AnimationState(0, 0.00f, 0));
        endJumpClimbBodyPos.Add(new AnimationState(0, 0.00f, 0));
        endJumpClimbBodyPos.Add(new AnimationState(0, 0.00f, 0));
        endJumpClimbBodyPos.Add(new AnimationState(0, 0.00f, 0));
        endJumpClimbBodyPos.Add(new AnimationState(0, 0.00f, 0));
        bodyInfo.Add(State.EndClimbJump, endJumpClimbBodyPos);
        #endregion

        #region repositionjump
        //repositionjump
        List<AnimationState> repositionJumpBasePos = new List<AnimationState>();
        repositionJumpBasePos.Add(new AnimationState(0, 0.0f, 0));
        repositionJumpBasePos.Add(new AnimationState(0, -0.02f, 0));
        repositionJumpBasePos.Add(new AnimationState(0, -0.05f, 0));
        repositionJumpBasePos.Add(new AnimationState(0, 0.01f, 0));
        repositionJumpBasePos.Add(new AnimationState(0, 0.0f, 0));
        //5
        repositionJumpBasePos.Add(new AnimationState(0, 0.03f, 1));
        repositionJumpBasePos.Add(new AnimationState(0, 0.09f, 2));
        repositionJumpBasePos.Add(new AnimationState(0, 0.2f, 2));
        repositionJumpBasePos.Add(new AnimationState(0, 0.3f, 2));
        repositionJumpBasePos.Add(new AnimationState(0, 0.42f, 3, true));
        baseInfo.Add(State.RepositionJump, repositionJumpBasePos);

        List<AnimationState> repositionJumpLeftArmPos = new List<AnimationState>();
        repositionJumpLeftArmPos.Add(new AnimationState(0, 0.0f, 0));
        repositionJumpLeftArmPos.Add(new AnimationState(0.0f, 0.0f, 3));
        repositionJumpLeftArmPos.Add(new AnimationState(0.0f, 0.0f, 7));
        repositionJumpLeftArmPos.Add(new AnimationState(0.0f, 0.0f, 1));
        repositionJumpLeftArmPos.Add(new AnimationState(0, 0.0f, 0));
        //5
        repositionJumpLeftArmPos.Add(new AnimationState(-0.01f, 0.00f, -3));
        repositionJumpLeftArmPos.Add(new AnimationState(-0.02f, 0.00f, -7));
        repositionJumpLeftArmPos.Add(new AnimationState(-0.03f, 0.00f, -10));
        repositionJumpLeftArmPos.Add(new AnimationState(-0.04f, 0.00f, -20));
        repositionJumpLeftArmPos.Add(new AnimationState(-0.06f, 0.00f, -32));
        leftArmInfo.Add(State.RepositionJump, repositionJumpLeftArmPos);

        List<AnimationState> repositionJumpRightArmPos = new List<AnimationState>();
        repositionJumpRightArmPos.Add(new AnimationState(0, 0.0f, 0));
        repositionJumpRightArmPos.Add(new AnimationState(0.0f, 0.0f, 3));
        repositionJumpRightArmPos.Add(new AnimationState(0.0f, 0.0f, 7));
        repositionJumpRightArmPos.Add(new AnimationState(0.0f, 0.0f, 1));
        repositionJumpRightArmPos.Add(new AnimationState(0, 0.0f, 0));
        //5
        repositionJumpRightArmPos.Add(new AnimationState(-0.01f, 0.0f, -3));
        repositionJumpRightArmPos.Add(new AnimationState(-0.02f, 0.0f, -7));
        repositionJumpRightArmPos.Add(new AnimationState(-0.03f, 0.0f, -10));
        repositionJumpRightArmPos.Add(new AnimationState(-0.04f, 0.0f, -20));
        repositionJumpRightArmPos.Add(new AnimationState(-0.06f, 0.0f, -32));
        rightArmInfo.Add(State.RepositionJump, repositionJumpRightArmPos);

        List<AnimationState> repositionJumpLeftLegPos = new List<AnimationState>();
        repositionJumpLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
        repositionJumpLeftLegPos.Add(new AnimationState(0, 0.03f, 0));
        repositionJumpLeftLegPos.Add(new AnimationState(0, 0.06f, 0));
        repositionJumpLeftLegPos.Add(new AnimationState(0, 0.01f, 0));
        repositionJumpLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
        //5
        repositionJumpLeftLegPos.Add(new AnimationState(0, -0.03f, 0));
        repositionJumpLeftLegPos.Add(new AnimationState(0, -0.09f, 0));
        repositionJumpLeftLegPos.Add(new AnimationState(0, -0.16f, 0));
        repositionJumpLeftLegPos.Add(new AnimationState(0, -0.22f, 0));
        repositionJumpLeftLegPos.Add(new AnimationState(0, -0.25f, 0));
        leftLegInfo.Add(State.RepositionJump, repositionJumpLeftLegPos);

        List<AnimationState> repositionJumpRightLegPos = new List<AnimationState>();
        repositionJumpRightLegPos.Add(new AnimationState(0, 0.0f, 0));
        repositionJumpRightLegPos.Add(new AnimationState(0, 0.03f, 0));
        repositionJumpRightLegPos.Add(new AnimationState(0, 0.06f, 0));
        repositionJumpRightLegPos.Add(new AnimationState(0, 0.01f, 0));
        repositionJumpRightLegPos.Add(new AnimationState(0, 0.0f, 0));
        //5
        repositionJumpRightLegPos.Add(new AnimationState(0, -0.03f, 0));
        repositionJumpRightLegPos.Add(new AnimationState(0, -0.09f, 0));
        repositionJumpRightLegPos.Add(new AnimationState(0, -0.16f, 0));
        repositionJumpRightLegPos.Add(new AnimationState(0, -0.22f, 0));
        repositionJumpRightLegPos.Add(new AnimationState(0, -0.25f, 0));
        rightLegInfo.Add(State.RepositionJump, repositionJumpRightLegPos);

        List<AnimationState> repositionJumpBodyPos = new List<AnimationState>();
        repositionJumpBodyPos.Add(new AnimationState(0, 0.0f, 0));
        repositionJumpBodyPos.Add(new AnimationState(0, 0.0f, 1));
        repositionJumpBodyPos.Add(new AnimationState(0, 0.0f, 3));
        repositionJumpBodyPos.Add(new AnimationState(0, 0.0f, 1));
        repositionJumpBodyPos.Add(new AnimationState(0, 0.0f, 0));
        //5
        repositionJumpBodyPos.Add(new AnimationState(0, 0.00f, -1));
        repositionJumpBodyPos.Add(new AnimationState(0, 0.00f, -2));
        repositionJumpBodyPos.Add(new AnimationState(0, 0.00f, -3));
        repositionJumpBodyPos.Add(new AnimationState(0, 0.00f, -5));
        repositionJumpBodyPos.Add(new AnimationState(0, 0.00f, -7));
        bodyInfo.Add(State.RepositionJump, repositionJumpBodyPos);
        #endregion

        #region repositionwalk
        //repositionwalk
        List<AnimationState> repositionWalkBasePos = new List<AnimationState>();
        repositionWalkBasePos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkBasePos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkBasePos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkBasePos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkBasePos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkBasePos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkBasePos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkBasePos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkBasePos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkBasePos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkBasePos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkBasePos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        baseInfo.Add(State.RepositionWalk, repositionWalkBasePos);

        List<AnimationState> repositionWalkLeftArmPos = new List<AnimationState>();
        repositionWalkLeftArmPos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkLeftArmPos.Add(new AnimationState(0.0f, 0.0f, 5.0f));
        repositionWalkLeftArmPos.Add(new AnimationState(0.0f, 0.0f, 10.0f));
        repositionWalkLeftArmPos.Add(new AnimationState(0.0f, 0.0f, 15.0f));
        repositionWalkLeftArmPos.Add(new AnimationState(0.0f, 0.0f, 20.0f));
        repositionWalkLeftArmPos.Add(new AnimationState(0.0f, 0.0f, 15.0f));
        repositionWalkLeftArmPos.Add(new AnimationState(0.0f, 0.0f, 10.0f));
        repositionWalkLeftArmPos.Add(new AnimationState(0.0f, 0.0f, 5.0f));
        repositionWalkLeftArmPos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkLeftArmPos.Add(new AnimationState(0.0f, 0.0f, -5.0f));
        repositionWalkLeftArmPos.Add(new AnimationState(0.0f, 0.0f, -10.0f));
        repositionWalkLeftArmPos.Add(new AnimationState(0.0f, 0.0f, -5.0f));
        leftArmInfo.Add(State.RepositionWalk, repositionWalkLeftArmPos);

        List<AnimationState> repositionWalkRightArmPos = new List<AnimationState>();
        repositionWalkRightArmPos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkRightArmPos.Add(new AnimationState(0.0f, 0.0f, -5.0f));
        repositionWalkRightArmPos.Add(new AnimationState(0.0f, 0.0f, -10.0f));
        repositionWalkRightArmPos.Add(new AnimationState(0.0f, 0.0f, -15.0f));
        repositionWalkRightArmPos.Add(new AnimationState(0.0f, 0.0f, -20.0f));
        repositionWalkRightArmPos.Add(new AnimationState(0.0f, 0.0f, -15.0f));
        repositionWalkRightArmPos.Add(new AnimationState(0.0f, 0.0f, -10.0f));
        repositionWalkRightArmPos.Add(new AnimationState(0.0f, 0.0f, -5.0f));
        repositionWalkRightArmPos.Add(new AnimationState(0.0f, 0.0f, -0.0f));
        repositionWalkRightArmPos.Add(new AnimationState(0.0f, 0.0f, 5.0f));
        repositionWalkRightArmPos.Add(new AnimationState(0.0f, 0.0f, 10.0f));
        repositionWalkRightArmPos.Add(new AnimationState(0.0f, 0.0f, 5.0f));
        rightArmInfo.Add(State.RepositionWalk, repositionWalkRightArmPos);

        List<AnimationState> repositionWalkLeftLegPos = new List<AnimationState>();
        repositionWalkLeftLegPos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkLeftLegPos.Add(new AnimationState(0.0f, 0.0f, -5.0f));
        repositionWalkLeftLegPos.Add(new AnimationState(0.0f, 0.0f, -10.0f));
        repositionWalkLeftLegPos.Add(new AnimationState(0.0f, 0.0f, -5.0f));
        repositionWalkLeftLegPos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkLeftLegPos.Add(new AnimationState(0.0f, 0.0f, 5.0f));
        repositionWalkLeftLegPos.Add(new AnimationState(0.0f, 0.0f, 10.0f));
        repositionWalkLeftLegPos.Add(new AnimationState(0.0f, 0.0f, 15.0f));
        repositionWalkLeftLegPos.Add(new AnimationState(0.0f, 0.0f, 20.0f));
        repositionWalkLeftLegPos.Add(new AnimationState(0.0f, 0.0f, 15.0f));
        repositionWalkLeftLegPos.Add(new AnimationState(0.0f, 0.0f, 10.0f));
        repositionWalkLeftLegPos.Add(new AnimationState(0.0f, 0.0f, 5.0f));
        leftLegInfo.Add(State.RepositionWalk, repositionWalkLeftLegPos);

        List<AnimationState> repositionWalkRightLegPos = new List<AnimationState>();
        repositionWalkRightLegPos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkRightLegPos.Add(new AnimationState(0.0f, 0.0f, 5.0f));
        repositionWalkRightLegPos.Add(new AnimationState(0.0f, 0.0f, 10.0f));
        repositionWalkRightLegPos.Add(new AnimationState(0.0f, 0.0f, 15.0f));
        repositionWalkRightLegPos.Add(new AnimationState(0.0f, 0.0f, 20.0f));
        repositionWalkRightLegPos.Add(new AnimationState(0.0f, 0.0f, 15.0f));
        repositionWalkRightLegPos.Add(new AnimationState(0.0f, 0.0f, 10.0f));
        repositionWalkRightLegPos.Add(new AnimationState(0.0f, 0.0f, 5.0f));
        repositionWalkRightLegPos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkRightLegPos.Add(new AnimationState(0.0f, 0.0f, -5.0f));
        repositionWalkRightLegPos.Add(new AnimationState(0.0f, 0.0f, -10.0f));
        repositionWalkRightLegPos.Add(new AnimationState(0.0f, 0.0f, -5.0f));
        rightLegInfo.Add(State.RepositionWalk, repositionWalkRightLegPos);

        List<AnimationState> repositionWalkBodyPos = new List<AnimationState>();
        repositionWalkBodyPos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkBodyPos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkBodyPos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkBodyPos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkBodyPos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkBodyPos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkBodyPos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkBodyPos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkBodyPos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkBodyPos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkBodyPos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkBodyPos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        repositionWalkBodyPos.Add(new AnimationState(0.0f, 0.0f, 0.0f));
        bodyInfo.Add(State.RepositionWalk, repositionWalkBodyPos);
        #endregion

        #region sleeping
        //idle
        List<AnimationState> sleepBasePos = new List<AnimationState>();
        sleepBasePos.Add(new AnimationState(0, 0.0f, 100));
        sleepBasePos.Add(new AnimationState(0, 0.0f, 100));
        sleepBasePos.Add(new AnimationState(0, 0.0f, 100));
        sleepBasePos.Add(new AnimationState(0, 0.0f, 100));
        baseInfo.Add(State.Sleeping, sleepBasePos);

        List<AnimationState> sleepLeftArmPos = new List<AnimationState>();
        sleepLeftArmPos.Add(new AnimationState(0, 0.0f, 0));
        sleepLeftArmPos.Add(new AnimationState(0, 0.0f, 0));
        sleepLeftArmPos.Add(new AnimationState(0, 0.0f, 0));
        sleepLeftArmPos.Add(new AnimationState(0, 0.0f, 0));
        leftArmInfo.Add(State.Sleeping, sleepLeftArmPos);

        List<AnimationState> sleepRightArmPos = new List<AnimationState>();
        sleepRightArmPos.Add(new AnimationState(0, 0.0f, 0));
        sleepRightArmPos.Add(new AnimationState(0, 0.0f, 0));
        sleepRightArmPos.Add(new AnimationState(0, 0.0f, 0));
        sleepRightArmPos.Add(new AnimationState(0, 0.0f, 0));
        rightArmInfo.Add(State.Sleeping, sleepRightArmPos);

        List<AnimationState> sleepLeftLegPos = new List<AnimationState>();
        sleepLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
        sleepLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
        sleepLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
        sleepLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
        leftLegInfo.Add(State.Sleeping, sleepLeftLegPos);

        List<AnimationState> sleepRightLegPos = new List<AnimationState>();
        sleepRightLegPos.Add(new AnimationState(0, 0.0f, 0));
        sleepRightLegPos.Add(new AnimationState(0, 0.0f, 0));
        sleepRightLegPos.Add(new AnimationState(0, 0.0f, 0));
        sleepRightLegPos.Add(new AnimationState(0, 0.0f, 0));
        rightLegInfo.Add(State.Sleeping, sleepRightLegPos);

        List<AnimationState> sleepBodyPos = new List<AnimationState>();
        sleepBodyPos.Add(new AnimationState(0, 0.0f, 0));
        sleepBodyPos.Add(new AnimationState(0, 0.0f, 2));
        sleepBodyPos.Add(new AnimationState(0, 0.0f, 4));
        sleepBodyPos.Add(new AnimationState(0, 0.0f, 2));
        bodyInfo.Add(State.Sleeping, sleepBodyPos);

        #endregion

        #region sleepingstandingup
        //idle
        List<AnimationState> sleepStandingUpBasePos = new List<AnimationState>();
        sleepStandingUpBasePos.Add(new AnimationState(0, 0.0f, 100));
        sleepStandingUpBasePos.Add(new AnimationState(0, 0.0f, 95));
        sleepStandingUpBasePos.Add(new AnimationState(0, 0.0f, 90));
        sleepStandingUpBasePos.Add(new AnimationState(0, 0.0f, 85));
        sleepStandingUpBasePos.Add(new AnimationState(0, 0.0f, 75));
        sleepStandingUpBasePos.Add(new AnimationState(0, 0.0f, 65));
        sleepStandingUpBasePos.Add(new AnimationState(0, 0.0f, 55));
        sleepStandingUpBasePos.Add(new AnimationState(0, 0.0f, 45));
        sleepStandingUpBasePos.Add(new AnimationState(0, 0.0f, 35));
        sleepStandingUpBasePos.Add(new AnimationState(0, 0.0f, 25));
        sleepStandingUpBasePos.Add(new AnimationState(0, 0.0f, 15));
        sleepStandingUpBasePos.Add(new AnimationState(0, 0.0f, 5));
        sleepStandingUpBasePos.Add(new AnimationState(0, 0.0f, 0));
        sleepStandingUpBasePos.Add(new AnimationState(0, 0.0f, 0, true));
        baseInfo.Add(State.SleepingStandingUp, sleepStandingUpBasePos);

        List<AnimationState> sleepStandingUpLeftArmPos = new List<AnimationState>();
        sleepStandingUpLeftArmPos.Add(new AnimationState(0, 0.0f, 0));
        sleepStandingUpLeftArmPos.Add(new AnimationState(-0.03f, 0.0f, -4));
        sleepStandingUpLeftArmPos.Add(new AnimationState(-0.06f, 0.0f, -8));
        sleepStandingUpLeftArmPos.Add(new AnimationState(-0.1f, 0.0f, -12));
        sleepStandingUpLeftArmPos.Add(new AnimationState(-0.1f, 0.0f, -12));
        sleepStandingUpLeftArmPos.Add(new AnimationState(-0.1f, 0.0f, -12));
        sleepStandingUpLeftArmPos.Add(new AnimationState(-0.1f, 0.0f, -12));
        sleepStandingUpLeftArmPos.Add(new AnimationState(-0.06f, 0.0f, -10));
        sleepStandingUpLeftArmPos.Add(new AnimationState(-0.03f, 0.0f, -6));
        sleepStandingUpLeftArmPos.Add(new AnimationState(0, 0.0f, 0));
        sleepStandingUpLeftArmPos.Add(new AnimationState(0, 0.0f, 0));
        sleepStandingUpLeftArmPos.Add(new AnimationState(0, 0.0f, 0));
        sleepStandingUpLeftArmPos.Add(new AnimationState(0, 0.0f, 0));
        sleepStandingUpLeftArmPos.Add(new AnimationState(0, 0.0f, 0));
        leftArmInfo.Add(State.SleepingStandingUp, sleepStandingUpLeftArmPos);

        List<AnimationState> sleepStandingUpRightArmPos = new List<AnimationState>();
        sleepStandingUpRightArmPos.Add(new AnimationState(0, 0.0f, 0));
        sleepStandingUpRightArmPos.Add(new AnimationState(0.03f, 0.1f, -4));
        sleepStandingUpRightArmPos.Add(new AnimationState(0.06f, 0.2f, -8));
        sleepStandingUpRightArmPos.Add(new AnimationState(0.1f, 0.3f, -12));
        sleepStandingUpRightArmPos.Add(new AnimationState(0.1f, 0.0f, -12));
        sleepStandingUpRightArmPos.Add(new AnimationState(0.1f, 0.0f, -12));
        sleepStandingUpRightArmPos.Add(new AnimationState(0.1f, 0.0f, -12));
        sleepStandingUpRightArmPos.Add(new AnimationState(0.06f, 0.0f, -10));
        sleepStandingUpRightArmPos.Add(new AnimationState(0.03f, 0.0f, -6));
        sleepStandingUpRightArmPos.Add(new AnimationState(0, 0.0f, 0));
        sleepStandingUpRightArmPos.Add(new AnimationState(0, 0.0f, 0));
        sleepStandingUpRightArmPos.Add(new AnimationState(0, 0.0f, 0));
        sleepStandingUpRightArmPos.Add(new AnimationState(0, 0.0f, 0));
        sleepStandingUpRightArmPos.Add(new AnimationState(0, 0.0f, 0));
        rightArmInfo.Add(State.SleepingStandingUp, sleepStandingUpRightArmPos);

        List<AnimationState> sleepStandingUpLeftLegPos = new List<AnimationState>();
        sleepStandingUpLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
        sleepStandingUpLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
        sleepStandingUpLeftLegPos.Add(new AnimationState(0, 0.0f, -4));
        sleepStandingUpLeftLegPos.Add(new AnimationState(0, 0.0f, -8));
        sleepStandingUpLeftLegPos.Add(new AnimationState(0, 0.0f, -12));
        sleepStandingUpLeftLegPos.Add(new AnimationState(0, 0.0f, -16));
        sleepStandingUpLeftLegPos.Add(new AnimationState(0, 0.0f, -24));
        sleepStandingUpLeftLegPos.Add(new AnimationState(0, 0.0f, -32));
        sleepStandingUpLeftLegPos.Add(new AnimationState(0, 0.0f, -22));
        sleepStandingUpLeftLegPos.Add(new AnimationState(0, 0.0f, -12));
        sleepStandingUpLeftLegPos.Add(new AnimationState(0, 0.0f, -2));
        sleepStandingUpLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
        sleepStandingUpLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
        sleepStandingUpLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
        leftLegInfo.Add(State.SleepingStandingUp, sleepStandingUpLeftLegPos);

        List<AnimationState> sleepStandingUpRightLegPos = new List<AnimationState>();
        sleepStandingUpRightLegPos.Add(new AnimationState(0, 0.0f, 0));
        sleepStandingUpRightLegPos.Add(new AnimationState(0, 0.0f, 0));
        sleepStandingUpRightLegPos.Add(new AnimationState(0, 0.0f, -4));
        sleepStandingUpRightLegPos.Add(new AnimationState(0, 0.0f, -8));
        sleepStandingUpRightLegPos.Add(new AnimationState(0, 0.0f, -12));
        sleepStandingUpRightLegPos.Add(new AnimationState(0, 0.0f, -16));
        sleepStandingUpRightLegPos.Add(new AnimationState(0, 0.0f, -24));
        sleepStandingUpRightLegPos.Add(new AnimationState(0, 0.0f, -32));
        sleepStandingUpRightLegPos.Add(new AnimationState(0, 0.0f, -22));
        sleepStandingUpRightLegPos.Add(new AnimationState(0, 0.0f, -12));
        sleepStandingUpRightLegPos.Add(new AnimationState(0, 0.0f, -2));
        sleepStandingUpRightLegPos.Add(new AnimationState(0, 0.0f, 0));
        sleepStandingUpRightLegPos.Add(new AnimationState(0, 0.0f, 0));
        sleepStandingUpRightLegPos.Add(new AnimationState(0, 0.0f, 0));
        rightLegInfo.Add(State.SleepingStandingUp, sleepStandingUpRightLegPos);

        List<AnimationState> sleepStandingUpBodyPos = new List<AnimationState>();
        sleepStandingUpBodyPos.Add(new AnimationState(0, 0.0f, 0));
        sleepStandingUpBodyPos.Add(new AnimationState(0, 0.0f, 5));
        sleepStandingUpBodyPos.Add(new AnimationState(0, 0.0f, 10));
        sleepStandingUpBodyPos.Add(new AnimationState(0, 0.0f, 15));
        sleepStandingUpBodyPos.Add(new AnimationState(0, 0.0f, 20));
        sleepStandingUpBodyPos.Add(new AnimationState(0, 0.0f, 25));
        sleepStandingUpBodyPos.Add(new AnimationState(0, 0.0f, 30));
        sleepStandingUpBodyPos.Add(new AnimationState(0, 0.0f, 35));
        sleepStandingUpBodyPos.Add(new AnimationState(0, 0.0f, 30));
        sleepStandingUpBodyPos.Add(new AnimationState(0, 0.0f, 25));
        sleepStandingUpBodyPos.Add(new AnimationState(0, 0.0f, 20));
        sleepStandingUpBodyPos.Add(new AnimationState(0, 0.0f, 15));
        sleepStandingUpBodyPos.Add(new AnimationState(0, 0.0f, 10));
        sleepStandingUpBodyPos.Add(new AnimationState(0, 0.0f, 5));
        sleepStandingUpBodyPos.Add(new AnimationState(0, 0.0f, 0));
        bodyInfo.Add(State.SleepingStandingUp, sleepStandingUpBodyPos);

        #endregion

        #region death
        //idle
        List<AnimationState> deathBasePos = new List<AnimationState>();
        deathBasePos.Add(new AnimationState(0f, 0.0f, 0));
        deathBasePos.Add(new AnimationState(0.05f, 0.05f, 0));
        deathBasePos.Add(new AnimationState(0f, 0f, 0));
        deathBasePos.Add(new AnimationState(-0.05f, 0.05f, 0));
        deathBasePos.Add(new AnimationState(0f, 0.0f, 0));
        deathBasePos.Add(new AnimationState(-0.05f, -0.05f, 0));
        deathBasePos.Add(new AnimationState(0f, 0f, 0));
        deathBasePos.Add(new AnimationState(0.05f, -0.05f, 0));
        baseInfo.Add(State.Death, deathBasePos);

        List<AnimationState> deathLeftArmPos = new List<AnimationState>();
        deathLeftArmPos.Add(new AnimationState(0, 0.0f, 0));
        deathLeftArmPos.Add(new AnimationState(0, 0.0f, 0));
        deathLeftArmPos.Add(new AnimationState(0, 0.0f, 0));
        deathLeftArmPos.Add(new AnimationState(0, 0.0f, 0));
        deathLeftArmPos.Add(new AnimationState(0, 0.0f, 0));
        deathLeftArmPos.Add(new AnimationState(0, 0.0f, 0));
        deathLeftArmPos.Add(new AnimationState(0, 0.0f, 0));
        deathLeftArmPos.Add(new AnimationState(0, 0.0f, 0));
        leftArmInfo.Add(State.Death, deathLeftArmPos);

        List<AnimationState> deathRightArmPos = new List<AnimationState>();
        deathRightArmPos.Add(new AnimationState(0, 0.0f, 0));
        deathRightArmPos.Add(new AnimationState(0, 0.0f, 0));
        deathRightArmPos.Add(new AnimationState(0, 0.0f, 0));
        deathRightArmPos.Add(new AnimationState(0, 0.0f, 0));
        deathRightArmPos.Add(new AnimationState(0, 0.0f, 0));
        deathRightArmPos.Add(new AnimationState(0, 0.0f, 0));
        deathRightArmPos.Add(new AnimationState(0, 0.0f, 0));
        deathRightArmPos.Add(new AnimationState(0, 0.0f, 0));
        rightArmInfo.Add(State.Death, deathRightArmPos);

        List<AnimationState> deathLeftLegPos = new List<AnimationState>();
        deathLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
        deathLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
        deathLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
        deathLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
        deathLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
        deathLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
        deathLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
        deathLeftLegPos.Add(new AnimationState(0, 0.0f, 0));
        leftLegInfo.Add(State.Death, deathLeftLegPos);

        List<AnimationState> deathRightLegPos = new List<AnimationState>();
        deathRightLegPos.Add(new AnimationState(0, 0.0f, 0));
        deathRightLegPos.Add(new AnimationState(0, 0.0f, 0));
        deathRightLegPos.Add(new AnimationState(0, 0.0f, 0));
        deathRightLegPos.Add(new AnimationState(0, 0.0f, 0));
        deathRightLegPos.Add(new AnimationState(0, 0.0f, 0));
        deathRightLegPos.Add(new AnimationState(0, 0.0f, 0));
        deathRightLegPos.Add(new AnimationState(0, 0.0f, 0));
        deathRightLegPos.Add(new AnimationState(0, 0.0f, 0));
        rightLegInfo.Add(State.Death, deathRightLegPos);

        List<AnimationState> deathBodyPos = new List<AnimationState>();
        deathBodyPos.Add(new AnimationState(0, 0.0f, 0));
        deathBodyPos.Add(new AnimationState(0, 0.0f, 0));
        deathBodyPos.Add(new AnimationState(0, 0.0f, 0));
        deathBodyPos.Add(new AnimationState(0, 0.0f, 0));
        deathBodyPos.Add(new AnimationState(0, 0.0f, 0));
        deathBodyPos.Add(new AnimationState(0, 0.0f, 0));
        deathBodyPos.Add(new AnimationState(0, 0.0f, 0));
        deathBodyPos.Add(new AnimationState(0, 0.0f, 0));
        bodyInfo.Add(State.Death, deathBodyPos);
        #endregion
    }

	void Update () 
	{
        if (health <= 0 && !doKill && currentState != State.Death)
        {
            SwitchState(State.Death);
			sound.PlayGolemDieSound ();
			RemoveStalactites();
        }
       

		animationTimer += 0.02f;
        if (animationTimer > currentFrameDuration) 
		{
            animationTimer -= currentFrameDuration;
			cycleInt++;
            if (currentState != State.RepositionJump)
            {
                if (cycleInt >= leftLegInfo[currentState].Count)
                {
                    cycleInt = 0;
                }
            }
            else
            {
                cycleInt = leftLegInfo[currentState].Count - 1;
            }
			
		}
		if (!actionToggle && baseInfo[currentState][cycleInt].action)
		{
			throwTime = Time.time;
            startReposJumpTime = Time.time;
            preActionBasePos = basePos;
			preActionLeftArmPos = leftArm.transform.position;
			preActionRightArmPos = rightArm.transform.position;
			preActionBodyPos = body.transform.position;
			preActionLeftLegPos = leftLeg.transform.position;
			preActionRightLegPos = rightArm.transform.position;
			actionToggle = true;
			doAction = true;
			targetPos = player.transform.position;
		}

        switch (currentState)
        {
            case State.Idle:
                LookAtPlayer();
                idleTimer += Time.deltaTime;
                if (idleTimer > idleTimeDuration)
                {
                    idleTimer = 0;
                    if (nextState != State.Idle)
                    {
                        SwitchState(nextState);
                    }
                    else
                    {
                        DecideState();
                    }
                }
                break;
            case State.Rock:
                break;
            case State.Throw:
                LookAtPlayer();
                break;
            case State.StartClimb:
                break;
            case State.MidClimb:
                basePos.y += 0.09f;
                if (basePos.y > -12)
                {
                    doAction = true;
                }
                break;
            case State.StartClimbJump:
                basePos.x -= 0.03f;
                basePos.y -= 0.2f;
                break;
            case State.MidClimbJump:
                basePos.x -= 0.03f;
                basePos.y -= 0.2f;
                if (basePos.y < -19.4f)
                {
                    doAction = true;
                }
                break;
            case State.EndClimbJump:
                if (!actionToggle)
                {
                    basePos.x -= 0.09f;
                }
                break;
            case State.RepositionJump:
                if (currentPlatform == 0)
                {
                    transform.localScale = new Vector3(-1, 1, 1);
                }
                break;
            case State.RepositionWalk:
                //if (currentPoint == 0)
                //{
                    float distance = Mathf.Abs(walkPoints[targetPoint].x - transform.localPosition.x);
                    if (distance > 0.1f)
	                {
		                if (walkPoints[targetPoint].x > transform.localPosition.x)
                        {
                            transform.localScale = new Vector3(-1, 1, 1);
                            basePos.x += walkSpeed * Time.deltaTime;
                        }
                        else 
                        {
                            transform.localScale = new Vector3(1, 1, 1);
                            basePos.x -= walkSpeed * Time.deltaTime;
                        }
	                }
                    else
                    {
                        currentPoint = targetPoint;
                        DecideState();
                    }

                //}
                break;
            case State.Sleeping:
                if (activated)
                {
                    idleTimer += Time.deltaTime;
                    if (idleTimer > idleTimeDuration)
                    {
                        idleTimer = 0;
                        SwitchState(State.SleepingStandingUp);
                        doAction = false;
                        actionToggle = false;

                    }
                }
                break;
            case State.Death:
                deathDustSpawnTimer += Time.deltaTime;
                if (deathDustSpawnTimer > deathDustSpawnInterval)
                {
                    deathDustSpawnTimer = 0;
                    Vector3 randomPos = new Vector3(Random.Range(-0.5f, 0.5f), Random.Range(-1.0f, 1.0f));
                    GameObject newDustParticle = Instantiate(dustCloudPrefab, body.transform.position + randomPos, Quaternion.identity) as GameObject;
                    float randomSize = Random.Range(1f, 2.0f);
                    newDustParticle.transform.localScale = new Vector3(randomSize, randomSize, randomSize);
                    DustParticleController dustController = newDustParticle.GetComponent<DustParticleController>();
                    dustController.angle = Random.Range(0, 360);


                    //rightLegStartPos = MoveAtAngle(rightLegStartPos, -45, 20f);
                }

                if (bodyPartNumber < 5)
                {
                    deathBodyPartExplodeTimer += Time.deltaTime;

                    if (deathBodyPartExplodeTimer > deathBodyPartExplodeInterval)
                    {
                        deathBodyPartExplodeTimer = 0;
                        flyingParts[bodyPartNumber] = true;
                        if (bodyPartNumber < 4)
                        {
                            bodyPartNumber++;
                        }
                    }
                }

                if (flyingParts[0])
                {
                    leftArm.transform.eulerAngles = new Vector3(leftArm.transform.eulerAngles.x, leftArm.transform.eulerAngles.y, 135);
                    leftArm.transform.position = MoveAtAngle(leftArm.transform.position, 135, 20f);
                }
                if (flyingParts[1])
                {
                    rightArm.transform.eulerAngles = new Vector3(rightArm.transform.eulerAngles.x, rightArm.transform.eulerAngles.y, 225);
                    rightArm.transform.position = MoveAtAngle(rightArm.transform.position, 225, 20f);
                }
                if (flyingParts[2])
                {
                    leftLeg.transform.eulerAngles = new Vector3(leftLeg.transform.eulerAngles.x, leftLeg.transform.eulerAngles.y, 45);
                    leftLeg.transform.position = MoveAtAngle(leftLeg.transform.position, 45, 20f);
                }
                if (flyingParts[3])
                {
                    rightLeg.transform.eulerAngles = new Vector3(rightLeg.transform.eulerAngles.x, rightLeg.transform.eulerAngles.y, 315);
                    rightLeg.transform.position = MoveAtAngle(rightLeg.transform.position, 315, 20f);
                    doSpawnDispenser = true;
                }
                if (flyingParts[4])
                {
                    body.transform.eulerAngles = new Vector3(body.transform.eulerAngles.x, body.transform.eulerAngles.y, 0);
                    body.transform.position = MoveAtAngle(body.transform.position, 90, 20f);
                    if (body.transform.position.y > -1)
                    {
                        doKill = true;
                    }
                }
                break;
            default:
                break;
        }

		if (doAction) 
		{
			switch (currentState)
			{
				case State.Idle:
					break;
				case State.Rock:
                    if (currentBounceTimes < bounceTimes)
                    {
                        ShakeDown(1);
                        currentBounceTimes++;
                        
                    }
                    else
                    {
                        DecideState();
                    }
                    doAction = false;
                    actionToggle = false;
					break;
				case State.Throw:
                    if (currentThrowTimes < throwTimes)
                    {
                        float time = (Time.time - throwTime) / throwDuration;
                        if (segmentInState == 0)
                        {
                            leftArm.transform.position = Vector3.Lerp(preActionLeftArmPos, targetPos, time);
                            leftArm.transform.localEulerAngles = new Vector3(leftArm.transform.localEulerAngles.x, leftArm.transform.localEulerAngles.y, leftArm.transform.localEulerAngles.z + throwRotationSpeed * Time.deltaTime);
                            rightArm.transform.position = Vector3.Lerp(preActionRightArmPos, targetPos, time);
                            rightArm.transform.localEulerAngles = new Vector3(rightArm.transform.localEulerAngles.x, rightArm.transform.localEulerAngles.y, rightArm.transform.localEulerAngles.z + throwRotationSpeed * Time.deltaTime);
                        }
                        else
                        {
                            leftArm.transform.position = Vector3.Lerp(targetPos, preActionLeftArmPos, time);
                            leftArm.transform.localEulerAngles = new Vector3(leftArm.transform.localEulerAngles.x, leftArm.transform.localEulerAngles.y, leftArm.transform.localEulerAngles.z - throwRotationSpeed * Time.deltaTime);
                            rightArm.transform.position = Vector3.Lerp(targetPos, preActionRightArmPos, time);
                            rightArm.transform.localEulerAngles = new Vector3(rightArm.transform.localEulerAngles.x, rightArm.transform.localEulerAngles.y, rightArm.transform.localEulerAngles.z - throwRotationSpeed * Time.deltaTime);
                        }

                        if (time >= 1f)
                        {
                            throwTime = Time.time;
                            if (segmentInState == 0)
                                segmentInState = 1;
                            else
                            {
                                segmentInState = 0;
                                doAction = false;
                                actionToggle = false;
                                currentThrowTimes++;
                            }
                        }
                    }
                    else
                    {
                        segmentInState = 0;
                        DecideState();
                        doAction = false;
                        actionToggle = false;
                    }
				
					break;
                case State.StartClimb:
                    SwitchState(State.MidClimb);
                    doAction = false;
				    actionToggle = false;
                    cycleInt = 0;
                    break;
                case State.MidClimb:
                    SwitchState(State.StartClimbJump);
                    doAction = false;
				    actionToggle = false;
                    cycleInt = 0;
                    break;
                case State.StartClimbJump:
                    SwitchState(State.MidClimbJump);
                   
                    doAction = false;
				    actionToggle = false;
                    cycleInt = 0;
                    leftArm.transform.localPosition = leftArmStartPos + leftArmInfo [currentState] [cycleInt].pos;
			        rightArm.transform.localPosition = rightArmStartPos + rightArmInfo [currentState] [cycleInt].pos;
                    leftArm.transform.localEulerAngles = new Vector3 (leftArm.transform.localEulerAngles.x, leftArm.transform.localEulerAngles.y, leftArmStartRotation + leftArmInfo [currentState] [cycleInt].rotation);
			        rightArm.transform.localEulerAngles = new Vector3 (rightArm.transform.localEulerAngles.x, rightArm.transform.localEulerAngles.y, rightArmStartRotation + rightArmInfo [currentState] [cycleInt].rotation);
                    break;
                case State.MidClimbJump:
                    SwitchState(State.EndClimbJump);
                    doAction = false;
				    actionToggle = false;
                    cycleInt = 0;
                    break;
                case State.EndClimbJump:
                    if (segmentInState == 0)
                    {
                        actionToggle = false;
                        doAction = false;
                        ShakeDown(2);
                        segmentInState = 1;
                       
                    }
                    else
                    {
                        DecideState();
                        doAction = false;
                        actionToggle = false;
                        cycleInt = 0;
                    }
                    break;
                case State.RepositionJump:
                    float reposJumpTimer = (Time.time - startReposJumpTime) / 0.75f;
                    if (currentPlatform == 0)
                    {
                        if (basePos.y > -19.6f)
                        {
                            Vector2 displacement = (((1 - reposJumpTimer) * (1 - reposJumpTimer)) * new Vector2(0, 0)) + (((1 - reposJumpTimer) * 2.0f) * reposJumpTimer * new Vector2(0.4f, 0.9f)) + ((reposJumpTimer * reposJumpTimer) * new Vector2(0.2f, -1.7f));
                            basePos += displacement;
                            if (basePos.y < -19.6f)
                            {
                                basePos.y = -19.6f;
                                if (currentPlatform == 0)
                                {
                                    currentPlatform = 1;
                                    currentPoint = 4;
                                }
                                else
                                {
                                    currentPlatform = 0;
                                    currentPoint = 0;
                                }
                                actionToggle = false;
                                SwitchState(State.EndClimbJump);
                            }
                        }
                    }
                    else
                    {
                        if (basePos.x > 29.6f)
                        {
                            Vector2 displacement = (((1 - reposJumpTimer) * (1 - reposJumpTimer)) * new Vector2(0, 0)) + (((1 - reposJumpTimer) * 2.0f) * reposJumpTimer * new Vector2(0.4f, 1.1f)) + ((reposJumpTimer * reposJumpTimer) * new Vector2(0.6f, -1.7f));
                            basePos.x -= displacement.x;
                            basePos.y += displacement.y;

                            if (basePos.x < 29.6f)
                            {
                                basePos.x = 29.6f;
                                basePos.y = -15.75f;
                                if (currentPlatform == 0)
                                {
                                    currentPlatform = 1;
                                    currentPoint = 4;
                                }
                                else
                                {
                                    currentPlatform = 0;
                                    currentPoint = 0;
                                }
                                SwitchState(State.EndClimbJump);
                            }
                        }
                    }
                    break;
                case State.Sleeping:
                    
                    break;
                case State.SleepingStandingUp:
                    SwitchState(State.Idle);
                    doAction = false;
                    actionToggle = false;
                    break;
				default:
					break;  
			}
		}
		else
		{
            if (!flyingParts[0])
            {
                leftArm.transform.localPosition = leftArmStartPos + leftArmInfo[currentState][cycleInt].pos;
                leftArm.transform.localEulerAngles = new Vector3(leftArm.transform.localEulerAngles.x, leftArm.transform.localEulerAngles.y, leftArmStartRotation + leftArmInfo[currentState][cycleInt].rotation);
            }
            if (!flyingParts[1])
            {
                rightArm.transform.localPosition = rightArmStartPos + rightArmInfo[currentState][cycleInt].pos;
                rightArm.transform.localEulerAngles = new Vector3(rightArm.transform.localEulerAngles.x, rightArm.transform.localEulerAngles.y, rightArmStartRotation + rightArmInfo[currentState][cycleInt].rotation);
            }
        }
        transform.position = basePos + baseInfo[currentState][cycleInt].pos;
        transform.localEulerAngles = new Vector3(transform.localEulerAngles.x, transform.localEulerAngles.y, baseRotation + baseInfo[currentState][cycleInt].rotation);

        if (!flyingParts[2])
        {
            leftLeg.transform.localPosition = leftLegStartPos + leftLegInfo[currentState][cycleInt].pos;
            leftLeg.transform.localEulerAngles = new Vector3(leftLeg.transform.localEulerAngles.x, leftLeg.transform.localEulerAngles.y, leftLegStartRotation + leftLegInfo[currentState][cycleInt].rotation);

        }
        if (!flyingParts[3])
        {
            rightLeg.transform.localPosition = rightLegStartPos + rightLegInfo[currentState][cycleInt].pos;
            rightLeg.transform.localEulerAngles = new Vector3(rightLeg.transform.localEulerAngles.x, rightLeg.transform.localEulerAngles.y, rightLegStartRotation + rightLegInfo[currentState][cycleInt].rotation);
        }
        if (!flyingParts[4])
        {
            body.transform.localScale = new Vector3(bodyInfo[currentState][cycleInt].flip, body.transform.localScale.y, body.transform.localScale.z);
            body.transform.localPosition = bodyStartPos + bodyInfo[currentState][cycleInt].pos;
            body.transform.localEulerAngles = new Vector3(body.transform.localEulerAngles.x, body.transform.localEulerAngles.y, bodyStartRotation + bodyInfo[currentState][cycleInt].rotation);
        }
        
	}

    void LookAtPlayer()
    {
        if (player.transform.position.x > transform.position.x)
        {
            transform.localScale = new Vector3(-1, 1, 1);

        }
        else
        {
            transform.localScale = new Vector3(1, 1, 1);
        }
    }

    Vector3 MoveAtAngle(Vector3 _pos, float angle, float moveSpeed)
    {
        Vector2 direction = new Vector2((float)Mathf.Cos(angle * Mathf.Deg2Rad), (float)Mathf.Sin(angle * Mathf.Deg2Rad));
        direction.Normalize();
        Vector2 pos = _pos;
        pos += direction * moveSpeed * Time.deltaTime;
        return pos;
    }

    void DecideState()
    {
        if (currentPlatform == 0)
        {
            if (currentPoint == 0)
            {
                int randomInt = Random.Range(0, 3);
                if (randomInt == 0)
                {
                    targetPoint = Random.Range(1, 3);
                    SwitchState(State.RepositionWalk);

                }
                else if (randomInt == 1)
                {
                    SwitchState(State.Idle);
                    nextState = State.Throw;
                }
                else
                {
                    SwitchState(State.Idle);
                    nextState = State.Rock;
                }
            }
            else if (currentPoint == 1)
            {
                int randomInt = Random.Range(0, 4);
                if (randomInt == 0)
                {
                    targetPoint = Random.Range(0, 2);
                    if (targetPoint == 1)
                    {
                        targetPoint = 2;
                    }
                    SwitchState(State.RepositionWalk);
                }
                else if (randomInt == 1)
                {
                    SwitchState(State.Idle);
                    nextState = State.Throw;
                }
                else
                {
                    SwitchState(State.Idle);
                    nextState = State.RepositionJump;
                    currentPoint = 4;
                    //Debug.Log("BEEEP");
                    //currentState = State.RepositionJump;
                }
            }
            else if (currentPoint == 2)
            {
                int randomInt = Random.Range(0, 2);
                if (randomInt == 0)
                {
                    targetPoint = Random.Range(0, 2);
                    SwitchState(State.RepositionWalk);
                }
                else
                {
                    SwitchState(State.Idle);
                    nextState = State.Throw;
                }
            }
        }
        else
        {
            if (currentPoint == 3)
            {
                int randomInt = Random.Range(0, 5);
                if (randomInt == 0)
                {
                    targetPoint = Random.Range(4, 6);
                    SwitchState(State.RepositionWalk);
                }
                else if (randomInt == 1)
                {
                    SwitchState(State.Idle);
                    nextState = State.Throw;
                }
                else
                {
                    currentPoint = 4;
                    SwitchState(State.Idle);
                    nextState = State.StartClimb;
                }
            }
            else if (currentPoint == 4)
            {
                int randomInt = Random.Range(0, 3);
                if (randomInt == 0)
                {
                    targetPoint = Random.Range(3, 5);
                    if (targetPoint == 4)
                    {
                        targetPoint = 5;
                    }
                    SwitchState(State.RepositionWalk);
                }
                else if (randomInt == 1)
                {
                    SwitchState(State.Idle);
                    nextState = State.Throw;
                }
                else
                {
                    SwitchState(State.Idle);
                    nextState = State.Rock;
                }
                
            }
            else if (currentPoint == 5)
            {
                int randomInt = Random.Range(0, 3);
                if (randomInt == 0)
                {
                    targetPoint = Random.Range(3, 5);
                    SwitchState(State.RepositionWalk);
                }
                else if (randomInt == 1)
                {
                    SwitchState(State.Idle);
                    nextState = State.Throw;
                }
                else
                {
                    SwitchState(State.Idle);
                    nextState = State.RepositionJump;
                    currentPoint = 0;
                }
            }
           // SwitchState(State.Idle);
            //nextState = State.RepositionJump;
            //currentPoint = 0;
        }
        //SwitchState(State.RepositionJump);
    }

    void SwitchState(State newState)
    {
		actionToggle = false;
		segmentInState = 0;
        cycleInt = 0;
        float newFrameDuration = 0.1f;
        switch (newState)
        {
            case State.Idle:
                newFrameDuration = idleFrameDuration;
                break;
            case State.Rock:
                currentBounceTimes = 0;
                newFrameDuration = rockFrameDuration;
                break;
			case State.Throw:
                currentThrowTimes = 0;
				newFrameDuration = throwFrameDuration;
				break;
            case State.StartClimb:
                transform.localScale = new Vector3(-1, transform.localScale.y, transform.localScale.z);
                newFrameDuration = climbFrameDuration;
                break;
            case State.MidClimb:
                newFrameDuration = climbFrameDuration;
                break;
            case State.StartClimbJump:
                newFrameDuration = climbFrameDuration;
                break;
            case State.MidClimbJump:
                newFrameDuration = climbFrameDuration;
                break;
            case State.EndClimbJump:
                newFrameDuration = falldownFrameDuration;
                break;
            case State.RepositionJump:
                newFrameDuration = rockFrameDuration;
                break;
            case State.RepositionWalk:
                newFrameDuration = walkFrameDuration;
                break;
            case State.Sleeping:
                newFrameDuration = idleFrameDuration;
                break;
            case State.SleepingStandingUp:
                newFrameDuration = standupFrameDuration;
                break;
            case State.Death:
                newFrameDuration = deathFrameDuration;
                break;
            default:
                break;  
        }
        currentFrameDuration = newFrameDuration;
        currentState = newState;
    }

	void RemoveStalactites()
	{
		List<GameObject> stalactites = GameObject.FindGameObjectsWithTag("Stalactite").ToList();
		for (int i = 0; i < stalactites.Count; i++) 
		{
			GameObject currentStalactite = stalactites [i];
			Destroy(currentStalactite.gameObject);
		}
	}

    void ShakeDown(int power = 1)
    {
        if (power == 1)
            cam.DoShake(0.22f);
        else
            cam.DoShake(0.36f);
        
        List<GameObject> stalactites = GameObject.FindGameObjectsWithTag("Stalactite").ToList();
        for (int i = 0; i < stalactites.Count; i++)
        {
            GameObject currentStalactite = stalactites[i];
            StalactiteController stalagController = currentStalactite.GetComponent<StalactiteController>();
            if (stalagController != null)
            {
                if (stalagController.IsEnabled && boundingBox.bounds.Contains(currentStalactite.transform.position))
                {
                    float fallDownChance = 0.5f;
                    if (power == 1)
                    {
                        fallDownChance = 0.175f;
                    }
                    float chance = Random.Range(0f, 1.0f);
                    if (chance < fallDownChance)
                    {
                        stalagController.GoFallDown();
                    }
                }
            }
            
        }

        for (int i = 0; i < stalactites.Count; i++)
        {
            GameObject currentStalactite = stalactites[i];
            StalactiteController stalagController = currentStalactite.GetComponent<StalactiteController>();
            if (stalagController != null && !stalagController.IsEnabled && boundingBox.bounds.Contains(currentStalactite.transform.position))
            {
                stalagController.GrowCheck();
            }
        }

    }
 

    class AnimationState
    {
        public Vector2 pos;
        public float rotation;
		public bool action;
        public int flip;
        public AnimationState(float x, float y, float _rotation, bool _action = false, int _flip = 1)
        {
            pos = new Vector2(x,y);
            rotation = _rotation;
			action = _action;
            flip = _flip;
        }
    }

}
