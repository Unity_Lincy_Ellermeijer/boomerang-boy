﻿using UnityEngine;
using System.Collections;

//This is an attack state
//It drops icicles from the top of the boss room while he has a shield
public class StateMassiveIceAttack : State<BirdBossController>
{
    float cooldown; //check to control when iciles must be dropped
    float attackTime; //check to control end of attack

    GameObject shield;

    public override void Enter(BirdBossController agent)
    {
        agent.Anim.SetTrigger("Centering");
        agent.Anim.SetBool("MassiveAttack", true);

        agent.InstatiateShield(); //set shield
        attackTime = Time.time + agent.mySettings.stayInMassAttckTimer.GetRandomInRange();

        agent.Invulnerable = true;
    }

    public override void Execute(BirdBossController agent)
    {
        if (attackTime < Time.time) //done with attack
            agent.GetFSM.ChangeState(agent.GetFSM.PossibleStates["Climbing"]);

        if (cooldown < Time.time) //ready for new iciles!
            SpawnIcicles(agent);
    }

    public override void Exit(BirdBossController agent)
    {
        agent.Anim.SetBool("MassiveAttack", false);
        agent.MassAttackTimer = Time.fixedTime + agent.StartMassAttackTimer;
        agent.DestroyShield();
        agent.Invulnerable = false;
    }

    void SpawnIcicles(BirdBossController agent)
    {
        cooldown = Time.time + agent.mySettings.cooldownMassAttack;
        var randomAmount = agent.mySettings.iciclesToSpawn.GetRandomInRange(); //get a random amount of icicles

        for (int i = 0; i < randomAmount; i++) //instantiate all icicles
        {
            Vector2 randomPos = agent.AttackPoints[Random.Range(0, agent.AttackPoints.Count)].position;

            GameObject icicle = new GameObject();
            icicle.transform.position = new Vector2(randomPos.x, agent.GetMaxYPos);
            icicle.transform.rotation = Quaternion.identity;
            var icicleScript = icicle.AddComponent<IcicleScript>();

            icicleScript.Create(agent);
        }
    }   

}
