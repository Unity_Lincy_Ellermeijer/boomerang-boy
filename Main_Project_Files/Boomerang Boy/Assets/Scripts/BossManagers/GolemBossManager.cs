﻿using UnityEngine;
using System.Collections;

public class GolemBossManager : BossRoomManager
{
    public Vector2 playerSetPos;
    GolemController controller;
    public ChangePlayerInRange exitPoint;
    public Vector2 returnPoint;
    bool isDone;
	public GameObject dispenserToRemove;
    public GameObject boomerangDispenser;
    public GameObject boomerangToDispense;

    //all temp shit because no monkey
    public bool noMonkey;
    bool fakeMonkeyToggle;
    float fakeMonkeyTimer;
    float fakeMonkeyDuration = 1;
    //--
    bool spawnedDispenser = false;

    public override void Start()
    {
        base.Start();

        boundary = GetComponent<BoxCollider2D>();
        min = boundary.bounds.min;
        max = boundary.bounds.max;
    }

    public override void Update()
    {
        base.Update();
        if (controller != null)
        {
            if (controller.doSpawnDispenser && !spawnedDispenser)
            {
				Destroy(dispenserToRemove);
                GameObject dispenser = Instantiate(boomerangDispenser, Vector3.zero, Quaternion.identity) as GameObject;
                dispenser.transform.parent = this.transform;
                dispenser.transform.localPosition = new Vector3(0f, 0f, 0);
                dispenser.GetComponent<BoomerangDispenser>().boomerangToDispense = boomerangToDispense;
                dispenser.GetComponent<BoomerangDispenser>().growDuration = 1f;
                spawnedDispenser = true;
            }
            if (controller.doKill)
            {
                Exit();
                isDone = true;
                Destroy(controller.gameObject);

                
            }
            if (noMonkey && !fakeMonkeyToggle)
            {
                fakeMonkeyTimer += Time.deltaTime;
                if (fakeMonkeyTimer > fakeMonkeyDuration)
                {
                    fakeMonkeyTimer = 0;
                    fakeMonkeyToggle = true;
                    WakeBoss();
                }
            }
        }
        

        if (isDone)
        {
            if (exitPoint.playerInZone)
            {
                player.transform.position = returnPoint;
            }
            if (enteredRoom.playerInZone)
            {
                player.transform.position = playerSetPos;
            }
        }
    }

    public override void SetUpBoss()
    {
        base.SetUpBoss();
        player.transform.position = playerSetPos;
        fakeMonkeyToggle = false;
        activatedPlayerMovement = false;
        spawnedBoss.transform.parent = this.transform;
        spawnedBoss.transform.localPosition = Vector2.zero;
        controller = spawnedBoss.GetComponent<GolemController>();
        controller.manager = this;
        controller.boundingBox = GetComponent<BoxCollider2D>();
    }

    public void WakeBoss()
    {
        controller.activated = true;
    }

    public override void ActivatePlayerMovement()
    {
        base.ActivatePlayerMovement();
        SpawnBossHpBar(controller.maxHealth);
    }


    public override void Exit()
    {
        LevelManager.golemBattleFinished = true;
        //FUCK YOU WOUTER D:
        LevelManager.ChangeBoomerangInDispensers("ElementalBoomerang");
        base.Exit();
    }

    public override void Reset()
    {
        LevelManager.inGolemBattle = false;
        base.Reset();
    }
}
