﻿using UnityEngine;
using System.Collections;

//This class must be placed on triggers that automatically talk if the player enters
public class AutomaticTalkScript : MonoBehaviour
{
    //checks
    public bool canTalk;
    public bool doneTalking;
    ChangePlayerInRange range;

    SpriteRenderer sprRender;
    TalkScript talkScr; //ref to talkscript

    void Start()
    {
        range = GetComponentInChildren<ChangePlayerInRange>();
        sprRender = GetComponent<SpriteRenderer>();
        talkScr = GetComponent<TalkScript>();
    }

    void Update()
    {
        if (talkScr.isDoneTalking) //if talk is over, disable possibility to talk again
        {
            doneTalking = true;
            talkScr.canTalkAutomaticly = false;
        }
    }

    void OnTriggerStay2D(Collider2D other)
    {
        if (other.tag == "Player" && !doneTalking)
        {
            canTalk = true;
        }
    }
}
