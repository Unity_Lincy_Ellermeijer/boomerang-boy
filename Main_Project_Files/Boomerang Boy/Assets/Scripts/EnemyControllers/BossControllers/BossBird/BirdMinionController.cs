﻿using UnityEngine;
using System.Collections;

//This class creates a minion bird and makes it fly between points in the boss room 
public class BirdMinionController : MonoBehaviour 
{
    //health variables 
    float startHealth;
    float health;
    float Health
    {
        get { return health; }
        set
        {
            health = value;
            if (health <= 0)
            {
                Die();
            }
        }
    }

    enum Element
    {
        fire,
        ice
    };

    Element element; //element corresponding to egg it hatched from 

    Rigidbody2D rb; 
    Animator anim;
    SpriteRenderer spriteRender;
    CircleCollider2D coll;

    //path variables 
    PathScript path; 
    Transform currentPoint;
    int pointSelection;
    float minDistToPoint = 0.2f;

    float startFlyingTime;
    bool flying = false;
    float moveSpeed;
    bool isFacingRight = true;
    int damage = 1;

    GameObject player;
    PlayerHealth playerHealth;
    Knockback knockback;

    //Call from eggscript to create a minion bird with an element
	public void Create(int tempElement, BirdBossController agent) 
    {
        startFlyingTime = agent.mySettings.startFlyingTime;
        moveSpeed = agent.mySettings.movementSpeedMinion;

        rb = gameObject.AddComponent<Rigidbody2D>();
        rb.gravityScale = 0;
        rb.constraints = RigidbodyConstraints2D.FreezeRotation;
        anim = gameObject.AddComponent<Animator>();
        spriteRender = gameObject.AddComponent<SpriteRenderer>();
        coll = gameObject.AddComponent<CircleCollider2D>();
        coll.radius = 0.5f;
        coll.isTrigger = true;

        gameObject.tag = "MinionBird";
        gameObject.layer = 11;

        Sprite tempSprite = Resources.Load<Sprite>("Art/Sprites/Enemies/Boss/birdIceFromEgg");
        AnimatorOverrideController animAOC = Resources.Load<AnimatorOverrideController>("Art/AnimatorControllers/Enemy/Birdboss/BlueBirdAOC");

        if (tempElement == 0)
        {
            animAOC = Resources.Load<AnimatorOverrideController>("Art/AnimatorControllers/Enemy/Birdboss/BlueBirdAOC");
            element = Element.ice;
            tempSprite = Resources.Load<Sprite>("Art/Sprites/Enemies/Boss/birdIceFromEgg");
        }
        else if (tempElement == 1)
        {
            animAOC = Resources.Load<AnimatorOverrideController>("Art/AnimatorControllers/Enemy/Birdboss/RedBirdAOC");
            element = Element.fire;
            tempSprite = Resources.Load<Sprite>("Art/Sprites/Enemies/Boss/birdRedFromEgg");
        }

        spriteRender.sprite = tempSprite;
        spriteRender.sortingLayerName = "Enemies";
        anim.runtimeAnimatorController = animAOC;

        var tempPath = GameObject.FindGameObjectWithTag("MinionBirdPath");
        path = tempPath.GetComponent<PathScript>();
        pointSelection = Random.Range(1, 2); 
        currentPoint = path.PathPoints[pointSelection];

        player = GameObject.FindGameObjectWithTag("Player");
        playerHealth = player.GetComponent<PlayerHealth>();
        knockback = player.GetComponent<Knockback>();

        StartCoroutine("WaitBeforeFlying");
	}

    //baby birds cannot fly instantly...
    IEnumerator WaitBeforeFlying()
    {
        yield return new WaitForSeconds(startFlyingTime);

        anim.SetBool("Flying", true);
        flying = true;

        StopCoroutine("WaitBeforeFlying");
    }

    void Update()
    {
        if (!flying)
            return;

        //fly between two points and flip if needed
        if ((rb.velocity.x > 0 && !isFacingRight) || (rb.velocity.x < 0 && isFacingRight))
            Flip();

        if ((transform.position - currentPoint.position).magnitude < minDistToPoint)
        {
            DetermineNextPoint();
        }

        rb.velocity = (currentPoint.transform.position - transform.position).normalized * moveSpeed;
    }

    void Flip()
    {
        isFacingRight = !isFacingRight;

        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    public void DetermineNextPoint()
    {
        pointSelection++;

        if (pointSelection >= path.PathPoints.Length)
            pointSelection = 1;

        currentPoint = path.PathPoints[pointSelection];
    }

    public void TakeDamage(float damage, int element)
    {
        if ((int)this.element == element)
            return;

        Health -= damage;
    }

    void Die()
    {
        Destroy(gameObject);
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            if (playerHealth.currentHearts > 0)
            {
                playerHealth.TakeDamage(damage);

                knockback.KnockBack(this.gameObject);

                Flip();
                DetermineNextPoint();

            }
        }
    }
}
