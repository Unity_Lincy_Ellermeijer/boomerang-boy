﻿using UnityEngine;
using System.Collections;

//A stone, thrown in the StateRockAttack
public class StoneScript : AttackObstacleScript<GorillaBossController>
{
    Vector2 target; 
    float lookOffset = 0; //to make stone face target

    //Call this method to instantiate a stone with a sprite and target
    public override void Create(Vector2 tempTarget, Sprite sprite, GorillaBossController agent)
    {
        base.Create(tempTarget, sprite, agent);

        thisSprite = Resources.Load<Sprite>("Art/Sprites/Enemies/Boss/Gorilla/Attacks/Stones/ParticleStone");

        target = tempTarget;

        CircleCollider2D coll = gameObject.AddComponent<CircleCollider2D>();
        coll.radius = 0.64f;
        coll.isTrigger = true;

        MoveSpeed = agent.mySettings.throwSpeed.GetRandomInRange();

        Vector2 distToTarget = new Vector2(target.x, target.y) - new Vector2(transform.position.x, transform.position.y);
        distToTarget.Normalize();

        Rb.velocity = new Vector2(distToTarget.x * MoveSpeed, distToTarget.y * MoveSpeed/2); //calculate velocity based on start and target position
    }

    public override void FixedUpdate()
    {
        FacePoint();
    }

    void FacePoint()
    {
        var lookPos = new Vector2(transform.position.x, transform.position.y) + new Vector2(Rb.velocity.x, Rb.velocity.y - 25);
        var angle = Mathf.Atan2(lookPos.x, lookPos.y) * Mathf.Rad2Deg;
        transform.eulerAngles = new Vector3(0f, 0f, -angle + lookOffset);
    }
}
