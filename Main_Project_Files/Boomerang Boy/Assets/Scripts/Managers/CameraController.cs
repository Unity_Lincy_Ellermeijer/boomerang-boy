﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour 
{

	AudioSource audio;
    GameObject target;
    public bool IsFollowing { get; set; }
    public bool IsResetting { get; set; }
    public float damping;
    Vector2 cursorPos;
    float ViewDistance;

    BoxCollider2D boundary;
    Vector3 _min, _max;

    float cameraHalfWidth;
    float cameraSize;

    public float shake = 0f;
	public float shakeAmount = 0.7f;
	public float decreaseFactor = 1.0f;
	
	Vector3 originalPos;
    bool isShaking;

    public void Start()
    {
        target = GameObject.FindGameObjectWithTag("Player");
        IsFollowing = true;
        IsResetting = false;
        boundary = GameObject.FindGameObjectWithTag("LevelBoundary").GetComponent<BoxCollider2D>();
        _min = boundary.bounds.min;
        _max = boundary.bounds.max;
        cameraHalfWidth = GetComponent<Camera>().orthographicSize * ((float)Screen.width / Screen.height);
        cameraSize = GetComponent<Camera>().orthographicSize;
        originalPos = transform.localPosition;
    }

    void FixedUpdate()
    {
        Vector3 shakeVector = Vector3.zero;
        if (isShaking)
        {
            if (shake > 0)
            {
                shakeVector = Random.insideUnitSphere * shakeAmount;

                shake -= Time.deltaTime * decreaseFactor;
            }
            else
            {
                shake = 0f;
                shakeVector = Vector3.zero;
                if (!IsFollowing)
                {
                    isShaking = false;
                    transform.position = originalPos;
                }
            }
        }

        if (!IsResetting)
        {
            cursorPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);

            var x = transform.position.x;
            var y = transform.position.y;

            if (IsFollowing)
            {
                x = Mathf.Lerp(target.transform.position.x, cursorPos.x, damping * Time.deltaTime);
                y = Mathf.Lerp(target.transform.position.y, cursorPos.y, damping * Time.deltaTime);
            }

            x = Mathf.Clamp(x, _min.x + cameraHalfWidth, _max.x - cameraHalfWidth);
            y = Mathf.Clamp(y, _min.y + cameraSize, _max.y - cameraSize);


            transform.position = new Vector3(x + shakeVector.x, y + shakeVector.y, -10);
        }
        
        
        //1.75f
    }

    public void DoShake(float amount)
    {
        originalPos = transform.localPosition;
        shake = amount;
        isShaking = true;
    }



}
