﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

//This state is an attack state
//It makes the boss throw flames while he has a shield 
public class StateMassiveFireAttack : State<BirdBossController>
{
    float cooldown; //check to control when flames must be thrown
    float attackTime; //check to control end of attack

    GameObject shield;

    public override void Enter(BirdBossController agent)
    {
        agent.Anim.SetTrigger("Centering");
        agent.Anim.SetBool("MassiveAttack", true);

        agent.InstatiateShield(); //set the shield
        attackTime = Time.time + agent.mySettings.stayInMassAttckTimer.GetRandomInRange();
        
        agent.Invulnerable = true; //not able to hit bird in this state
    }

    public override void Execute(BirdBossController agent)
    {
        if(attackTime < Time.time) //done with attack
            agent.GetFSM.ChangeState(agent.GetFSM.PossibleStates["Climbing"]);
       
        if(cooldown < Time.time) //ready for new flames !
            ShootFlame(agent);
    }

    public override void Exit(BirdBossController agent)
    {
        agent.Anim.SetBool("MassiveAttack", false);
        agent.MassAttackTimer = Time.fixedTime + agent.StartMassAttackTimer;
        agent.DestroyShield();
        agent.Invulnerable = false;
    }

    void ShootFlame(BirdBossController agent)
    {
        cooldown = Time.time + agent.mySettings.cooldownMassAttack;
        var randomAmount = agent.mySettings.flamesToSpawn.GetRandomInRange(); //get a random amount of flames

        for (int i = 0; i < randomAmount; i++) //instantiate all flames
        {
            GameObject flame = new GameObject();
            flame.transform.position = agent.transform.position;
            flame.transform.rotation = Quaternion.identity;
            var flameScript = flame.AddComponent<FlameScript>();

            Vector2 randomPos = agent.AttackPoints[Random.Range(0, agent.AttackPoints.Count)].position;

            flameScript.Create(randomPos, agent.MidPointRoom.position, agent);
        }
    }
}
