﻿using UnityEngine;
using System.Collections;

public class WoodenDoor : MonoBehaviour
{
    public int maxHp = 3;
    int currentHp;
    bool healthCanBeReduced;
    Renderer renderer;

    public int CurrentHp
    {
        get { return currentHp; }
        set
        {
            currentHp = value;
            Color matColor = renderer.material.color;
            matColor.a = (float)currentHp / (float)maxHp;
            renderer.material.color = matColor;

            if (currentHp <= 0)
                Destroy(this.gameObject);
        }
    }

    void Start()
    {
        renderer = GetComponent<Renderer>();
        healthCanBeReduced = true;
        currentHp = maxHp;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Boomerang"))
        {
            BoomerangScript boomerang = other.GetComponent<BoomerangScript>();
            if (boomerang.isFired && healthCanBeReduced)
            {
                CurrentHp--;
                healthCanBeReduced = false;
                
            }
        }
    }

    void OnTriggerExit2D(Collider2D other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Boomerang"))
        {
            healthCanBeReduced = true;
        }
    }
}
